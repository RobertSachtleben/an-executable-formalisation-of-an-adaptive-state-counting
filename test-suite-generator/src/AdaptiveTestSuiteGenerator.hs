{-
  This module is based on the code automatically generated from the Isabelle formalisation.
  It is slightly modified for performance:
    1.) The implementation of compare_integer is modified to use the Prelude.compare function for Integer 
-}


{-# LANGUAGE EmptyDataDecls, RankNTypes, ScopedTypeVariables #-}

module
  AdaptiveTestSuiteGenerator(Ccompare, Ceq, Set, Fsm_impl_ext, Fsm, Set_impl,
                              Nat, integer_of_nat, Card_UNIV, Sum(..), size,
                              fsm_from_list, generate_test_suite_naive,
                              generate_test_suite_greedy)
  where {

import Prelude ((==), (/=), (<), (<=), (>=), (>), (+), (-), (*), (/), (**),
  (>>=), (>>), (=<<), (&&), (||), (^), (^^), (.), ($), ($!), (++), (!!), Eq,
  error, id, return, not, fst, snd, map, filter, concat, concatMap, reverse,
  zip, null, takeWhile, dropWhile, all, any, Integer, negate, abs, divMod,
  String, Bool(True, False), Maybe(Nothing, Just));
import qualified Prelude;

data Ordera = Eqa | Lt | Gt;

class Ccompare a where {
  ccompare :: Maybe (a -> a -> Ordera);
};

equal_order :: Ordera -> Ordera -> Bool;
equal_order Lt Gt = False;
equal_order Gt Lt = False;
equal_order Eqa Gt = False;
equal_order Gt Eqa = False;
equal_order Eqa Lt = False;
equal_order Lt Eqa = False;
equal_order Gt Gt = True;
equal_order Lt Lt = True;
equal_order Eqa Eqa = True;

newtype Generator a b = Generator (b -> Bool, b -> (a, b));

generator :: forall a b. Generator a b -> (b -> Bool, b -> (a, b));
generator (Generator x) = x;

has_next :: forall a b. Generator a b -> b -> Bool;
has_next g = fst (generator g);

next :: forall a b. Generator a b -> b -> (a, b);
next g = snd (generator g);

sorted_list_subset_fusion ::
  forall a b c.
    (a -> a -> Bool) ->
      (a -> a -> Bool) -> Generator a b -> Generator a c -> b -> c -> Bool;
sorted_list_subset_fusion less eq g1 g2 s1 s2 =
  (if has_next g1 s1
    then (case next g1 s1 of {
           (x, s1a) ->
             has_next g2 s2 &&
               (case next g2 s2 of {
                 (y, s2a) ->
                   (if eq x y
                     then sorted_list_subset_fusion less eq g1 g2 s1a s2a
                     else less y x &&
                            sorted_list_subset_fusion less eq g1 g2 s1 s2a);
               });
         })
    else True);

list_all_fusion :: forall a b. Generator a b -> (a -> Bool) -> b -> Bool;
list_all_fusion g p s =
  (if has_next g s then (case next g s of {
                          (x, sa) -> p x && list_all_fusion g p sa;
                        })
    else True);

class Ceq a where {
  ceq :: Maybe (a -> a -> Bool);
};

data Color = R | B;

data Rbt a b = Empty | Branch Color (Rbt a b) a b (Rbt a b);

rbt_keys_next ::
  forall a b. ([(a, Rbt a b)], Rbt a b) -> (a, ([(a, Rbt a b)], Rbt a b));
rbt_keys_next ((k, t) : kts, Empty) = (k, (kts, t));
rbt_keys_next (kts, Branch c l k v r) = rbt_keys_next ((k, r) : kts, l);

rbt_has_next :: forall a b c. ([(a, Rbt b c)], Rbt b c) -> Bool;
rbt_has_next ([], Empty) = False;
rbt_has_next (vb : vc, va) = True;
rbt_has_next (v, Branch vb vc vd ve vf) = True;

rbt_keys_generator :: forall a b. Generator a ([(a, Rbt a b)], Rbt a b);
rbt_keys_generator = Generator (rbt_has_next, rbt_keys_next);

lt_of_comp :: forall a. (a -> a -> Ordera) -> a -> a -> Bool;
lt_of_comp acomp x y = (case acomp x y of {
                         Eqa -> False;
                         Lt -> True;
                         Gt -> False;
                       });

newtype Mapping_rbt b a = Mapping_RBTa (Rbt b a);

newtype Set_dlist a = Abs_dlist [a];

data Set a = Collect_set (a -> Bool) | DList_set (Set_dlist a)
  | RBT_set (Mapping_rbt a ()) | Set_Monad [a] | Complement (Set a);

list_of_dlist :: forall a. (Ceq a) => Set_dlist a -> [a];
list_of_dlist (Abs_dlist x) = x;

dlist_all :: forall a. (Ceq a) => (a -> Bool) -> Set_dlist a -> Bool;
dlist_all x xc = all x (list_of_dlist xc);

impl_ofa :: forall b a. (Ccompare b) => Mapping_rbt b a -> Rbt b a;
impl_ofa (Mapping_RBTa x) = x;

rbt_init :: forall a b c. Rbt a b -> ([(c, Rbt a b)], Rbt a b);
rbt_init = (\ a -> ([], a));

init ::
  forall a b c. (Ccompare a) => Mapping_rbt a b -> ([(c, Rbt a b)], Rbt a b);
init xa = rbt_init (impl_ofa xa);

class Cenum a where {
  cEnum :: Maybe ([a], ((a -> Bool) -> Bool, (a -> Bool) -> Bool));
};

collect :: forall a. (Cenum a) => (a -> Bool) -> Set a;
collect p = (case cEnum of {
              Nothing -> Collect_set p;
              Just (enum, _) -> Set_Monad (filter p enum);
            });

list_member :: forall a. (a -> a -> Bool) -> [a] -> a -> Bool;
list_member equal (x : xs) y = equal x y || list_member equal xs y;
list_member equal [] y = False;

the :: forall a. Maybe a -> a;
the (Just x2) = x2;

memberc :: forall a. (Ceq a) => Set_dlist a -> a -> Bool;
memberc xa = list_member (the ceq) (list_of_dlist xa);

rbt_comp_lookup :: forall a b. (a -> a -> Ordera) -> Rbt a b -> a -> Maybe b;
rbt_comp_lookup c Empty k = Nothing;
rbt_comp_lookup c (Branch uu l x y r) k = (case c k x of {
    Eqa -> Just y;
    Lt -> rbt_comp_lookup c l k;
    Gt -> rbt_comp_lookup c r k;
  });

lookupb :: forall a b. (Ccompare a) => Mapping_rbt a b -> a -> Maybe b;
lookupb xa = rbt_comp_lookup (the ccompare) (impl_ofa xa);

memberb :: forall a. (Ccompare a) => Mapping_rbt a () -> a -> Bool;
memberb t x = lookupb t x == Just ();

member :: forall a. (Ceq a, Ccompare a) => a -> Set a -> Bool;
member x (Set_Monad xs) =
  (case ceq of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "member Set_Monad: ceq = None" (\ _ -> member x (Set_Monad xs));
    Just eq -> list_member eq xs x;
  });
member xa (Complement x) = not (member xa x);
member x (RBT_set rbt) = memberb rbt x;
member x (DList_set dxs) = memberc dxs x;
member x (Collect_set a) = a x;

subset_eq :: forall a. (Cenum a, Ceq a, Ccompare a) => Set a -> Set a -> Bool;
subset_eq (RBT_set rbt1) (RBT_set rbt2) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "subset RBT_set RBT_set: ccompare = None"
        (\ _ -> subset_eq (RBT_set rbt1) (RBT_set rbt2));
    Just c ->
      (case ceq of {
        Nothing ->
          sorted_list_subset_fusion (lt_of_comp c)
            (\ x y -> equal_order (c x y) Eqa) rbt_keys_generator
            rbt_keys_generator (init rbt1) (init rbt2);
        Just eq ->
          sorted_list_subset_fusion (lt_of_comp c) eq rbt_keys_generator
            rbt_keys_generator (init rbt1) (init rbt2);
      });
  });
subset_eq (Complement a1) (Complement a2) = subset_eq a2 a1;
subset_eq (Collect_set p) (Complement a) =
  subset_eq a (collect (\ x -> not (p x)));
subset_eq (Set_Monad xs) c = all (\ x -> member x c) xs;
subset_eq (DList_set dxs) c =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "subset DList_set1: ceq = None" (\ _ -> subset_eq (DList_set dxs) c);
    Just _ -> dlist_all (\ x -> member x c) dxs;
  });
subset_eq (RBT_set rbt) b =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "subset RBT_set1: ccompare = None" (\ _ -> subset_eq (RBT_set rbt) b);
    Just _ -> list_all_fusion rbt_keys_generator (\ x -> member x b) (init rbt);
  });

less_eq_set :: forall a. (Cenum a, Ceq a, Ccompare a) => Set a -> Set a -> Bool;
less_eq_set = subset_eq;

list_all2_fusion ::
  forall a b c d.
    (a -> b -> Bool) -> Generator a c -> Generator b d -> c -> d -> Bool;
list_all2_fusion p g1 g2 s1 s2 =
  (if has_next g1 s1
    then has_next g2 s2 &&
           (case next g1 s1 of {
             (x, s1a) ->
               (case next g2 s2 of {
                 (y, s2a) -> p x y && list_all2_fusion p g1 g2 s1a s2a;
               });
           })
    else not (has_next g2 s2));

set_eq :: forall a. (Cenum a, Ceq a, Ccompare a) => Set a -> Set a -> Bool;
set_eq (RBT_set rbt1) (RBT_set rbt2) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "set_eq RBT_set RBT_set: ccompare = None"
        (\ _ -> set_eq (RBT_set rbt1) (RBT_set rbt2));
    Just c ->
      (case ceq of {
        Nothing ->
          list_all2_fusion (\ x y -> equal_order (c x y) Eqa) rbt_keys_generator
            rbt_keys_generator (init rbt1) (init rbt2);
        Just eq ->
          list_all2_fusion eq rbt_keys_generator rbt_keys_generator (init rbt1)
            (init rbt2);
      });
  });
set_eq (Complement a) (Complement b) = set_eq a b;
set_eq a b = less_eq_set a b && less_eq_set b a;

data Fsm_impl_ext a b c d =
  Fsm_impl_ext a (Set a) (Set b) (Set c) (Set (a, (b, (c, a)))) d;

transitionsa :: forall a b c d. Fsm_impl_ext a b c d -> Set (a, (b, (c, a)));
transitionsa (Fsm_impl_ext initial nodes inputs outputs transitions more) =
  transitions;

newtype Fsm c b a = Fsm (Fsm_impl_ext c b a ());

fsm_impl :: forall c b a. Fsm c b a -> Fsm_impl_ext c b a ();
fsm_impl (Fsm x) = x;

transitions :: forall a b c. Fsm a b c -> Set (a, (b, (c, a)));
transitions x = transitionsa (fsm_impl x);

outputsa :: forall a b c d. Fsm_impl_ext a b c d -> Set c;
outputsa (Fsm_impl_ext initial nodes inputs outputs transitions more) = outputs;

outputs :: forall a b c. Fsm a b c -> Set c;
outputs x = outputsa (fsm_impl x);

initiala :: forall a b c d. Fsm_impl_ext a b c d -> a;
initiala (Fsm_impl_ext initial nodes inputs outputs transitions more) = initial;

initial :: forall a b c. Fsm a b c -> a;
initial x = initiala (fsm_impl x);

inputsa :: forall a b c d. Fsm_impl_ext a b c d -> Set b;
inputsa (Fsm_impl_ext initial nodes inputs outputs transitions more) = inputs;

inputs :: forall a b c. Fsm a b c -> Set b;
inputs x = inputsa (fsm_impl x);

nodesa :: forall a b c d. Fsm_impl_ext a b c d -> Set a;
nodesa (Fsm_impl_ext initial nodes inputs outputs transitions more) = nodes;

nodes :: forall a b c. Fsm a b c -> Set a;
nodes x = nodesa (fsm_impl x);

comparator_prod ::
  forall a b.
    (a -> a -> Ordera) -> (b -> b -> Ordera) -> (a, b) -> (a, b) -> Ordera;
comparator_prod comp_a comp_b (x, xa) (y, ya) = (case comp_a x y of {
          Eqa -> comp_b xa ya;
          Lt -> Lt;
          Gt -> Gt;
        });

ccompare_prod ::
  forall a b. (Ccompare a, Ccompare b) => Maybe ((a, b) -> (a, b) -> Ordera);
ccompare_prod =
  (case ccompare of {
    Nothing -> Nothing;
    Just comp_a -> (case ccompare of {
                     Nothing -> Nothing;
                     Just comp_b -> Just (comparator_prod comp_a comp_b);
                   });
  });

instance (Ccompare a, Ccompare b) => Ccompare (a, b) where {
  ccompare = ccompare_prod;
};

product :: forall a b. [a] -> [b] -> [(a, b)];
product [] uu = [];
product (x : xs) ys = map (\ a -> (x, a)) ys ++ product xs ys;

cEnum_prod ::
  forall a b.
    (Cenum a,
      Cenum b) => Maybe ([(a, b)],
                          (((a, b) -> Bool) -> Bool, ((a, b) -> Bool) -> Bool));
cEnum_prod =
  (case cEnum of {
    Nothing -> Nothing;
    Just (enum_a, (enum_all_a, enum_ex_a)) ->
      (case cEnum of {
        Nothing -> Nothing;
        Just (enum_b, (enum_all_b, enum_ex_b)) ->
          Just (product enum_a enum_b,
                 ((\ p -> enum_all_a (\ x -> enum_all_b (\ y -> p (x, y)))),
                   (\ p -> enum_ex_a (\ x -> enum_ex_b (\ y -> p (x, y))))));
      });
  });

instance (Cenum a, Cenum b) => Cenum (a, b) where {
  cEnum = cEnum_prod;
};

equality_prod ::
  forall a b. (a -> a -> Bool) -> (b -> b -> Bool) -> (a, b) -> (a, b) -> Bool;
equality_prod eq_a eq_b (x, xa) (y, ya) = eq_a x y && eq_b xa ya;

ceq_prod :: forall a b. (Ceq a, Ceq b) => Maybe ((a, b) -> (a, b) -> Bool);
ceq_prod = (case ceq of {
             Nothing -> Nothing;
             Just eq_a -> (case ceq of {
                            Nothing -> Nothing;
                            Just eq_b -> Just (equality_prod eq_a eq_b);
                          });
           });

instance (Ceq a, Ceq b) => Ceq (a, b) where {
  ceq = ceq_prod;
};

equal_fsm ::
  forall a b c.
    (Cenum a, Ceq a, Ccompare a, Eq a, Cenum b, Ceq b, Ccompare b, Cenum c,
      Ceq c, Ccompare c) => Fsm a b c -> Fsm a b c -> Bool;
equal_fsm x y =
  initial x == initial y &&
    set_eq (nodes x) (nodes y) &&
      set_eq (inputs x) (inputs y) &&
        set_eq (outputs x) (outputs y) &&
          set_eq (transitions x) (transitions y);

ceq_fsm ::
  forall a b c.
    (Cenum a, Ceq a, Ccompare a, Eq a, Cenum b, Ceq b, Ccompare b, Cenum c,
      Ceq c, Ccompare c) => Maybe (Fsm a b c -> Fsm a b c -> Bool);
ceq_fsm = Just equal_fsm;

instance (Cenum a, Ceq a, Ccompare a, Eq a, Cenum b, Ceq b, Ccompare b, Cenum c,
           Ceq c, Ccompare c) => Ceq (Fsm a b c) where {
  ceq = ceq_fsm;
};

newtype Phantom a b = Phantom b;

data Set_impla = Set_Choose | Set_Collect | Set_DList | Set_RBT | Set_Monada;

set_impl_fsm :: forall a b c. Phantom (Fsm a b c) Set_impla;
set_impl_fsm = Phantom Set_DList;

class Set_impl a where {
  set_impl :: Phantom a Set_impla;
};

instance Set_impl (Fsm a b c) where {
  set_impl = set_impl_fsm;
};

ccompare_fsm :: forall a b c. Maybe (Fsm a b c -> Fsm a b c -> Ordera);
ccompare_fsm = Nothing;

instance Ccompare (Fsm a b c) where {
  ccompare = ccompare_fsm;
};

equal_set ::
  forall a. (Cenum a, Ceq a, Ccompare a, Eq a) => Set a -> Set a -> Bool;
equal_set a b = less_eq_set a b && less_eq_set b a;

instance (Cenum a, Ceq a, Ccompare a, Eq a) => Eq (Set a) where {
  a == b = equal_set a b;
};

uminus_set :: forall a. Set a -> Set a;
uminus_set (Complement b) = b;
uminus_set (Collect_set p) = Collect_set (\ x -> not (p x));
uminus_set a = Complement a;

balance :: forall a b. Rbt a b -> a -> b -> Rbt a b -> Rbt a b;
balance (Branch R a w x b) s t (Branch R c y z d) =
  Branch R (Branch B a w x b) s t (Branch B c y z d);
balance (Branch R (Branch R a w x b) s t c) y z Empty =
  Branch R (Branch B a w x b) s t (Branch B c y z Empty);
balance (Branch R (Branch R a w x b) s t c) y z (Branch B va vb vc vd) =
  Branch R (Branch B a w x b) s t (Branch B c y z (Branch B va vb vc vd));
balance (Branch R Empty w x (Branch R b s t c)) y z Empty =
  Branch R (Branch B Empty w x b) s t (Branch B c y z Empty);
balance (Branch R (Branch B va vb vc vd) w x (Branch R b s t c)) y z Empty =
  Branch R (Branch B (Branch B va vb vc vd) w x b) s t (Branch B c y z Empty);
balance (Branch R Empty w x (Branch R b s t c)) y z (Branch B va vb vc vd) =
  Branch R (Branch B Empty w x b) s t (Branch B c y z (Branch B va vb vc vd));
balance (Branch R (Branch B ve vf vg vh) w x (Branch R b s t c)) y z
  (Branch B va vb vc vd) =
  Branch R (Branch B (Branch B ve vf vg vh) w x b) s t
    (Branch B c y z (Branch B va vb vc vd));
balance Empty w x (Branch R b s t (Branch R c y z d)) =
  Branch R (Branch B Empty w x b) s t (Branch B c y z d);
balance (Branch B va vb vc vd) w x (Branch R b s t (Branch R c y z d)) =
  Branch R (Branch B (Branch B va vb vc vd) w x b) s t (Branch B c y z d);
balance Empty w x (Branch R (Branch R b s t c) y z Empty) =
  Branch R (Branch B Empty w x b) s t (Branch B c y z Empty);
balance Empty w x (Branch R (Branch R b s t c) y z (Branch B va vb vc vd)) =
  Branch R (Branch B Empty w x b) s t (Branch B c y z (Branch B va vb vc vd));
balance (Branch B va vb vc vd) w x (Branch R (Branch R b s t c) y z Empty) =
  Branch R (Branch B (Branch B va vb vc vd) w x b) s t (Branch B c y z Empty);
balance (Branch B va vb vc vd) w x
  (Branch R (Branch R b s t c) y z (Branch B ve vf vg vh)) =
  Branch R (Branch B (Branch B va vb vc vd) w x b) s t
    (Branch B c y z (Branch B ve vf vg vh));
balance Empty s t Empty = Branch B Empty s t Empty;
balance Empty s t (Branch B va vb vc vd) =
  Branch B Empty s t (Branch B va vb vc vd);
balance Empty s t (Branch v Empty vb vc Empty) =
  Branch B Empty s t (Branch v Empty vb vc Empty);
balance Empty s t (Branch v (Branch B ve vf vg vh) vb vc Empty) =
  Branch B Empty s t (Branch v (Branch B ve vf vg vh) vb vc Empty);
balance Empty s t (Branch v Empty vb vc (Branch B vf vg vh vi)) =
  Branch B Empty s t (Branch v Empty vb vc (Branch B vf vg vh vi));
balance Empty s t (Branch v (Branch B ve vj vk vl) vb vc (Branch B vf vg vh vi))
  = Branch B Empty s t
      (Branch v (Branch B ve vj vk vl) vb vc (Branch B vf vg vh vi));
balance (Branch B va vb vc vd) s t Empty =
  Branch B (Branch B va vb vc vd) s t Empty;
balance (Branch B va vb vc vd) s t (Branch B ve vf vg vh) =
  Branch B (Branch B va vb vc vd) s t (Branch B ve vf vg vh);
balance (Branch B va vb vc vd) s t (Branch v Empty vf vg Empty) =
  Branch B (Branch B va vb vc vd) s t (Branch v Empty vf vg Empty);
balance (Branch B va vb vc vd) s t (Branch v (Branch B vi vj vk vl) vf vg Empty)
  = Branch B (Branch B va vb vc vd) s t
      (Branch v (Branch B vi vj vk vl) vf vg Empty);
balance (Branch B va vb vc vd) s t (Branch v Empty vf vg (Branch B vj vk vl vm))
  = Branch B (Branch B va vb vc vd) s t
      (Branch v Empty vf vg (Branch B vj vk vl vm));
balance (Branch B va vb vc vd) s t
  (Branch v (Branch B vi vn vo vp) vf vg (Branch B vj vk vl vm)) =
  Branch B (Branch B va vb vc vd) s t
    (Branch v (Branch B vi vn vo vp) vf vg (Branch B vj vk vl vm));
balance (Branch v Empty vb vc Empty) s t Empty =
  Branch B (Branch v Empty vb vc Empty) s t Empty;
balance (Branch v Empty vb vc (Branch B ve vf vg vh)) s t Empty =
  Branch B (Branch v Empty vb vc (Branch B ve vf vg vh)) s t Empty;
balance (Branch v (Branch B vf vg vh vi) vb vc Empty) s t Empty =
  Branch B (Branch v (Branch B vf vg vh vi) vb vc Empty) s t Empty;
balance (Branch v (Branch B vf vg vh vi) vb vc (Branch B ve vj vk vl)) s t Empty
  = Branch B (Branch v (Branch B vf vg vh vi) vb vc (Branch B ve vj vk vl)) s t
      Empty;
balance (Branch v Empty vf vg Empty) s t (Branch B va vb vc vd) =
  Branch B (Branch v Empty vf vg Empty) s t (Branch B va vb vc vd);
balance (Branch v Empty vf vg (Branch B vi vj vk vl)) s t (Branch B va vb vc vd)
  = Branch B (Branch v Empty vf vg (Branch B vi vj vk vl)) s t
      (Branch B va vb vc vd);
balance (Branch v (Branch B vj vk vl vm) vf vg Empty) s t (Branch B va vb vc vd)
  = Branch B (Branch v (Branch B vj vk vl vm) vf vg Empty) s t
      (Branch B va vb vc vd);
balance (Branch v (Branch B vj vk vl vm) vf vg (Branch B vi vn vo vp)) s t
  (Branch B va vb vc vd) =
  Branch B (Branch v (Branch B vj vk vl vm) vf vg (Branch B vi vn vo vp)) s t
    (Branch B va vb vc vd);

rbt_comp_ins ::
  forall a b.
    (a -> a -> Ordera) -> (a -> b -> b -> b) -> a -> b -> Rbt a b -> Rbt a b;
rbt_comp_ins c f k v Empty = Branch R Empty k v Empty;
rbt_comp_ins c f k v (Branch B l x y r) =
  (case c k x of {
    Eqa -> Branch B l x (f k y v) r;
    Lt -> balance (rbt_comp_ins c f k v l) x y r;
    Gt -> balance l x y (rbt_comp_ins c f k v r);
  });
rbt_comp_ins c f k v (Branch R l x y r) =
  (case c k x of {
    Eqa -> Branch R l x (f k y v) r;
    Lt -> Branch R (rbt_comp_ins c f k v l) x y r;
    Gt -> Branch R l x y (rbt_comp_ins c f k v r);
  });

paint :: forall a b. Color -> Rbt a b -> Rbt a b;
paint c Empty = Empty;
paint c (Branch uu l k v r) = Branch c l k v r;

rbt_comp_insert_with_key ::
  forall a b.
    (a -> a -> Ordera) -> (a -> b -> b -> b) -> a -> b -> Rbt a b -> Rbt a b;
rbt_comp_insert_with_key c f k v t = paint B (rbt_comp_ins c f k v t);

rbt_comp_insert ::
  forall a b. (a -> a -> Ordera) -> a -> b -> Rbt a b -> Rbt a b;
rbt_comp_insert c = rbt_comp_insert_with_key c (\ _ _ nv -> nv);

insertb ::
  forall a b. (Ccompare a) => a -> b -> Mapping_rbt a b -> Mapping_rbt a b;
insertb xc xd xe =
  Mapping_RBTa (rbt_comp_insert (the ccompare) xc xd (impl_ofa xe));

comp_sunion_with ::
  forall a b.
    (a -> a -> Ordera) ->
      (a -> b -> b -> b) -> [(a, b)] -> [(a, b)] -> [(a, b)];
comp_sunion_with c f ((ka, va) : asa) ((k, v) : bs) =
  (case c k ka of {
    Eqa -> (ka, f ka va v) : comp_sunion_with c f asa bs;
    Lt -> (k, v) : comp_sunion_with c f ((ka, va) : asa) bs;
    Gt -> (ka, va) : comp_sunion_with c f asa ((k, v) : bs);
  });
comp_sunion_with c f [] bs = bs;
comp_sunion_with c f asa [] = asa;

data Compare = LT | GT | EQ;

skip_red :: forall a b. Rbt a b -> Rbt a b;
skip_red (Branch R l k v r) = l;
skip_red Empty = Empty;
skip_red (Branch B va vb vc vd) = Branch B va vb vc vd;

skip_black :: forall a b. Rbt a b -> Rbt a b;
skip_black t = let {
                 ta = skip_red t;
               } in (case ta of {
                      Empty -> ta;
                      Branch R _ _ _ _ -> ta;
                      Branch B l _ _ _ -> l;
                    });

compare_height ::
  forall a b. Rbt a b -> Rbt a b -> Rbt a b -> Rbt a b -> Compare;
compare_height sx s t tx =
  (case (skip_red sx, (skip_red s, (skip_red t, skip_red tx))) of {
    (Empty, (Empty, (_, Empty))) -> EQ;
    (Empty, (Empty, (_, Branch _ _ _ _ _))) -> LT;
    (Empty, (Branch _ _ _ _ _, (Empty, _))) -> EQ;
    (Empty, (Branch _ _ _ _ _, (Branch _ _ _ _ _, Empty))) -> EQ;
    (Empty, (Branch _ sa _ _ _, (Branch _ ta _ _ _, Branch _ txa _ _ _))) ->
      compare_height Empty sa ta (skip_black txa);
    (Branch _ _ _ _ _, (Empty, (Empty, Empty))) -> GT;
    (Branch _ _ _ _ _, (Empty, (Empty, Branch _ _ _ _ _))) -> LT;
    (Branch _ _ _ _ _, (Empty, (Branch _ _ _ _ _, Empty))) -> EQ;
    (Branch _ _ _ _ _, (Empty, (Branch _ _ _ _ _, Branch _ _ _ _ _))) -> LT;
    (Branch _ _ _ _ _, (Branch _ _ _ _ _, (Empty, _))) -> GT;
    (Branch _ sxa _ _ _, (Branch _ sa _ _ _, (Branch _ ta _ _ _, Empty))) ->
      compare_height (skip_black sxa) sa ta Empty;
    (Branch _ sxa _ _ _,
      (Branch _ sa _ _ _, (Branch _ ta _ _ _, Branch _ txa _ _ _)))
      -> compare_height (skip_black sxa) sa ta (skip_black txa);
  });

newtype Nat = Nat Integer;

zero_nat :: Nat;
zero_nat = Nat (0 :: Integer);

integer_of_nat :: Nat -> Integer;
integer_of_nat (Nat x) = x;

plus_nat :: Nat -> Nat -> Nat;
plus_nat m n = Nat (integer_of_nat m + integer_of_nat n);

data Num = One | Bit0 Num | Bit1 Num;

one_nat :: Nat;
one_nat = Nat (1 :: Integer);

suc :: Nat -> Nat;
suc n = plus_nat n one_nat;

gen_length :: forall a. Nat -> [a] -> Nat;
gen_length n (x : xs) = gen_length (suc n) xs;
gen_length n [] = n;

size_list :: forall a. [a] -> Nat;
size_list = gen_length zero_nat;

equal_nat :: Nat -> Nat -> Bool;
equal_nat m n = integer_of_nat m == integer_of_nat n;

class Ord a where {
  less_eq :: a -> a -> Bool;
  less :: a -> a -> Bool;
};

max :: forall a. (Ord a) => a -> a -> a;
max a b = (if less_eq a b then b else a);

instance Ord Integer where {
  less_eq = (\ a b -> a <= b);
  less = (\ a b -> a < b);
};

nat_of_integer :: Integer -> Nat;
nat_of_integer k = Nat (max (0 :: Integer) k);

apfst :: forall a b c. (a -> b) -> (a, c) -> (b, c);
apfst f (x, y) = (f x, y);

map_prod :: forall a b c d. (a -> b) -> (c -> d) -> (a, c) -> (b, d);
map_prod f g (a, b) = (f a, g b);

divmod_nat :: Nat -> Nat -> (Nat, Nat);
divmod_nat m n =
  let {
    k = integer_of_nat m;
    l = integer_of_nat n;
  } in map_prod nat_of_integer nat_of_integer
         (if k == (0 :: Integer) then ((0 :: Integer), (0 :: Integer))
           else (if l == (0 :: Integer) then ((0 :: Integer), k)
                  else divMod (abs k) (abs l)));

rbtreeify_g :: forall a b. Nat -> [(a, b)] -> (Rbt a b, [(a, b)]);
rbtreeify_g n kvs =
  (if equal_nat n zero_nat || equal_nat n one_nat then (Empty, kvs)
    else (case divmod_nat n (nat_of_integer (2 :: Integer)) of {
           (na, r) ->
             (if equal_nat r zero_nat
               then (case rbtreeify_g na kvs of {
                      (t1, (k, v) : kvsa) ->
                        apfst (Branch B t1 k v) (rbtreeify_g na kvsa);
                    })
               else (case rbtreeify_f na kvs of {
                      (t1, (k, v) : kvsa) ->
                        apfst (Branch B t1 k v) (rbtreeify_g na kvsa);
                    }));
         }));

rbtreeify_f :: forall a b. Nat -> [(a, b)] -> (Rbt a b, [(a, b)]);
rbtreeify_f n kvs =
  (if equal_nat n zero_nat then (Empty, kvs)
    else (if equal_nat n one_nat
           then (case kvs of {
                  (k, v) : kvsa -> (Branch R Empty k v Empty, kvsa);
                })
           else (case divmod_nat n (nat_of_integer (2 :: Integer)) of {
                  (na, r) ->
                    (if equal_nat r zero_nat
                      then (case rbtreeify_f na kvs of {
                             (t1, (k, v) : kvsa) ->
                               apfst (Branch B t1 k v) (rbtreeify_g na kvsa);
                           })
                      else (case rbtreeify_f na kvs of {
                             (t1, (k, v) : kvsa) ->
                               apfst (Branch B t1 k v) (rbtreeify_f na kvsa);
                           }));
                })));

rbtreeify :: forall a b. [(a, b)] -> Rbt a b;
rbtreeify kvs = fst (rbtreeify_g (suc (size_list kvs)) kvs);

gen_entries :: forall a b. [((a, b), Rbt a b)] -> Rbt a b -> [(a, b)];
gen_entries kvts (Branch c l k v r) = gen_entries (((k, v), r) : kvts) l;
gen_entries ((kv, t) : kvts) Empty = kv : gen_entries kvts t;
gen_entries [] Empty = [];

entries :: forall a b. Rbt a b -> [(a, b)];
entries = gen_entries [];

folda :: forall a b c. (a -> b -> c -> c) -> Rbt a b -> c -> c;
folda f (Branch c lt k v rt) x = folda f rt (f k v (folda f lt x));
folda f Empty x = x;

rbt_comp_union_with_key ::
  forall a b.
    (a -> a -> Ordera) -> (a -> b -> b -> b) -> Rbt a b -> Rbt a b -> Rbt a b;
rbt_comp_union_with_key c f t1 t2 =
  (case compare_height t1 t1 t2 t2 of {
    LT -> folda (rbt_comp_insert_with_key c (\ k v w -> f k w v)) t1 t2;
    GT -> folda (rbt_comp_insert_with_key c f) t2 t1;
    EQ -> rbtreeify (comp_sunion_with c f (entries t1) (entries t2));
  });

join ::
  forall a b.
    (Ccompare a) => (a -> b -> b -> b) ->
                      Mapping_rbt a b -> Mapping_rbt a b -> Mapping_rbt a b;
join xc xd xe =
  Mapping_RBTa
    (rbt_comp_union_with_key (the ccompare) xc (impl_ofa xd) (impl_ofa xe));

list_insert :: forall a. (a -> a -> Bool) -> a -> [a] -> [a];
list_insert equal x xs = (if list_member equal xs x then xs else x : xs);

inserta :: forall a. (Ceq a) => a -> Set_dlist a -> Set_dlist a;
inserta xb xc = Abs_dlist (list_insert (the ceq) xb (list_of_dlist xc));

fold :: forall a b. (a -> b -> b) -> [a] -> b -> b;
fold f (x : xs) s = fold f xs (f x s);
fold f [] s = s;

foldc :: forall a b. (Ceq a) => (a -> b -> b) -> Set_dlist a -> b -> b;
foldc x xc = fold x (list_of_dlist xc);

union :: forall a. (Ceq a) => Set_dlist a -> Set_dlist a -> Set_dlist a;
union = foldc inserta;

is_none :: forall a. Maybe a -> Bool;
is_none (Just x) = False;
is_none Nothing = True;

inter_list ::
  forall a. (Ccompare a) => Mapping_rbt a () -> [a] -> Mapping_rbt a ();
inter_list xb xc =
  Mapping_RBTa
    (fold (\ k -> rbt_comp_insert (the ccompare) k ())
      (filter
        (\ x -> not (is_none (rbt_comp_lookup (the ccompare) (impl_ofa xb) x)))
        xc)
      Empty);

filterc ::
  forall a b.
    (Ccompare a) => ((a, b) -> Bool) -> Mapping_rbt a b -> Mapping_rbt a b;
filterc xb xc = Mapping_RBTa (rbtreeify (filter xb (entries (impl_ofa xc))));

comp_sinter_with ::
  forall a b.
    (a -> a -> Ordera) ->
      (a -> b -> b -> b) -> [(a, b)] -> [(a, b)] -> [(a, b)];
comp_sinter_with c f ((ka, va) : asa) ((k, v) : bs) =
  (case c k ka of {
    Eqa -> (ka, f ka va v) : comp_sinter_with c f asa bs;
    Lt -> comp_sinter_with c f ((ka, va) : asa) bs;
    Gt -> comp_sinter_with c f asa ((k, v) : bs);
  });
comp_sinter_with c f [] uu = [];
comp_sinter_with c f uv [] = [];

map_option :: forall a b. (a -> b) -> Maybe a -> Maybe b;
map_option f Nothing = Nothing;
map_option f (Just x2) = Just (f x2);

map_filter :: forall a b. (a -> Maybe b) -> [a] -> [b];
map_filter f [] = [];
map_filter f (x : xs) = (case f x of {
                          Nothing -> map_filter f xs;
                          Just y -> y : map_filter f xs;
                        });

rbt_comp_inter_with_key ::
  forall a b.
    (a -> a -> Ordera) -> (a -> b -> b -> b) -> Rbt a b -> Rbt a b -> Rbt a b;
rbt_comp_inter_with_key c f t1 t2 =
  (case compare_height t1 t1 t2 t2 of {
    LT -> rbtreeify
            (map_filter
              (\ (k, v) ->
                map_option (\ w -> (k, f k v w)) (rbt_comp_lookup c t2 k))
              (entries t1));
    GT -> rbtreeify
            (map_filter
              (\ (k, v) ->
                map_option (\ w -> (k, f k w v)) (rbt_comp_lookup c t1 k))
              (entries t2));
    EQ -> rbtreeify (comp_sinter_with c f (entries t1) (entries t2));
  });

meet ::
  forall a b.
    (Ccompare a) => (a -> b -> b -> b) ->
                      Mapping_rbt a b -> Mapping_rbt a b -> Mapping_rbt a b;
meet xc xd xe =
  Mapping_RBTa
    (rbt_comp_inter_with_key (the ccompare) xc (impl_ofa xd) (impl_ofa xe));

filterb :: forall a. (Ceq a) => (a -> Bool) -> Set_dlist a -> Set_dlist a;
filterb xb xc = Abs_dlist (filter xb (list_of_dlist xc));

inf_set :: forall a. (Ceq a, Ccompare a) => Set a -> Set a -> Set a;
inf_set (RBT_set rbt1) (Set_Monad xs) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter RBT_set Set_Monad: ccompare = None"
        (\ _ -> inf_set (RBT_set rbt1) (Set_Monad xs));
    Just _ -> RBT_set (inter_list rbt1 xs);
  });
inf_set (RBT_set rbt) (DList_set dxs) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter RBT_set DList_set: ccompare = None"
        (\ _ -> inf_set (RBT_set rbt) (DList_set dxs));
    Just _ ->
      (case (ceq :: Maybe (a -> a -> Bool)) of {
        Nothing ->
          (error :: forall a. String -> (() -> a) -> a)
            "inter RBT_set DList_set: ceq = None"
            (\ _ -> inf_set (RBT_set rbt) (DList_set dxs));
        Just _ -> RBT_set (inter_list rbt (list_of_dlist dxs));
      });
  });
inf_set (RBT_set rbt1) (RBT_set rbt2) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter RBT_set RBT_set: ccompare = None"
        (\ _ -> inf_set (RBT_set rbt1) (RBT_set rbt2));
    Just _ -> RBT_set (meet (\ _ _ -> id) rbt1 rbt2);
  });
inf_set (DList_set dxs1) (Set_Monad xs) =
  (case ceq of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter DList_set Set_Monad: ceq = None"
        (\ _ -> inf_set (DList_set dxs1) (Set_Monad xs));
    Just eq -> DList_set (filterb (list_member eq xs) dxs1);
  });
inf_set (DList_set dxs1) (DList_set dxs2) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter DList_set DList_set: ceq = None"
        (\ _ -> inf_set (DList_set dxs1) (DList_set dxs2));
    Just _ -> DList_set (filterb (memberc dxs2) dxs1);
  });
inf_set (DList_set dxs) (RBT_set rbt) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter DList_set RBT_set: ccompare = None"
        (\ _ -> inf_set (DList_set dxs) (RBT_set rbt));
    Just _ ->
      (case (ceq :: Maybe (a -> a -> Bool)) of {
        Nothing ->
          (error :: forall a. String -> (() -> a) -> a)
            "inter DList_set RBT_set: ceq = None"
            (\ _ -> inf_set (DList_set dxs) (RBT_set rbt));
        Just _ -> RBT_set (inter_list rbt (list_of_dlist dxs));
      });
  });
inf_set (Set_Monad xs1) (Set_Monad xs2) =
  (case ceq of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter Set_Monad Set_Monad: ceq = None"
        (\ _ -> inf_set (Set_Monad xs1) (Set_Monad xs2));
    Just eq -> Set_Monad (filter (list_member eq xs2) xs1);
  });
inf_set (Set_Monad xs) (DList_set dxs2) =
  (case ceq of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter Set_Monad DList_set: ceq = None"
        (\ _ -> inf_set (Set_Monad xs) (DList_set dxs2));
    Just eq -> DList_set (filterb (list_member eq xs) dxs2);
  });
inf_set (Set_Monad xs) (RBT_set rbt1) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter Set_Monad RBT_set: ccompare = None"
        (\ _ -> inf_set (RBT_set rbt1) (Set_Monad xs));
    Just _ -> RBT_set (inter_list rbt1 xs);
  });
inf_set (Complement ba) (Complement b) = Complement (sup_set ba b);
inf_set g (RBT_set rbt2) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter RBT_set2: ccompare = None" (\ _ -> inf_set g (RBT_set rbt2));
    Just _ -> RBT_set (filterc ((\ x -> member x g) . fst) rbt2);
  });
inf_set (RBT_set rbt1) g =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter RBT_set1: ccompare = None" (\ _ -> inf_set (RBT_set rbt1) g);
    Just _ -> RBT_set (filterc ((\ x -> member x g) . fst) rbt1);
  });
inf_set h (DList_set dxs2) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter DList_set2: ceq = None" (\ _ -> inf_set h (DList_set dxs2));
    Just _ -> DList_set (filterb (\ x -> member x h) dxs2);
  });
inf_set (DList_set dxs1) h =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "inter DList_set1: ceq = None" (\ _ -> inf_set (DList_set dxs1) h);
    Just _ -> DList_set (filterb (\ x -> member x h) dxs1);
  });
inf_set i (Set_Monad xs) = Set_Monad (filter (\ x -> member x i) xs);
inf_set (Set_Monad xs) i = Set_Monad (filter (\ x -> member x i) xs);
inf_set j (Collect_set a) = Collect_set (\ x -> a x && member x j);
inf_set (Collect_set a) j = Collect_set (\ x -> a x && member x j);

sup_set :: forall a. (Ceq a, Ccompare a) => Set a -> Set a -> Set a;
sup_set ba (Complement b) = Complement (inf_set (uminus_set ba) b);
sup_set (Complement ba) b = Complement (inf_set ba (uminus_set b));
sup_set b (Collect_set a) = Collect_set (\ x -> a x || member x b);
sup_set (Collect_set a) b = Collect_set (\ x -> a x || member x b);
sup_set (Set_Monad xs) (Set_Monad ys) = Set_Monad (xs ++ ys);
sup_set (DList_set dxs1) (Set_Monad ws) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "union DList_set Set_Monad: ceq = None"
        (\ _ -> sup_set (DList_set dxs1) (Set_Monad ws));
    Just _ -> DList_set (fold inserta ws dxs1);
  });
sup_set (Set_Monad ws) (DList_set dxs2) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "union Set_Monad DList_set: ceq = None"
        (\ _ -> sup_set (Set_Monad ws) (DList_set dxs2));
    Just _ -> DList_set (fold inserta ws dxs2);
  });
sup_set (RBT_set rbt1) (Set_Monad zs) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "union RBT_set Set_Monad: ccompare = None"
        (\ _ -> sup_set (RBT_set rbt1) (Set_Monad zs));
    Just _ -> RBT_set (fold (\ k -> insertb k ()) zs rbt1);
  });
sup_set (Set_Monad zs) (RBT_set rbt2) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "union Set_Monad RBT_set: ccompare = None"
        (\ _ -> sup_set (Set_Monad zs) (RBT_set rbt2));
    Just _ -> RBT_set (fold (\ k -> insertb k ()) zs rbt2);
  });
sup_set (DList_set dxs1) (DList_set dxs2) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "union DList_set DList_set: ceq = None"
        (\ _ -> sup_set (DList_set dxs1) (DList_set dxs2));
    Just _ -> DList_set (union dxs1 dxs2);
  });
sup_set (DList_set dxs) (RBT_set rbt) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "union DList_set RBT_set: ccompare = None"
        (\ _ -> sup_set (RBT_set rbt) (DList_set dxs));
    Just _ ->
      (case (ceq :: Maybe (a -> a -> Bool)) of {
        Nothing ->
          (error :: forall a. String -> (() -> a) -> a)
            "union DList_set RBT_set: ceq = None"
            (\ _ -> sup_set (RBT_set rbt) (DList_set dxs));
        Just _ -> RBT_set (foldc (\ k -> insertb k ()) dxs rbt);
      });
  });
sup_set (RBT_set rbt) (DList_set dxs) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "union RBT_set DList_set: ccompare = None"
        (\ _ -> sup_set (RBT_set rbt) (DList_set dxs));
    Just _ ->
      (case (ceq :: Maybe (a -> a -> Bool)) of {
        Nothing ->
          (error :: forall a. String -> (() -> a) -> a)
            "union RBT_set DList_set: ceq = None"
            (\ _ -> sup_set (RBT_set rbt) (DList_set dxs));
        Just _ -> RBT_set (foldc (\ k -> insertb k ()) dxs rbt);
      });
  });
sup_set (RBT_set rbt1) (RBT_set rbt2) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "union RBT_set RBT_set: ccompare = None"
        (\ _ -> sup_set (RBT_set rbt1) (RBT_set rbt2));
    Just _ -> RBT_set (join (\ _ _ -> id) rbt1 rbt2);
  });

class Inf a where {
  inf :: a -> a -> a;
};

instance (Ceq a, Ccompare a) => Inf (Set a) where {
  inf = inf_set;
};

class Sup a where {
  sup :: a -> a -> a;
};

instance (Ceq a, Ccompare a) => Sup (Set a) where {
  sup = sup_set;
};

less_set :: forall a. (Cenum a, Ceq a, Ccompare a) => Set a -> Set a -> Bool;
less_set a b = less_eq_set a b && not (less_eq_set b a);

instance (Cenum a, Ceq a, Ccompare a) => Ord (Set a) where {
  less_eq = less_eq_set;
  less = less_set;
};

class (Ord a) => Preorder a where {
};

class (Preorder a) => Order a where {
};

instance (Cenum a, Ceq a, Ccompare a) => Preorder (Set a) where {
};

instance (Cenum a, Ceq a, Ccompare a) => Order (Set a) where {
};

class (Sup a, Order a) => Semilattice_sup a where {
};

class (Inf a, Order a) => Semilattice_inf a where {
};

class (Semilattice_inf a, Semilattice_sup a) => Lattice a where {
};

instance (Cenum a, Ceq a, Ccompare a) => Semilattice_sup (Set a) where {
};

instance (Cenum a, Ceq a, Ccompare a) => Semilattice_inf (Set a) where {
};

instance (Cenum a, Ceq a, Ccompare a) => Lattice (Set a) where {
};

ceq_set ::
  forall a. (Cenum a, Ceq a, Ccompare a) => Maybe (Set a -> Set a -> Bool);
ceq_set = (case (ceq :: Maybe (a -> a -> Bool)) of {
            Nothing -> Nothing;
            Just _ -> Just set_eq;
          });

instance (Cenum a, Ceq a, Ccompare a) => Ceq (Set a) where {
  ceq = ceq_set;
};

set_impl_set :: forall a. Phantom (Set a) Set_impla;
set_impl_set = Phantom Set_Choose;

instance Set_impl (Set a) where {
  set_impl = set_impl_set;
};

of_phantom :: forall a b. Phantom a b -> b;
of_phantom (Phantom x) = x;

emptyc :: forall a b. (Ccompare a) => Mapping_rbt a b;
emptyc = Mapping_RBTa Empty;

emptyb :: forall a. (Ceq a) => Set_dlist a;
emptyb = Abs_dlist [];

set_empty_choose :: forall a. (Ceq a, Ccompare a) => Set a;
set_empty_choose = (case (ccompare :: Maybe (a -> a -> Ordera)) of {
                     Nothing -> (case (ceq :: Maybe (a -> a -> Bool)) of {
                                  Nothing -> Set_Monad [];
                                  Just _ -> DList_set emptyb;
                                });
                     Just _ -> RBT_set emptyc;
                   });

set_empty :: forall a. (Ceq a, Ccompare a) => Set_impla -> Set a;
set_empty Set_Choose = set_empty_choose;
set_empty Set_Monada = Set_Monad [];
set_empty Set_RBT = RBT_set emptyc;
set_empty Set_DList = DList_set emptyb;
set_empty Set_Collect = Collect_set (\ _ -> False);

fun_upda :: forall a b. (a -> a -> Bool) -> (a -> b) -> a -> b -> a -> b;
fun_upda equal f aa b a = (if equal aa a then b else f a);

balance_right :: forall a b. Rbt a b -> a -> b -> Rbt a b -> Rbt a b;
balance_right a k x (Branch R b s y c) = Branch R a k x (Branch B b s y c);
balance_right (Branch B a k x b) s y Empty =
  balance (Branch R a k x b) s y Empty;
balance_right (Branch B a k x b) s y (Branch B va vb vc vd) =
  balance (Branch R a k x b) s y (Branch B va vb vc vd);
balance_right (Branch R a k x (Branch B b s y c)) t z Empty =
  Branch R (balance (paint R a) k x b) s y (Branch B c t z Empty);
balance_right (Branch R a k x (Branch B b s y c)) t z (Branch B va vb vc vd) =
  Branch R (balance (paint R a) k x b) s y
    (Branch B c t z (Branch B va vb vc vd));
balance_right Empty k x Empty = Empty;
balance_right (Branch R va vb vc Empty) k x Empty = Empty;
balance_right (Branch R va vb vc (Branch R ve vf vg vh)) k x Empty = Empty;
balance_right Empty k x (Branch B va vb vc vd) = Empty;
balance_right (Branch R ve vf vg Empty) k x (Branch B va vb vc vd) = Empty;
balance_right (Branch R ve vf vg (Branch R vi vj vk vl)) k x
  (Branch B va vb vc vd) = Empty;

balance_left :: forall a b. Rbt a b -> a -> b -> Rbt a b -> Rbt a b;
balance_left (Branch R a k x b) s y c = Branch R (Branch B a k x b) s y c;
balance_left Empty k x (Branch B a s y b) =
  balance Empty k x (Branch R a s y b);
balance_left (Branch B va vb vc vd) k x (Branch B a s y b) =
  balance (Branch B va vb vc vd) k x (Branch R a s y b);
balance_left Empty k x (Branch R (Branch B a s y b) t z c) =
  Branch R (Branch B Empty k x a) s y (balance b t z (paint R c));
balance_left (Branch B va vb vc vd) k x (Branch R (Branch B a s y b) t z c) =
  Branch R (Branch B (Branch B va vb vc vd) k x a) s y
    (balance b t z (paint R c));
balance_left Empty k x Empty = Empty;
balance_left Empty k x (Branch R Empty vb vc vd) = Empty;
balance_left Empty k x (Branch R (Branch R ve vf vg vh) vb vc vd) = Empty;
balance_left (Branch B va vb vc vd) k x Empty = Empty;
balance_left (Branch B va vb vc vd) k x (Branch R Empty vf vg vh) = Empty;
balance_left (Branch B va vb vc vd) k x
  (Branch R (Branch R vi vj vk vl) vf vg vh) = Empty;

combine :: forall a b. Rbt a b -> Rbt a b -> Rbt a b;
combine Empty x = x;
combine (Branch v va vb vc vd) Empty = Branch v va vb vc vd;
combine (Branch R a k x b) (Branch R c s y d) =
  (case combine b c of {
    Empty -> Branch R a k x (Branch R Empty s y d);
    Branch R b2 t z c2 -> Branch R (Branch R a k x b2) t z (Branch R c2 s y d);
    Branch B b2 t z c2 -> Branch R a k x (Branch R (Branch B b2 t z c2) s y d);
  });
combine (Branch B a k x b) (Branch B c s y d) =
  (case combine b c of {
    Empty -> balance_left a k x (Branch B Empty s y d);
    Branch R b2 t z c2 -> Branch R (Branch B a k x b2) t z (Branch B c2 s y d);
    Branch B b2 t z c2 ->
      balance_left a k x (Branch B (Branch B b2 t z c2) s y d);
  });
combine (Branch B va vb vc vd) (Branch R b k x c) =
  Branch R (combine (Branch B va vb vc vd) b) k x c;
combine (Branch R a k x b) (Branch B va vb vc vd) =
  Branch R a k x (combine b (Branch B va vb vc vd));

rbt_comp_del :: forall a b. (a -> a -> Ordera) -> a -> Rbt a b -> Rbt a b;
rbt_comp_del c x Empty = Empty;
rbt_comp_del c x (Branch uu a y s b) =
  (case c x y of {
    Eqa -> combine a b;
    Lt -> rbt_comp_del_from_left c x a y s b;
    Gt -> rbt_comp_del_from_right c x a y s b;
  });

rbt_comp_del_from_left ::
  forall a b.
    (a -> a -> Ordera) -> a -> Rbt a b -> a -> b -> Rbt a b -> Rbt a b;
rbt_comp_del_from_left c x (Branch B lt z v rt) y s b =
  balance_left (rbt_comp_del c x (Branch B lt z v rt)) y s b;
rbt_comp_del_from_left c x Empty y s b =
  Branch R (rbt_comp_del c x Empty) y s b;
rbt_comp_del_from_left c x (Branch R va vb vc vd) y s b =
  Branch R (rbt_comp_del c x (Branch R va vb vc vd)) y s b;

rbt_comp_del_from_right ::
  forall a b.
    (a -> a -> Ordera) -> a -> Rbt a b -> a -> b -> Rbt a b -> Rbt a b;
rbt_comp_del_from_right c x a y s (Branch B lt z v rt) =
  balance_right a y s (rbt_comp_del c x (Branch B lt z v rt));
rbt_comp_del_from_right c x a y s Empty =
  Branch R a y s (rbt_comp_del c x Empty);
rbt_comp_del_from_right c x a y s (Branch R va vb vc vd) =
  Branch R a y s (rbt_comp_del c x (Branch R va vb vc vd));

rbt_comp_delete :: forall a b. (a -> a -> Ordera) -> a -> Rbt a b -> Rbt a b;
rbt_comp_delete c k t = paint B (rbt_comp_del c k t);

delete :: forall a b. (Ccompare a) => a -> Mapping_rbt a b -> Mapping_rbt a b;
delete xb xc = Mapping_RBTa (rbt_comp_delete (the ccompare) xb (impl_ofa xc));

list_remove1 :: forall a. (a -> a -> Bool) -> a -> [a] -> [a];
list_remove1 equal x (y : xs) =
  (if equal x y then xs else y : list_remove1 equal x xs);
list_remove1 equal x [] = [];

removea :: forall a. (Ceq a) => a -> Set_dlist a -> Set_dlist a;
removea xb xc = Abs_dlist (list_remove1 (the ceq) xb (list_of_dlist xc));

insert :: forall a. (Ceq a, Ccompare a) => a -> Set a -> Set a;
insert xa (Complement x) = Complement (remove xa x);
insert x (RBT_set rbt) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "insert RBT_set: ccompare = None" (\ _ -> insert x (RBT_set rbt));
    Just _ -> RBT_set (insertb x () rbt);
  });
insert x (DList_set dxs) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "insert DList_set: ceq = None" (\ _ -> insert x (DList_set dxs));
    Just _ -> DList_set (inserta x dxs);
  });
insert x (Set_Monad xs) = Set_Monad (x : xs);
insert x (Collect_set a) =
  (case ceq of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "insert Collect_set: ceq = None" (\ _ -> insert x (Collect_set a));
    Just eq -> Collect_set (fun_upda eq a x True);
  });

remove :: forall a. (Ceq a, Ccompare a) => a -> Set a -> Set a;
remove x (Complement a) = Complement (insert x a);
remove x (RBT_set rbt) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "remove RBT_set: ccompare = None" (\ _ -> remove x (RBT_set rbt));
    Just _ -> RBT_set (delete x rbt);
  });
remove x (DList_set dxs) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "remove DList_set: ceq = None" (\ _ -> remove x (DList_set dxs));
    Just _ -> DList_set (removea x dxs);
  });
remove x (Collect_set a) =
  (case ceq of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a) "remove Collect: ceq = None"
        (\ _ -> remove x (Collect_set a));
    Just eq -> Collect_set (fun_upda eq a x False);
  });

foldl :: forall a b. (a -> b -> a) -> a -> [b] -> a;
foldl f a [] = a;
foldl f a (x : xs) = foldl f (f a x) xs;

set_aux :: forall a. (Ceq a, Ccompare a) => Set_impla -> [a] -> Set a;
set_aux Set_Monada = Set_Monad;
set_aux Set_Choose =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing -> (case (ceq :: Maybe (a -> a -> Bool)) of {
                 Nothing -> Set_Monad;
                 Just _ -> foldl (\ s x -> insert x s) (DList_set emptyb);
               });
    Just _ -> foldl (\ s x -> insert x s) (RBT_set emptyc);
  });
set_aux impl = foldl (\ s x -> insert x s) (set_empty impl);

set :: forall a. (Ceq a, Ccompare a, Set_impl a) => [a] -> Set a;
set xs = set_aux (of_phantom (set_impl :: Phantom a Set_impla)) xs;

subseqs :: forall a. [a] -> [[a]];
subseqs [] = [[]];
subseqs (x : xs) = let {
                     xss = subseqs xs;
                   } in map (\ a -> x : a) xss ++ xss;

cEnum_set ::
  forall a.
    (Cenum a, Ceq a, Ccompare a,
      Set_impl a) => Maybe ([Set a],
                             ((Set a -> Bool) -> Bool,
                               (Set a -> Bool) -> Bool));
cEnum_set =
  (case cEnum of {
    Nothing -> Nothing;
    Just (enum_a, (_, _)) ->
      Just (map set (subseqs enum_a),
             ((\ p -> all p (map set (subseqs enum_a))),
               (\ p -> any p (map set (subseqs enum_a)))));
  });

instance (Cenum a, Ceq a, Ccompare a, Set_impl a) => Cenum (Set a) where {
  cEnum = cEnum_set;
};

class Finite_UNIV a where {
  finite_UNIV :: Phantom a Bool;
};

finite_UNIV_set :: forall a. (Finite_UNIV a) => Phantom (Set a) Bool;
finite_UNIV_set = Phantom (of_phantom (finite_UNIV :: Phantom a Bool));

instance (Finite_UNIV a) => Finite_UNIV (Set a) where {
  finite_UNIV = finite_UNIV_set;
};

class (Ccompare a) => Cproper_interval a where {
  cproper_interval :: Maybe a -> Maybe a -> Bool;
};

set_less_eq_aux_Compl_fusion ::
  forall a b c.
    (a -> a -> Bool) ->
      (Maybe a -> Maybe a -> Bool) ->
        Generator a b -> Generator a c -> Maybe a -> b -> c -> Bool;
set_less_eq_aux_Compl_fusion less proper_interval g1 g2 ao s1 s2 =
  (if has_next g1 s1
    then (if has_next g2 s2
           then (case next g1 s1 of {
                  (x, s1a) ->
                    (case next g2 s2 of {
                      (y, s2a) ->
                        (if less x y
                          then proper_interval ao (Just x) ||
                                 set_less_eq_aux_Compl_fusion less
                                   proper_interval g1 g2 (Just x) s1a s2
                          else (if less y x
                                 then proper_interval ao (Just y) ||
set_less_eq_aux_Compl_fusion less proper_interval g1 g2 (Just y) s1 s2a
                                 else proper_interval ao (Just y)));
                    });
                })
           else True)
    else True);

compl_set_less_eq_aux_fusion ::
  forall a b c.
    (a -> a -> Bool) ->
      (Maybe a -> Maybe a -> Bool) ->
        Generator a b -> Generator a c -> Maybe a -> b -> c -> Bool;
compl_set_less_eq_aux_fusion less proper_interval g1 g2 ao s1 s2 =
  (if has_next g1 s1
    then (case next g1 s1 of {
           (x, s1a) ->
             (if has_next g2 s2
               then (case next g2 s2 of {
                      (y, s2a) ->
                        (if less x y
                          then not (proper_interval ao (Just x)) &&
                                 compl_set_less_eq_aux_fusion less
                                   proper_interval g1 g2 (Just x) s1a s2
                          else (if less y x
                                 then not (proper_interval ao (Just y)) &&
compl_set_less_eq_aux_fusion less proper_interval g1 g2 (Just y) s1 s2a
                                 else not (proper_interval ao (Just y))));
                    })
               else not (proper_interval ao (Just x)) &&
                      compl_set_less_eq_aux_fusion less proper_interval g1 g2
                        (Just x) s1a s2);
         })
    else (if has_next g2 s2
           then (case next g2 s2 of {
                  (y, s2a) ->
                    not (proper_interval ao (Just y)) &&
                      compl_set_less_eq_aux_fusion less proper_interval g1 g2
                        (Just y) s1 s2a;
                })
           else not (proper_interval ao Nothing)));

set_less_eq_aux_Compl ::
  forall a.
    (a -> a -> Bool) ->
      (Maybe a -> Maybe a -> Bool) -> Maybe a -> [a] -> [a] -> Bool;
set_less_eq_aux_Compl less proper_interval ao (x : xs) (y : ys) =
  (if less x y
    then proper_interval ao (Just x) ||
           set_less_eq_aux_Compl less proper_interval (Just x) xs (y : ys)
    else (if less y x
           then proper_interval ao (Just y) ||
                  set_less_eq_aux_Compl less proper_interval (Just y) (x : xs)
                    ys
           else proper_interval ao (Just y)));
set_less_eq_aux_Compl less proper_interval ao xs [] = True;
set_less_eq_aux_Compl less proper_interval ao [] ys = True;

compl_set_less_eq_aux ::
  forall a.
    (a -> a -> Bool) ->
      (Maybe a -> Maybe a -> Bool) -> Maybe a -> [a] -> [a] -> Bool;
compl_set_less_eq_aux less proper_interval ao (x : xs) (y : ys) =
  (if less x y
    then not (proper_interval ao (Just x)) &&
           compl_set_less_eq_aux less proper_interval (Just x) xs (y : ys)
    else (if less y x
           then not (proper_interval ao (Just y)) &&
                  compl_set_less_eq_aux less proper_interval (Just y) (x : xs)
                    ys
           else not (proper_interval ao (Just y))));
compl_set_less_eq_aux less proper_interval ao (x : xs) [] =
  not (proper_interval ao (Just x)) &&
    compl_set_less_eq_aux less proper_interval (Just x) xs [];
compl_set_less_eq_aux less proper_interval ao [] (y : ys) =
  not (proper_interval ao (Just y)) &&
    compl_set_less_eq_aux less proper_interval (Just y) [] ys;
compl_set_less_eq_aux less proper_interval ao [] [] =
  not (proper_interval ao Nothing);

lexord_eq_fusion ::
  forall a b c.
    (a -> a -> Bool) -> Generator a b -> Generator a c -> b -> c -> Bool;
lexord_eq_fusion less g1 g2 s1 s2 =
  (if has_next g1 s1
    then has_next g2 s2 &&
           (case next g1 s1 of {
             (x, s1a) ->
               (case next g2 s2 of {
                 (y, s2a) ->
                   less x y ||
                     not (less y x) && lexord_eq_fusion less g1 g2 s1a s2a;
               });
           })
    else True);

remdups_sorted :: forall a. (a -> a -> Bool) -> [a] -> [a];
remdups_sorted less (x : y : xs) =
  (if less x y then x : remdups_sorted less (y : xs)
    else remdups_sorted less (y : xs));
remdups_sorted less [x] = [x];
remdups_sorted less [] = [];

quicksort_acc :: forall a. (a -> a -> Bool) -> [a] -> [a] -> [a];
quicksort_acc less ac (x : v : va) = quicksort_part less ac x [] [] [] (v : va);
quicksort_acc less ac [x] = x : ac;
quicksort_acc less ac [] = ac;

quicksort_part ::
  forall a. (a -> a -> Bool) -> [a] -> a -> [a] -> [a] -> [a] -> [a] -> [a];
quicksort_part less ac x lts eqs gts (z : zs) =
  (if less x z then quicksort_part less ac x lts eqs (z : gts) zs
    else (if less z x then quicksort_part less ac x (z : lts) eqs gts zs
           else quicksort_part less ac x lts (z : eqs) gts zs));
quicksort_part less ac x lts eqs gts [] =
  quicksort_acc less (eqs ++ x : quicksort_acc less ac gts) lts;

quicksort :: forall a. (a -> a -> Bool) -> [a] -> [a];
quicksort less = quicksort_acc less [];

gen_keys :: forall a b. [(a, Rbt a b)] -> Rbt a b -> [a];
gen_keys kts (Branch c l k v r) = gen_keys ((k, r) : kts) l;
gen_keys ((k, t) : kts) Empty = k : gen_keys kts t;
gen_keys [] Empty = [];

keys :: forall a b. Rbt a b -> [a];
keys = gen_keys [];

keysa :: forall a. (Ccompare a) => Mapping_rbt a () -> [a];
keysa xa = keys (impl_ofa xa);

csorted_list_of_set :: forall a. (Ceq a, Ccompare a) => Set a -> [a];
csorted_list_of_set (Set_Monad xs) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "csorted_list_of_set Set_Monad: ccompare = None"
        (\ _ -> csorted_list_of_set (Set_Monad xs));
    Just c -> remdups_sorted (lt_of_comp c) (quicksort (lt_of_comp c) xs);
  });
csorted_list_of_set (DList_set dxs) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "csorted_list_of_set DList_set: ceq = None"
        (\ _ -> csorted_list_of_set (DList_set dxs));
    Just _ ->
      (case ccompare of {
        Nothing ->
          (error :: forall a. String -> (() -> a) -> a)
            "csorted_list_of_set DList_set: ccompare = None"
            (\ _ -> csorted_list_of_set (DList_set dxs));
        Just c -> quicksort (lt_of_comp c) (list_of_dlist dxs);
      });
  });
csorted_list_of_set (RBT_set rbt) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "csorted_list_of_set RBT_set: ccompare = None"
        (\ _ -> csorted_list_of_set (RBT_set rbt));
    Just _ -> keysa rbt;
  });

bot_set :: forall a. (Ceq a, Ccompare a, Set_impl a) => Set a;
bot_set = set_empty (of_phantom (set_impl :: Phantom a Set_impla));

top_set :: forall a. (Ceq a, Ccompare a, Set_impl a) => Set a;
top_set = uminus_set bot_set;

le_of_comp :: forall a. (a -> a -> Ordera) -> a -> a -> Bool;
le_of_comp acomp x y = (case acomp x y of {
                         Eqa -> True;
                         Lt -> True;
                         Gt -> False;
                       });

lexordp_eq :: forall a. (a -> a -> Bool) -> [a] -> [a] -> Bool;
lexordp_eq less (x : xs) (y : ys) =
  less x y || not (less y x) && lexordp_eq less xs ys;
lexordp_eq less (x : xs) [] = False;
lexordp_eq less xs [] = null xs;
lexordp_eq less [] ys = True;

finite :: forall a. (Finite_UNIV a, Ceq a, Ccompare a) => Set a -> Bool;
finite (Collect_set p) =
  of_phantom (finite_UNIV :: Phantom a Bool) ||
    (error :: forall a. String -> (() -> a) -> a) "finite Collect_set"
      (\ _ -> finite (Collect_set p));
finite (Set_Monad xs) = True;
finite (Complement a) =
  (if of_phantom (finite_UNIV :: Phantom a Bool) then True
    else (if finite a then False
           else (error :: forall a. String -> (() -> a) -> a)
                  "finite Complement: infinite set"
                  (\ _ -> finite (Complement a))));
finite (RBT_set rbt) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "finite RBT_set: ccompare = None" (\ _ -> finite (RBT_set rbt));
    Just _ -> True;
  });
finite (DList_set dxs) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "finite DList_set: ceq = None" (\ _ -> finite (DList_set dxs));
    Just _ -> True;
  });

set_less_aux_Compl_fusion ::
  forall a b c.
    (a -> a -> Bool) ->
      (Maybe a -> Maybe a -> Bool) ->
        Generator a b -> Generator a c -> Maybe a -> b -> c -> Bool;
set_less_aux_Compl_fusion less proper_interval g1 g2 ao s1 s2 =
  (if has_next g1 s1
    then (case next g1 s1 of {
           (x, s1a) ->
             (if has_next g2 s2
               then (case next g2 s2 of {
                      (y, s2a) ->
                        (if less x y
                          then proper_interval ao (Just x) ||
                                 set_less_aux_Compl_fusion less proper_interval
                                   g1 g2 (Just x) s1a s2
                          else (if less y x
                                 then proper_interval ao (Just y) ||
set_less_aux_Compl_fusion less proper_interval g1 g2 (Just y) s1 s2a
                                 else proper_interval ao (Just y)));
                    })
               else proper_interval ao (Just x) ||
                      set_less_aux_Compl_fusion less proper_interval g1 g2
                        (Just x) s1a s2);
         })
    else (if has_next g2 s2
           then (case next g2 s2 of {
                  (y, s2a) ->
                    proper_interval ao (Just y) ||
                      set_less_aux_Compl_fusion less proper_interval g1 g2
                        (Just y) s1 s2a;
                })
           else proper_interval ao Nothing));

compl_set_less_aux_fusion ::
  forall a b c.
    (a -> a -> Bool) ->
      (Maybe a -> Maybe a -> Bool) ->
        Generator a b -> Generator a c -> Maybe a -> b -> c -> Bool;
compl_set_less_aux_fusion less proper_interval g1 g2 ao s1 s2 =
  has_next g1 s1 &&
    has_next g2 s2 &&
      (case next g1 s1 of {
        (x, s1a) ->
          (case next g2 s2 of {
            (y, s2a) ->
              (if less x y
                then not (proper_interval ao (Just x)) &&
                       compl_set_less_aux_fusion less proper_interval g1 g2
                         (Just x) s1a s2
                else (if less y x
                       then not (proper_interval ao (Just y)) &&
                              compl_set_less_aux_fusion less proper_interval g1
                                g2 (Just y) s1 s2a
                       else not (proper_interval ao (Just y))));
          });
      });

set_less_aux_Compl ::
  forall a.
    (a -> a -> Bool) ->
      (Maybe a -> Maybe a -> Bool) -> Maybe a -> [a] -> [a] -> Bool;
set_less_aux_Compl less proper_interval ao (x : xs) (y : ys) =
  (if less x y
    then proper_interval ao (Just x) ||
           set_less_aux_Compl less proper_interval (Just x) xs (y : ys)
    else (if less y x
           then proper_interval ao (Just y) ||
                  set_less_aux_Compl less proper_interval (Just y) (x : xs) ys
           else proper_interval ao (Just y)));
set_less_aux_Compl less proper_interval ao (x : xs) [] =
  proper_interval ao (Just x) ||
    set_less_aux_Compl less proper_interval (Just x) xs [];
set_less_aux_Compl less proper_interval ao [] (y : ys) =
  proper_interval ao (Just y) ||
    set_less_aux_Compl less proper_interval (Just y) [] ys;
set_less_aux_Compl less proper_interval ao [] [] = proper_interval ao Nothing;

compl_set_less_aux ::
  forall a.
    (a -> a -> Bool) ->
      (Maybe a -> Maybe a -> Bool) -> Maybe a -> [a] -> [a] -> Bool;
compl_set_less_aux less proper_interval ao (x : xs) (y : ys) =
  (if less x y
    then not (proper_interval ao (Just x)) &&
           compl_set_less_aux less proper_interval (Just x) xs (y : ys)
    else (if less y x
           then not (proper_interval ao (Just y)) &&
                  compl_set_less_aux less proper_interval (Just y) (x : xs) ys
           else not (proper_interval ao (Just y))));
compl_set_less_aux less proper_interval ao xs [] = False;
compl_set_less_aux less proper_interval ao [] ys = False;

lexord_fusion ::
  forall a b c.
    (a -> a -> Bool) -> Generator a b -> Generator a c -> b -> c -> Bool;
lexord_fusion less g1 g2 s1 s2 =
  (if has_next g1 s1
    then (if has_next g2 s2
           then (case next g1 s1 of {
                  (x, s1a) ->
                    (case next g2 s2 of {
                      (y, s2a) ->
                        less x y ||
                          not (less y x) && lexord_fusion less g1 g2 s1a s2a;
                    });
                })
           else False)
    else has_next g2 s2);

lexordp :: forall a. (a -> a -> Bool) -> [a] -> [a] -> Bool;
lexordp less (x : xs) (y : ys) =
  less x y || not (less y x) && lexordp less xs ys;
lexordp less xs [] = False;
lexordp less [] ys = not (null ys);

comp_of_ords ::
  forall a. (a -> a -> Bool) -> (a -> a -> Bool) -> a -> a -> Ordera;
comp_of_ords le lt x y = (if lt x y then Lt else (if le x y then Eqa else Gt));

ccompare_set ::
  forall a.
    (Finite_UNIV a, Ceq a, Cproper_interval a,
      Set_impl a) => Maybe (Set a -> Set a -> Ordera);
ccompare_set = (case (ccompare :: Maybe (a -> a -> Ordera)) of {
                 Nothing -> Nothing;
                 Just _ -> Just (comp_of_ords cless_eq_set cless_set);
               });

cless_set ::
  forall a.
    (Finite_UNIV a, Ceq a, Cproper_interval a,
      Set_impl a) => Set a -> Set a -> Bool;
cless_set (Complement (RBT_set rbt1)) (RBT_set rbt2) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_set (Complement RBT_set) RBT_set: ccompare = None"
        (\ _ -> cless_set (Complement (RBT_set rbt1)) (RBT_set rbt2));
    Just c ->
      (finite :: Set a -> Bool) (top_set :: Set a) &&
        compl_set_less_aux_fusion (lt_of_comp c) cproper_interval
          rbt_keys_generator rbt_keys_generator Nothing (init rbt1) (init rbt2);
  });
cless_set (RBT_set rbt1) (Complement (RBT_set rbt2)) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_set RBT_set (Complement RBT_set): ccompare = None"
        (\ _ -> cless_set (RBT_set rbt1) (Complement (RBT_set rbt2)));
    Just c ->
      (if (finite :: Set a -> Bool) (top_set :: Set a)
        then set_less_aux_Compl_fusion (lt_of_comp c) cproper_interval
               rbt_keys_generator rbt_keys_generator Nothing (init rbt1)
               (init rbt2)
        else True);
  });
cless_set (RBT_set rbta) (RBT_set rbt) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_set RBT_set RBT_set: ccompare = None"
        (\ _ -> cless_set (RBT_set rbta) (RBT_set rbt));
    Just c ->
      lexord_fusion (\ x y -> lt_of_comp c y x) rbt_keys_generator
        rbt_keys_generator (init rbta) (init rbt);
  });
cless_set (Complement a) (Complement b) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_set Complement Complement: ccompare = None"
        (\ _ -> cless_set (Complement a) (Complement b));
    Just _ -> lt_of_comp (the ccompare_set) b a;
  });
cless_set (Complement a) b =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_set Complement1: ccompare = None"
        (\ _ -> cless_set (Complement a) b);
    Just c ->
      (if finite a && finite b
        then (finite :: Set a -> Bool) (top_set :: Set a) &&
               compl_set_less_aux (lt_of_comp c) cproper_interval Nothing
                 (csorted_list_of_set a) (csorted_list_of_set b)
        else (error :: forall a. String -> (() -> a) -> a)
               "cless_set Complement1: infinite set"
               (\ _ -> cless_set (Complement a) b));
  });
cless_set a (Complement b) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_set Complement2: ccompare = None"
        (\ _ -> cless_set a (Complement b));
    Just c ->
      (if finite a && finite b
        then (if (finite :: Set a -> Bool) (top_set :: Set a)
               then set_less_aux_Compl (lt_of_comp c) cproper_interval Nothing
                      (csorted_list_of_set a) (csorted_list_of_set b)
               else True)
        else (error :: forall a. String -> (() -> a) -> a)
               "cless_set Complement2: infinite set"
               (\ _ -> cless_set a (Complement b)));
  });
cless_set a b =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a) "cless_set: ccompare = None"
        (\ _ -> cless_set a b);
    Just c ->
      (if finite a && finite b
        then lexordp (\ x y -> lt_of_comp c y x) (csorted_list_of_set a)
               (csorted_list_of_set b)
        else (error :: forall a. String -> (() -> a) -> a)
               "cless_set: infinite set" (\ _ -> cless_set a b));
  });

cless_eq_set ::
  forall a.
    (Finite_UNIV a, Ceq a, Cproper_interval a,
      Set_impl a) => Set a -> Set a -> Bool;
cless_eq_set (Complement (RBT_set rbt1)) (RBT_set rbt2) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_eq_set (Complement RBT_set) RBT_set: ccompare = None"
        (\ _ -> cless_eq_set (Complement (RBT_set rbt1)) (RBT_set rbt2));
    Just c ->
      (finite :: Set a -> Bool) (top_set :: Set a) &&
        compl_set_less_eq_aux_fusion (lt_of_comp c) cproper_interval
          rbt_keys_generator rbt_keys_generator Nothing (init rbt1) (init rbt2);
  });
cless_eq_set (RBT_set rbt1) (Complement (RBT_set rbt2)) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_eq_set RBT_set (Complement RBT_set): ccompare = None"
        (\ _ -> cless_eq_set (RBT_set rbt1) (Complement (RBT_set rbt2)));
    Just c ->
      (if (finite :: Set a -> Bool) (top_set :: Set a)
        then set_less_eq_aux_Compl_fusion (lt_of_comp c) cproper_interval
               rbt_keys_generator rbt_keys_generator Nothing (init rbt1)
               (init rbt2)
        else True);
  });
cless_eq_set (RBT_set rbta) (RBT_set rbt) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_eq_set RBT_set RBT_set: ccompare = None"
        (\ _ -> cless_eq_set (RBT_set rbta) (RBT_set rbt));
    Just c ->
      lexord_eq_fusion (\ x y -> lt_of_comp c y x) rbt_keys_generator
        rbt_keys_generator (init rbta) (init rbt);
  });
cless_eq_set (Complement a) (Complement b) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_eq_set Complement Complement: ccompare = None"
        (\ _ -> le_of_comp (the ccompare_set) (Complement a) (Complement b));
    Just _ -> cless_eq_set b a;
  });
cless_eq_set (Complement a) b =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_eq_set Complement1: ccompare = None"
        (\ _ -> cless_eq_set (Complement a) b);
    Just c ->
      (if finite a && finite b
        then (finite :: Set a -> Bool) (top_set :: Set a) &&
               compl_set_less_eq_aux (lt_of_comp c) cproper_interval Nothing
                 (csorted_list_of_set a) (csorted_list_of_set b)
        else (error :: forall a. String -> (() -> a) -> a)
               "cless_eq_set Complement1: infinite set"
               (\ _ -> cless_eq_set (Complement a) b));
  });
cless_eq_set a (Complement b) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_eq_set Complement2: ccompare = None"
        (\ _ -> cless_eq_set a (Complement b));
    Just c ->
      (if finite a && finite b
        then (if (finite :: Set a -> Bool) (top_set :: Set a)
               then set_less_eq_aux_Compl (lt_of_comp c) cproper_interval
                      Nothing (csorted_list_of_set a) (csorted_list_of_set b)
               else True)
        else (error :: forall a. String -> (() -> a) -> a)
               "cless_eq_set Complement2: infinite set"
               (\ _ -> cless_eq_set a (Complement b)));
  });
cless_eq_set a b =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cless_eq_set: ccompare = None" (\ _ -> cless_eq_set a b);
    Just c ->
      (if finite a && finite b
        then lexordp_eq (\ x y -> lt_of_comp c y x) (csorted_list_of_set a)
               (csorted_list_of_set b)
        else (error :: forall a. String -> (() -> a) -> a)
               "cless_eq_set: infinite set" (\ _ -> cless_eq_set a b));
  });

instance (Finite_UNIV a, Ceq a, Cproper_interval a,
           Set_impl a) => Ccompare (Set a) where {
  ccompare = ccompare_set;
};

fold_fusion :: forall a b c. Generator a b -> (a -> c -> c) -> b -> c -> c;
fold_fusion g f s b =
  (if has_next g s then (case next g s of {
                          (x, sa) -> fold_fusion g f sa (f x b);
                        })
    else b);

length_last_fusion :: forall a b. Generator a b -> b -> (Nat, a);
length_last_fusion g s =
  (if has_next g s
    then (case next g s of {
           (x, sa) ->
             fold_fusion g (\ xa (n, _) -> (plus_nat n one_nat, xa)) sa
               (one_nat, x);
         })
    else (zero_nat, error "undefined"));

minus_nat :: Nat -> Nat -> Nat;
minus_nat m n = Nat (max (0 :: Integer) (integer_of_nat m - integer_of_nat n));

less_nat :: Nat -> Nat -> Bool;
less_nat m n = integer_of_nat m < integer_of_nat n;

gen_length_fusion :: forall a b. Generator a b -> Nat -> b -> Nat;
gen_length_fusion g n s =
  (if has_next g s then gen_length_fusion g (suc n) (snd (next g s)) else n);

length_fusion :: forall a b. Generator a b -> b -> Nat;
length_fusion g = gen_length_fusion g zero_nat;

class (Finite_UNIV a) => Card_UNIV a where {
  card_UNIVa :: Phantom a Nat;
};

card_UNIV :: forall a. (Card_UNIV a) => Phantom a Nat;
card_UNIV = card_UNIVa;

proper_interval_set_Compl_aux_fusion ::
  forall a b c.
    (Card_UNIV a) => (a -> a -> Bool) ->
                       (Maybe a -> Maybe a -> Bool) ->
                         Generator a b ->
                           Generator a c -> Maybe a -> Nat -> b -> c -> Bool;
proper_interval_set_Compl_aux_fusion less proper_interval g1 g2 ao n s1 s2 =
  (if has_next g1 s1
    then (case next g1 s1 of {
           (x, s1a) ->
             (if has_next g2 s2
               then (case next g2 s2 of {
                      (y, s2a) ->
                        (if less x y
                          then proper_interval ao (Just x) ||
                                 proper_interval_set_Compl_aux_fusion less
                                   proper_interval g1 g2 (Just x)
                                   (plus_nat n one_nat) s1a s2
                          else (if less y x
                                 then proper_interval ao (Just y) ||
proper_interval_set_Compl_aux_fusion less proper_interval g1 g2 (Just y)
  (plus_nat n one_nat) s1 s2a
                                 else proper_interval ao (Just x) &&
let {
  m = minus_nat (of_phantom (card_UNIV :: Phantom a Nat)) n;
} in not (equal_nat (minus_nat m (length_fusion g2 s2a))
           (nat_of_integer (2 :: Integer))) ||
       not (equal_nat (minus_nat m (length_fusion g1 s1a))
             (nat_of_integer (2 :: Integer)))));
                    })
               else let {
                      m = minus_nat (of_phantom (card_UNIV :: Phantom a Nat)) n;
                    } in (case length_last_fusion g1 s1 of {
                           (len_x, xa) ->
                             not (equal_nat m len_x) &&
                               (if equal_nat m (plus_nat len_x one_nat)
                                 then not (proper_interval (Just xa) Nothing)
                                 else True);
                         }));
         })
    else (if has_next g2 s2
           then (case next g2 s2 of {
                  (_, _) ->
                    let {
                      m = minus_nat (of_phantom (card_UNIV :: Phantom a Nat)) n;
                    } in (case length_last_fusion g2 s2 of {
                           (len_y, y) ->
                             not (equal_nat m len_y) &&
                               (if equal_nat m (plus_nat len_y one_nat)
                                 then not (proper_interval (Just y) Nothing)
                                 else True);
                         });
                })
           else less_nat (plus_nat n one_nat)
                  (of_phantom (card_UNIV :: Phantom a Nat))));

proper_interval_Compl_set_aux_fusion ::
  forall a b c.
    (a -> a -> Bool) ->
      (Maybe a -> Maybe a -> Bool) ->
        Generator a b -> Generator a c -> Maybe a -> b -> c -> Bool;
proper_interval_Compl_set_aux_fusion less proper_interval g1 g2 ao s1 s2 =
  has_next g1 s1 &&
    has_next g2 s2 &&
      (case next g1 s1 of {
        (x, s1a) ->
          (case next g2 s2 of {
            (y, s2a) ->
              (if less x y
                then not (proper_interval ao (Just x)) &&
                       proper_interval_Compl_set_aux_fusion less proper_interval
                         g1 g2 (Just x) s1a s2
                else (if less y x
                       then not (proper_interval ao (Just y)) &&
                              proper_interval_Compl_set_aux_fusion less
                                proper_interval g1 g2 (Just y) s1 s2a
                       else not (proper_interval ao (Just x)) &&
                              (has_next g2 s2a || has_next g1 s1a)));
          });
      });

exhaustive_above_fusion ::
  forall a b. (Maybe a -> Maybe a -> Bool) -> Generator a b -> a -> b -> Bool;
exhaustive_above_fusion proper_interval g y s =
  (if has_next g s
    then (case next g s of {
           (x, sa) ->
             not (proper_interval (Just y) (Just x)) &&
               exhaustive_above_fusion proper_interval g x sa;
         })
    else not (proper_interval (Just y) Nothing));

proper_interval_set_aux_fusion ::
  forall a b c.
    (a -> a -> Bool) ->
      (Maybe a -> Maybe a -> Bool) ->
        Generator a b -> Generator a c -> b -> c -> Bool;
proper_interval_set_aux_fusion less proper_interval g1 g2 s1 s2 =
  has_next g2 s2 &&
    (case next g2 s2 of {
      (y, s2a) ->
        (if has_next g1 s1
          then (case next g1 s1 of {
                 (x, s1a) ->
                   (if less x y then False
                     else (if less y x
                            then proper_interval (Just y) (Just x) ||
                                   (has_next g2 s2a ||
                                     not (exhaustive_above_fusion
   proper_interval g1 x s1a))
                            else proper_interval_set_aux_fusion less
                                   proper_interval g1 g2 s1a s2a));
               })
          else has_next g2 s2a || proper_interval (Just y) Nothing);
    });

length_last :: forall a. [a] -> (Nat, a);
length_last (x : xs) =
  fold (\ xa (n, _) -> (plus_nat n one_nat, xa)) xs (one_nat, x);
length_last [] = (zero_nat, error "undefined");

proper_interval_set_Compl_aux ::
  forall a.
    (Card_UNIV a) => (a -> a -> Bool) ->
                       (Maybe a -> Maybe a -> Bool) ->
                         Maybe a -> Nat -> [a] -> [a] -> Bool;
proper_interval_set_Compl_aux less proper_interval ao n (x : xs) (y : ys) =
  (if less x y
    then proper_interval ao (Just x) ||
           proper_interval_set_Compl_aux less proper_interval (Just x)
             (plus_nat n one_nat) xs (y : ys)
    else (if less y x
           then proper_interval ao (Just y) ||
                  proper_interval_set_Compl_aux less proper_interval (Just y)
                    (plus_nat n one_nat) (x : xs) ys
           else proper_interval ao (Just x) &&
                  let {
                    m = minus_nat (of_phantom (card_UNIV :: Phantom a Nat)) n;
                  } in not (equal_nat (minus_nat m (size_list ys))
                             (nat_of_integer (2 :: Integer))) ||
                         not (equal_nat (minus_nat m (size_list xs))
                               (nat_of_integer (2 :: Integer)))));
proper_interval_set_Compl_aux less proper_interval ao n (x : xs) [] =
  let {
    m = minus_nat (of_phantom (card_UNIV :: Phantom a Nat)) n;
  } in (case length_last (x : xs) of {
         (len_x, xa) ->
           not (equal_nat m len_x) &&
             (if equal_nat m (plus_nat len_x one_nat)
               then not (proper_interval (Just xa) Nothing) else True);
       });
proper_interval_set_Compl_aux less proper_interval ao n [] (y : ys) =
  let {
    m = minus_nat (of_phantom (card_UNIV :: Phantom a Nat)) n;
  } in (case length_last (y : ys) of {
         (len_y, ya) ->
           not (equal_nat m len_y) &&
             (if equal_nat m (plus_nat len_y one_nat)
               then not (proper_interval (Just ya) Nothing) else True);
       });
proper_interval_set_Compl_aux less proper_interval ao n [] [] =
  less_nat (plus_nat n one_nat) (of_phantom (card_UNIV :: Phantom a Nat));

proper_interval_Compl_set_aux ::
  forall a.
    (a -> a -> Bool) ->
      (Maybe a -> Maybe a -> Bool) -> Maybe a -> [a] -> [a] -> Bool;
proper_interval_Compl_set_aux less proper_interval ao uu [] = False;
proper_interval_Compl_set_aux less proper_interval ao [] uv = False;
proper_interval_Compl_set_aux less proper_interval ao (x : xs) (y : ys) =
  (if less x y
    then not (proper_interval ao (Just x)) &&
           proper_interval_Compl_set_aux less proper_interval (Just x) xs
             (y : ys)
    else (if less y x
           then not (proper_interval ao (Just y)) &&
                  proper_interval_Compl_set_aux less proper_interval (Just y)
                    (x : xs) ys
           else not (proper_interval ao (Just x)) &&
                  (if null ys then not (null xs) else True)));

exhaustive_above :: forall a. (Maybe a -> Maybe a -> Bool) -> a -> [a] -> Bool;
exhaustive_above proper_interval x (y : ys) =
  not (proper_interval (Just x) (Just y)) &&
    exhaustive_above proper_interval y ys;
exhaustive_above proper_interval x [] = not (proper_interval (Just x) Nothing);

proper_interval_set_aux ::
  forall a.
    (a -> a -> Bool) -> (Maybe a -> Maybe a -> Bool) -> [a] -> [a] -> Bool;
proper_interval_set_aux less proper_interval (x : xs) (y : ys) =
  (if less x y then False
    else (if less y x
           then proper_interval (Just y) (Just x) ||
                  (not (null ys) || not (exhaustive_above proper_interval x xs))
           else proper_interval_set_aux less proper_interval xs ys));
proper_interval_set_aux less proper_interval [] (y : ys) =
  not (null ys) || proper_interval (Just y) Nothing;
proper_interval_set_aux less proper_interval xs [] = False;

exhaustive_fusion ::
  forall a b. (Maybe a -> Maybe a -> Bool) -> Generator a b -> b -> Bool;
exhaustive_fusion proper_interval g s =
  has_next g s &&
    (case next g s of {
      (x, sa) ->
        not (proper_interval Nothing (Just x)) &&
          exhaustive_above_fusion proper_interval g x sa;
    });

list_remdups :: forall a. (a -> a -> Bool) -> [a] -> [a];
list_remdups equal (x : xs) =
  (if list_member equal xs x then list_remdups equal xs
    else x : list_remdups equal xs);
list_remdups equal [] = [];

length :: forall a. (Ceq a) => Set_dlist a -> Nat;
length xa = size_list (list_of_dlist xa);

card :: forall a. (Card_UNIV a, Ceq a, Ccompare a) => Set a -> Nat;
card (Complement a) =
  let {
    aa = card a;
    s = of_phantom (card_UNIV :: Phantom a Nat);
  } in (if less_nat zero_nat s then minus_nat s aa
         else (if finite a then zero_nat
                else (error :: forall a. String -> (() -> a) -> a)
                       "card Complement: infinite"
                       (\ _ -> card (Complement a))));
card (Set_Monad xs) =
  (case ceq of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a) "card Set_Monad: ceq = None"
        (\ _ -> card (Set_Monad xs));
    Just eq -> size_list (list_remdups eq xs);
  });
card (RBT_set rbt) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "card RBT_set: ccompare = None" (\ _ -> card (RBT_set rbt));
    Just _ -> size_list (keysa rbt);
  });
card (DList_set dxs) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a) "card DList_set: ceq = None"
        (\ _ -> card (DList_set dxs));
    Just _ -> length dxs;
  });

is_UNIV :: forall a. (Card_UNIV a, Ceq a, Cproper_interval a) => Set a -> Bool;
is_UNIV (RBT_set rbt) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "is_UNIV RBT_set: ccompare = None" (\ _ -> is_UNIV (RBT_set rbt));
    Just _ ->
      of_phantom (finite_UNIV :: Phantom a Bool) &&
        exhaustive_fusion cproper_interval rbt_keys_generator (init rbt);
  });
is_UNIV a =
  let {
    aa = of_phantom (card_UNIV :: Phantom a Nat);
    b = card a;
  } in (if less_nat zero_nat aa then equal_nat aa b
         else (if less_nat zero_nat b then False
                else (error :: forall a. String -> (() -> a) -> a)
                       "is_UNIV called on infinite type and set"
                       (\ _ -> is_UNIV a)));

is_emptya :: forall a b. (Ccompare a) => Mapping_rbt a b -> Bool;
is_emptya xa = (case impl_ofa xa of {
                 Empty -> True;
                 Branch _ _ _ _ _ -> False;
               });

nulla :: forall a. (Ceq a) => Set_dlist a -> Bool;
nulla xa = null (list_of_dlist xa);

is_empty :: forall a. (Card_UNIV a, Ceq a, Cproper_interval a) => Set a -> Bool;
is_empty (Complement a) = is_UNIV a;
is_empty (RBT_set rbt) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "is_empty RBT_set: ccompare = None" (\ _ -> is_empty (RBT_set rbt));
    Just _ -> is_emptya rbt;
  });
is_empty (DList_set dxs) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "is_empty DList_set: ceq = None" (\ _ -> is_empty (DList_set dxs));
    Just _ -> nulla dxs;
  });
is_empty (Set_Monad xs) = null xs;

cproper_interval_set ::
  forall a.
    (Card_UNIV a, Ceq a, Cproper_interval a,
      Set_impl a) => Maybe (Set a) -> Maybe (Set a) -> Bool;
cproper_interval_set (Just (Complement (RBT_set rbt1))) (Just (RBT_set rbt2)) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cproper_interval (Complement RBT_set) RBT_set: ccompare = None"
        (\ _ ->
          cproper_interval_set (Just (Complement (RBT_set rbt1)))
            (Just (RBT_set rbt2)));
    Just c ->
      (finite :: Set a -> Bool) (top_set :: Set a) &&
        proper_interval_Compl_set_aux_fusion (lt_of_comp c) cproper_interval
          rbt_keys_generator rbt_keys_generator Nothing (init rbt1) (init rbt2);
  });
cproper_interval_set (Just (RBT_set rbt1)) (Just (Complement (RBT_set rbt2))) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cproper_interval RBT_set (Complement RBT_set): ccompare = None"
        (\ _ ->
          cproper_interval_set (Just (RBT_set rbt1))
            (Just (Complement (RBT_set rbt2))));
    Just c ->
      (finite :: Set a -> Bool) (top_set :: Set a) &&
        proper_interval_set_Compl_aux_fusion (lt_of_comp c) cproper_interval
          rbt_keys_generator rbt_keys_generator Nothing zero_nat (init rbt1)
          (init rbt2);
  });
cproper_interval_set (Just (RBT_set rbt1)) (Just (RBT_set rbt2)) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cproper_interval RBT_set RBT_set: ccompare = None"
        (\ _ ->
          cproper_interval_set (Just (RBT_set rbt1)) (Just (RBT_set rbt2)));
    Just c ->
      (finite :: Set a -> Bool) (top_set :: Set a) &&
        proper_interval_set_aux_fusion (lt_of_comp c) cproper_interval
          rbt_keys_generator rbt_keys_generator (init rbt1) (init rbt2);
  });
cproper_interval_set (Just (Complement a)) (Just (Complement b)) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cproper_interval Complement Complement: ccompare = None"
        (\ _ ->
          cproper_interval_set (Just (Complement a)) (Just (Complement b)));
    Just _ -> cproper_interval_set (Just b) (Just a);
  });
cproper_interval_set (Just (Complement a)) (Just b) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cproper_interval Complement1: ccompare = None"
        (\ _ -> cproper_interval_set (Just (Complement a)) (Just b));
    Just c ->
      (finite :: Set a -> Bool) (top_set :: Set a) &&
        proper_interval_Compl_set_aux (lt_of_comp c) cproper_interval Nothing
          (csorted_list_of_set a) (csorted_list_of_set b);
  });
cproper_interval_set (Just a) (Just (Complement b)) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cproper_interval Complement2: ccompare = None"
        (\ _ -> cproper_interval_set (Just a) (Just (Complement b)));
    Just c ->
      (finite :: Set a -> Bool) (top_set :: Set a) &&
        proper_interval_set_Compl_aux (lt_of_comp c) cproper_interval Nothing
          zero_nat (csorted_list_of_set a) (csorted_list_of_set b);
  });
cproper_interval_set (Just a) (Just b) =
  (case ccompare of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "cproper_interval: ccompare = None"
        (\ _ -> cproper_interval_set (Just a) (Just b));
    Just c ->
      (finite :: Set a -> Bool) (top_set :: Set a) &&
        proper_interval_set_aux (lt_of_comp c) cproper_interval
          (csorted_list_of_set a) (csorted_list_of_set b);
  });
cproper_interval_set (Just a) Nothing = not (is_UNIV a);
cproper_interval_set Nothing (Just b) = not (is_empty b);
cproper_interval_set Nothing Nothing = True;

instance (Card_UNIV a, Ceq a, Cproper_interval a,
           Set_impl a) => Cproper_interval (Set a) where {
  cproper_interval = cproper_interval_set;
};

rec_list :: forall a b. a -> (b -> [b] -> a -> a) -> [b] -> a;
rec_list f1 f2 [] = f1;
rec_list f1 f2 (x21 : x22) = f2 x21 x22 (rec_list f1 f2 x22);

less_eq_list :: forall a. (Eq a, Ord a) => [a] -> [a] -> Bool;
less_eq_list =
  (\ x y ->
    rec_list (\ a -> (case a of {
                       [] -> False;
                       _ : _ -> True;
                     }))
      (\ x_0 _ res_0 a ->
        (case a of {
          [] -> False;
          y_0 : y_1 -> less x_0 y_0 || x_0 == y_0 && res_0 y_1;
        }))
      x y ||
      x == y);

less_list :: forall a. (Eq a, Ord a) => [a] -> [a] -> Bool;
less_list =
  rec_list (\ a -> (case a of {
                     [] -> False;
                     _ : _ -> True;
                   }))
    (\ x_0 _ res_0 a ->
      (case a of {
        [] -> False;
        y_0 : y_1 -> less x_0 y_0 || x_0 == y_0 && res_0 y_1;
      }));

instance (Eq a, Ord a) => Ord [a] where {
  less_eq = less_eq_list;
  less = less_list;
};

instance (Eq a, Order a) => Preorder [a] where {
};

instance (Eq a, Order a) => Order [a] where {
};

equality_list :: forall a. (a -> a -> Bool) -> [a] -> [a] -> Bool;
equality_list eq_a (x : xa) (y : ya) = eq_a x y && equality_list eq_a xa ya;
equality_list eq_a (x : xa) [] = False;
equality_list eq_a [] (y : ya) = False;
equality_list eq_a [] [] = True;

ceq_list :: forall a. (Ceq a) => Maybe ([a] -> [a] -> Bool);
ceq_list = (case ceq of {
             Nothing -> Nothing;
             Just eq_a -> Just (equality_list eq_a);
           });

instance (Ceq a) => Ceq [a] where {
  ceq = ceq_list;
};

set_impl_list :: forall a. Phantom [a] Set_impla;
set_impl_list = Phantom Set_Choose;

instance Set_impl [a] where {
  set_impl = set_impl_list;
};

class (Order a) => Linorder a where {
};

instance (Eq a, Linorder a) => Linorder [a] where {
};

cEnum_list ::
  forall a. Maybe ([[a]], (([a] -> Bool) -> Bool, ([a] -> Bool) -> Bool));
cEnum_list = Nothing;

instance Cenum [a] where {
  cEnum = cEnum_list;
};

finite_UNIV_list :: forall a. Phantom [a] Bool;
finite_UNIV_list = Phantom False;

instance Finite_UNIV [a] where {
  finite_UNIV = finite_UNIV_list;
};

comparator_list :: forall a. (a -> a -> Ordera) -> [a] -> [a] -> Ordera;
comparator_list comp_a (x : xa) (y : ya) =
  (case comp_a x y of {
    Eqa -> comparator_list comp_a xa ya;
    Lt -> Lt;
    Gt -> Gt;
  });
comparator_list comp_a (x : xa) [] = Gt;
comparator_list comp_a [] (y : ya) = Lt;
comparator_list comp_a [] [] = Eqa;

ccompare_list :: forall a. (Ccompare a) => Maybe ([a] -> [a] -> Ordera);
ccompare_list = (case ccompare of {
                  Nothing -> Nothing;
                  Just comp_a -> Just (comparator_list comp_a);
                });

instance (Ccompare a) => Ccompare [a] where {
  ccompare = ccompare_list;
};

data Mapping_impla = Mapping_Choose | Mapping_Assoc_List | Mapping_RBT
  | Mapping_Mapping;

mapping_impl_list :: forall a. Phantom [a] Mapping_impla;
mapping_impl_list = Phantom Mapping_Choose;

class Mapping_impl a where {
  mapping_impl :: Phantom a Mapping_impla;
};

instance Mapping_impl [a] where {
  mapping_impl = mapping_impl_list;
};

cproper_interval_list ::
  forall a. (Ccompare a) => Maybe [a] -> Maybe [a] -> Bool;
cproper_interval_list xso yso = error "undefined";

instance (Ccompare a) => Cproper_interval [a] where {
  cproper_interval = cproper_interval_list;
};

data Sum a b = Inl a | Inr b;

equal_sum :: forall a b. (Eq a, Eq b) => Sum a b -> Sum a b -> Bool;
equal_sum (Inl x1) (Inr x2) = False;
equal_sum (Inr x2) (Inl x1) = False;
equal_sum (Inr x2) (Inr y2) = x2 == y2;
equal_sum (Inl x1) (Inl y1) = x1 == y1;

instance (Eq a, Eq b) => Eq (Sum a b) where {
  a == b = equal_sum a b;
};

less_eq_sum :: forall a b. (Ord a, Ord b) => Sum a b -> Sum a b -> Bool;
less_eq_sum (Inl a) (Inl b) = less_eq a b;
less_eq_sum (Inl a) (Inr b) = True;
less_eq_sum (Inr a) (Inl b) = False;
less_eq_sum (Inr a) (Inr b) = less_eq a b;

less_sum ::
  forall a b. (Eq a, Ord a, Eq b, Ord b) => Sum a b -> Sum a b -> Bool;
less_sum a b = less_eq_sum a b && not (equal_sum a b);

instance (Eq a, Ord a, Eq b, Ord b) => Ord (Sum a b) where {
  less_eq = less_eq_sum;
  less = less_sum;
};

instance (Eq a, Linorder a, Eq b, Linorder b) => Preorder (Sum a b) where {
};

instance (Eq a, Linorder a, Eq b, Linorder b) => Order (Sum a b) where {
};

equality_sum ::
  forall a b.
    (a -> a -> Bool) -> (b -> b -> Bool) -> Sum a b -> Sum a b -> Bool;
equality_sum eq_a eq_b (Inr x) (Inr ya) = eq_b x ya;
equality_sum eq_a eq_b (Inr x) (Inl y) = False;
equality_sum eq_a eq_b (Inl x) (Inr ya) = False;
equality_sum eq_a eq_b (Inl x) (Inl y) = eq_a x y;

ceq_sum :: forall a b. (Ceq a, Ceq b) => Maybe (Sum a b -> Sum a b -> Bool);
ceq_sum = (case ceq of {
            Nothing -> Nothing;
            Just eq_a -> (case ceq of {
                           Nothing -> Nothing;
                           Just eq_b -> Just (equality_sum eq_a eq_b);
                         });
          });

instance (Ceq a, Ceq b) => Ceq (Sum a b) where {
  ceq = ceq_sum;
};

set_impl_choose2 :: Set_impla -> Set_impla -> Set_impla;
set_impl_choose2 Set_Monada Set_Monada = Set_Monada;
set_impl_choose2 Set_RBT Set_RBT = Set_RBT;
set_impl_choose2 Set_DList Set_DList = Set_DList;
set_impl_choose2 Set_Collect Set_Collect = Set_Collect;
set_impl_choose2 x y = Set_Choose;

set_impl_sum ::
  forall a b. (Set_impl a, Set_impl b) => Phantom (Sum a b) Set_impla;
set_impl_sum =
  Phantom
    (set_impl_choose2 (of_phantom (set_impl :: Phantom a Set_impla))
      (of_phantom (set_impl :: Phantom b Set_impla)));

instance (Set_impl a, Set_impl b) => Set_impl (Sum a b) where {
  set_impl = set_impl_sum;
};

instance (Eq a, Linorder a, Eq b, Linorder b) => Linorder (Sum a b) where {
};

finite_UNIV_sum ::
  forall a b. (Finite_UNIV a, Finite_UNIV b) => Phantom (Sum a b) Bool;
finite_UNIV_sum =
  Phantom
    (of_phantom (finite_UNIV :: Phantom a Bool) &&
      of_phantom (finite_UNIV :: Phantom b Bool));

card_UNIV_sum ::
  forall a b. (Card_UNIV a, Card_UNIV b) => Phantom (Sum a b) Nat;
card_UNIV_sum =
  Phantom
    (let {
       ca = of_phantom (card_UNIVa :: Phantom a Nat);
       cb = of_phantom (card_UNIVa :: Phantom b Nat);
     } in (if not (equal_nat ca zero_nat) && not (equal_nat cb zero_nat)
            then plus_nat ca cb else zero_nat));

instance (Finite_UNIV a, Finite_UNIV b) => Finite_UNIV (Sum a b) where {
  finite_UNIV = finite_UNIV_sum;
};

instance (Card_UNIV a, Card_UNIV b) => Card_UNIV (Sum a b) where {
  card_UNIVa = card_UNIV_sum;
};

cEnum_sum ::
  forall a b.
    (Cenum a,
      Cenum b) => Maybe ([Sum a b],
                          ((Sum a b -> Bool) -> Bool,
                            (Sum a b -> Bool) -> Bool));
cEnum_sum =
  (case cEnum of {
    Nothing -> Nothing;
    Just (enum_a, (enum_all_a, enum_ex_a)) ->
      (case cEnum of {
        Nothing -> Nothing;
        Just (enum_b, (enum_all_b, enum_ex_b)) ->
          Just (map Inl enum_a ++ map Inr enum_b,
                 ((\ p ->
                    enum_all_a (\ x -> p (Inl x)) &&
                      enum_all_b (\ x -> p (Inr x))),
                   (\ p ->
                     enum_ex_a (\ x -> p (Inl x)) ||
                       enum_ex_b (\ x -> p (Inr x)))));
      });
  });

instance (Cenum a, Cenum b) => Cenum (Sum a b) where {
  cEnum = cEnum_sum;
};

comparator_sum ::
  forall a b.
    (a -> a -> Ordera) -> (b -> b -> Ordera) -> Sum a b -> Sum a b -> Ordera;
comparator_sum comp_a comp_b (Inr x) (Inr ya) = comp_b x ya;
comparator_sum comp_a comp_b (Inr x) (Inl y) = Gt;
comparator_sum comp_a comp_b (Inl x) (Inr ya) = Lt;
comparator_sum comp_a comp_b (Inl x) (Inl y) = comp_a x y;

ccompare_sum ::
  forall a b. (Ccompare a, Ccompare b) => Maybe (Sum a b -> Sum a b -> Ordera);
ccompare_sum =
  (case ccompare of {
    Nothing -> Nothing;
    Just comp_a -> (case ccompare of {
                     Nothing -> Nothing;
                     Just comp_b -> Just (comparator_sum comp_a comp_b);
                   });
  });

instance (Ccompare a, Ccompare b) => Ccompare (Sum a b) where {
  ccompare = ccompare_sum;
};

mapping_impl_choose2 :: Mapping_impla -> Mapping_impla -> Mapping_impla;
mapping_impl_choose2 Mapping_RBT Mapping_RBT = Mapping_RBT;
mapping_impl_choose2 Mapping_Assoc_List Mapping_Assoc_List = Mapping_Assoc_List;
mapping_impl_choose2 Mapping_Mapping Mapping_Mapping = Mapping_Mapping;
mapping_impl_choose2 x y = Mapping_Choose;

mapping_impl_sum ::
  forall a b.
    (Mapping_impl a, Mapping_impl b) => Phantom (Sum a b) Mapping_impla;
mapping_impl_sum =
  Phantom
    (mapping_impl_choose2 (of_phantom (mapping_impl :: Phantom a Mapping_impla))
      (of_phantom (mapping_impl :: Phantom b Mapping_impla)));

instance (Mapping_impl a, Mapping_impl b) => Mapping_impl (Sum a b) where {
  mapping_impl = mapping_impl_sum;
};

cproper_interval_sum ::
  forall a b.
    (Cproper_interval a,
      Cproper_interval b) => Maybe (Sum a b) -> Maybe (Sum a b) -> Bool;
cproper_interval_sum Nothing Nothing = True;
cproper_interval_sum Nothing (Just (Inl x)) = cproper_interval Nothing (Just x);
cproper_interval_sum Nothing (Just (Inr y)) = True;
cproper_interval_sum (Just (Inl x)) Nothing = True;
cproper_interval_sum (Just (Inl x)) (Just (Inl y)) =
  cproper_interval (Just x) (Just y);
cproper_interval_sum (Just (Inl x)) (Just (Inr y)) =
  cproper_interval (Just x) Nothing || cproper_interval Nothing (Just y);
cproper_interval_sum (Just (Inr y)) Nothing = cproper_interval (Just y) Nothing;
cproper_interval_sum (Just (Inr y)) (Just (Inl x)) = False;
cproper_interval_sum (Just (Inr x)) (Just (Inr y)) =
  cproper_interval (Just x) (Just y);

instance (Cproper_interval a,
           Cproper_interval b) => Cproper_interval (Sum a b) where {
  cproper_interval = cproper_interval_sum;
};

equality_option :: forall a. (a -> a -> Bool) -> Maybe a -> Maybe a -> Bool;
equality_option eq_a (Just x) (Just y) = eq_a x y;
equality_option eq_a (Just x) Nothing = False;
equality_option eq_a Nothing (Just y) = False;
equality_option eq_a Nothing Nothing = True;

ceq_option :: forall a. (Ceq a) => Maybe (Maybe a -> Maybe a -> Bool);
ceq_option = (case ceq of {
               Nothing -> Nothing;
               Just eq_a -> Just (equality_option eq_a);
             });

instance (Ceq a) => Ceq (Maybe a) where {
  ceq = ceq_option;
};

set_impl_option :: forall a. (Set_impl a) => Phantom (Maybe a) Set_impla;
set_impl_option = Phantom (of_phantom (set_impl :: Phantom a Set_impla));

instance (Set_impl a) => Set_impl (Maybe a) where {
  set_impl = set_impl_option;
};

comparator_option ::
  forall a. (a -> a -> Ordera) -> Maybe a -> Maybe a -> Ordera;
comparator_option comp_a (Just x) (Just y) = comp_a x y;
comparator_option comp_a (Just x) Nothing = Gt;
comparator_option comp_a Nothing (Just y) = Lt;
comparator_option comp_a Nothing Nothing = Eqa;

ccompare_option ::
  forall a. (Ccompare a) => Maybe (Maybe a -> Maybe a -> Ordera);
ccompare_option = (case ccompare of {
                    Nothing -> Nothing;
                    Just comp_a -> Just (comparator_option comp_a);
                  });

instance (Ccompare a) => Ccompare (Maybe a) where {
  ccompare = ccompare_option;
};

less_eq_prod :: forall a b. (Ord a, Ord b) => (a, b) -> (a, b) -> Bool;
less_eq_prod (x1, y1) (x2, y2) = less x1 x2 || less_eq x1 x2 && less_eq y1 y2;

less_prod :: forall a b. (Ord a, Ord b) => (a, b) -> (a, b) -> Bool;
less_prod (x1, y1) (x2, y2) = less x1 x2 || less_eq x1 x2 && less y1 y2;

instance (Ord a, Ord b) => Ord (a, b) where {
  less_eq = less_eq_prod;
  less = less_prod;
};

instance (Preorder a, Preorder b) => Preorder (a, b) where {
};

instance (Order a, Order b) => Order (a, b) where {
};

set_impl_prod ::
  forall a b. (Set_impl a, Set_impl b) => Phantom (a, b) Set_impla;
set_impl_prod =
  Phantom
    (set_impl_choose2 (of_phantom (set_impl :: Phantom a Set_impla))
      (of_phantom (set_impl :: Phantom b Set_impla)));

instance (Set_impl a, Set_impl b) => Set_impl (a, b) where {
  set_impl = set_impl_prod;
};

instance (Linorder a, Linorder b) => Linorder (a, b) where {
};

finite_UNIV_prod ::
  forall a b. (Finite_UNIV a, Finite_UNIV b) => Phantom (a, b) Bool;
finite_UNIV_prod =
  Phantom
    (of_phantom (finite_UNIV :: Phantom a Bool) &&
      of_phantom (finite_UNIV :: Phantom b Bool));

times_nat :: Nat -> Nat -> Nat;
times_nat m n = Nat (integer_of_nat m * integer_of_nat n);

card_UNIV_prod :: forall a b. (Card_UNIV a, Card_UNIV b) => Phantom (a, b) Nat;
card_UNIV_prod =
  Phantom
    (times_nat (of_phantom (card_UNIVa :: Phantom a Nat))
      (of_phantom (card_UNIVa :: Phantom b Nat)));

instance (Finite_UNIV a, Finite_UNIV b) => Finite_UNIV (a, b) where {
  finite_UNIV = finite_UNIV_prod;
};

instance (Card_UNIV a, Card_UNIV b) => Card_UNIV (a, b) where {
  card_UNIVa = card_UNIV_prod;
};

mapping_impl_prod ::
  forall a b. (Mapping_impl a, Mapping_impl b) => Phantom (a, b) Mapping_impla;
mapping_impl_prod =
  Phantom
    (mapping_impl_choose2 (of_phantom (mapping_impl :: Phantom a Mapping_impla))
      (of_phantom (mapping_impl :: Phantom b Mapping_impla)));

instance (Mapping_impl a, Mapping_impl b) => Mapping_impl (a, b) where {
  mapping_impl = mapping_impl_prod;
};

cproper_interval_prod ::
  forall a b.
    (Cproper_interval a,
      Cproper_interval b) => Maybe (a, b) -> Maybe (a, b) -> Bool;
cproper_interval_prod Nothing Nothing = True;
cproper_interval_prod Nothing (Just (y1, y2)) =
  cproper_interval Nothing (Just y1) || cproper_interval Nothing (Just y2);
cproper_interval_prod (Just (x1, x2)) Nothing =
  cproper_interval (Just x1) Nothing || cproper_interval (Just x2) Nothing;
cproper_interval_prod (Just (x1, x2)) (Just (y1, y2)) =
  cproper_interval (Just x1) (Just y1) ||
    (lt_of_comp (the ccompare) x1 y1 &&
       (cproper_interval (Just x2) Nothing ||
         cproper_interval Nothing (Just y2)) ||
      not (lt_of_comp (the ccompare) y1 x1) &&
        cproper_interval (Just x2) (Just y2));

instance (Cproper_interval a,
           Cproper_interval b) => Cproper_interval (a, b) where {
  cproper_interval = cproper_interval_prod;
};

instance Preorder Integer where {
};

instance Order Integer where {
};

ceq_integer :: Maybe (Integer -> Integer -> Bool);
ceq_integer = Just (\ a b -> a == b);

instance Ceq Integer where {
  ceq = ceq_integer;
};

set_impl_integer :: Phantom Integer Set_impla;
set_impl_integer = Phantom Set_RBT;

instance Set_impl Integer where {
  set_impl = set_impl_integer;
};

instance Linorder Integer where {
};

finite_UNIV_integer :: Phantom Integer Bool;
finite_UNIV_integer = Phantom False;

card_UNIV_integer :: Phantom Integer Nat;
card_UNIV_integer = Phantom zero_nat;

instance Finite_UNIV Integer where {
  finite_UNIV = finite_UNIV_integer;
};

instance Card_UNIV Integer where {
  card_UNIVa = card_UNIV_integer;
};

cEnum_integer ::
  Maybe ([Integer], ((Integer -> Bool) -> Bool, (Integer -> Bool) -> Bool));
cEnum_integer = Nothing;

instance Cenum Integer where {
  cEnum = cEnum_integer;
};

{- Original implementation:
comparator_of :: forall a. (Eq a, Linorder a) => a -> a -> Ordera;
comparator_of x y = (if less x y then Lt else (if x == y then Eqa else Gt));

compare_integer :: Integer -> Integer -> Ordera;
compare_integer = comparator_of;
-}

compare_integer :: Integer -> Integer -> Ordera;
compare_integer x y = (case (Prelude.compare x y) of {
  Prelude.LT -> Lt;
  Prelude.EQ -> Eqa; 
  Prelude.GT -> Gt; 
  });

ccompare_integer :: Maybe (Integer -> Integer -> Ordera);
ccompare_integer = Just compare_integer;

instance Ccompare Integer where {
  ccompare = ccompare_integer;
};

mapping_impl_integer :: Phantom Integer Mapping_impla;
mapping_impl_integer = Phantom Mapping_RBT;

instance Mapping_impl Integer where {
  mapping_impl = mapping_impl_integer;
};

proper_interval_integer :: Maybe Integer -> Maybe Integer -> Bool;
proper_interval_integer xo Nothing = True;
proper_interval_integer Nothing yo = True;
proper_interval_integer (Just x) (Just y) = (1 :: Integer) < y - x;

cproper_interval_integer :: Maybe Integer -> Maybe Integer -> Bool;
cproper_interval_integer = proper_interval_integer;

instance Cproper_interval Integer where {
  cproper_interval = cproper_interval_integer;
};

newtype Alist b a = Alist [(b, a)];

data Mapping a b = Assoc_List_Mapping (Alist a b)
  | RBT_Mapping (Mapping_rbt a b) | Mapping (a -> Maybe b);

data Test_suite a b c d =
  Test_Suite (Set (a, Fsm a b c)) (a -> Set [(a, (b, (c, a)))])
    ((a, [(a, (b, (c, a)))]) -> Set a) ((a, a) -> Set (Fsm d b c, (d, d)));

newtype Comp_fun_idem b a = Abs_comp_fun_idem (b -> a -> a);

newtype Mp_trie a = MP_Trie [(a, Mp_trie a)];

impl_of :: forall b a. Alist b a -> [(b, a)];
impl_of (Alist x) = x;

update :: forall a b. (Eq a) => a -> b -> [(a, b)] -> [(a, b)];
update k v [] = [(k, v)];
update k v (p : ps) = (if fst p == k then (k, v) : ps else p : update k v ps);

updatea :: forall a b. (Eq a) => a -> b -> Alist a b -> Alist a b;
updatea xc xd xe = Alist (update xc xd (impl_of xe));

fun_upd :: forall a b. (Eq a) => (a -> b) -> a -> b -> a -> b;
fun_upd f a b = (\ x -> (if x == a then b else f x));

updateb ::
  forall a b. (Ccompare a, Eq a) => a -> b -> Mapping a b -> Mapping a b;
updateb k v (RBT_Mapping t) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "update RBT_Mapping: ccompare = None"
        (\ _ -> updateb k v (RBT_Mapping t));
    Just _ -> RBT_Mapping (insertb k v t);
  });
updateb k v (Assoc_List_Mapping al) = Assoc_List_Mapping (updatea k v al);
updateb k v (Mapping m) = Mapping (fun_upd m k (Just v));

map_of :: forall a b. (Eq a) => [(a, b)] -> a -> Maybe b;
map_of ((l, v) : ps) k = (if l == k then Just v else map_of ps k);
map_of [] k = Nothing;

lookup :: forall a b. (Eq a) => Alist a b -> a -> Maybe b;
lookup xa = map_of (impl_of xa);

lookupa :: forall a b. (Ccompare a, Eq a) => Mapping a b -> a -> Maybe b;
lookupa (RBT_Mapping t) = lookupb t;
lookupa (Assoc_List_Mapping al) = lookup al;

foldb ::
  forall a b. (Ccompare a) => (a -> b -> b) -> Mapping_rbt a () -> b -> b;
foldb x xc = folda (\ a _ -> x a) (impl_ofa xc);

empty :: forall a b. Alist a b;
empty = Alist [];

mapping_empty_choose :: forall a b. (Ccompare a) => Mapping a b;
mapping_empty_choose = (case (ccompare :: Maybe (a -> a -> Ordera)) of {
                         Nothing -> Assoc_List_Mapping empty;
                         Just _ -> RBT_Mapping emptyc;
                       });

mapping_empty :: forall a b. (Ccompare a) => Mapping_impla -> Mapping a b;
mapping_empty Mapping_RBT = RBT_Mapping emptyc;
mapping_empty Mapping_Assoc_List = Assoc_List_Mapping empty;
mapping_empty Mapping_Mapping = Mapping (\ _ -> Nothing);
mapping_empty Mapping_Choose = mapping_empty_choose;

emptya :: forall a b. (Ccompare a, Mapping_impl a) => Mapping a b;
emptya = mapping_empty (of_phantom (mapping_impl :: Phantom a Mapping_impla));

set_as_map_image ::
  forall a b c d.
    (Ccompare a, Ccompare b, Ccompare c, Eq c, Mapping_impl c, Ceq d,
      Ccompare d,
      Set_impl d) => Set (a, b) -> ((a, b) -> (c, d)) -> c -> Maybe (Set d);
set_as_map_image (RBT_set t) f1 =
  (case (ccompare_prod :: Maybe ((a, b) -> (a, b) -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "set_as_map_image RBT_set: ccompare = None"
        (\ _ -> set_as_map_image (RBT_set t) f1);
    Just _ ->
      lookupa
        (foldb
          (\ kv m1 ->
            (case f1 kv of {
              (x, z) -> (case lookupa m1 x of {
                          Nothing -> updateb x (insert z bot_set) m1;
                          Just zs -> updateb x (insert z zs) m1;
                        });
            }))
          t emptya);
  });

h :: forall a b c.
       (Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ccompare b, Eq b,
         Mapping_impl b, Ceq c, Ccompare c,
         Set_impl c) => Fsm a b c -> (a, b) -> Set (c, a);
h m (q, x) =
  let {
    ma = set_as_map_image (transitions m)
           (\ (qb, (xa, (y, qa))) -> ((qb, xa), (y, qa)));
  } in (case ma (q, x) of {
         Nothing -> bot_set;
         Just yqs -> yqs;
       });

dlist_ex :: forall a. (Ceq a) => (a -> Bool) -> Set_dlist a -> Bool;
dlist_ex x xc = any x (list_of_dlist xc);

rBT_Impl_rbt_ex :: forall a b. (a -> b -> Bool) -> Rbt a b -> Bool;
rBT_Impl_rbt_ex p (Branch c l k v r) =
  p k v || (rBT_Impl_rbt_ex p l || rBT_Impl_rbt_ex p r);
rBT_Impl_rbt_ex p Empty = False;

ex :: forall a b. (Ccompare a) => (a -> b -> Bool) -> Mapping_rbt a b -> Bool;
ex xb xc = rBT_Impl_rbt_ex xb (impl_ofa xc);

bex :: forall a. (Ceq a, Ccompare a) => Set a -> (a -> Bool) -> Bool;
bex (RBT_set rbt) p =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "Bex RBT_set: ccompare = None" (\ _ -> bex (RBT_set rbt) p);
    Just _ -> ex (\ k _ -> p k) rbt;
  });
bex (DList_set dxs) p =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a) "Bex DList_set: ceq = None"
        (\ _ -> bex (DList_set dxs) p);
    Just _ -> dlist_ex p dxs;
  });
bex (Set_Monad xs) p = any p xs;

size :: forall a b c. (Card_UNIV a, Ceq a, Ccompare a) => Fsm a b c -> Nat;
size m = card (nodes m);

rBT_Impl_rbt_all :: forall a b. (a -> b -> Bool) -> Rbt a b -> Bool;
rBT_Impl_rbt_all p (Branch c l k v r) =
  p k v && rBT_Impl_rbt_all p l && rBT_Impl_rbt_all p r;
rBT_Impl_rbt_all p Empty = True;

alla :: forall a b. (Ccompare a) => (a -> b -> Bool) -> Mapping_rbt a b -> Bool;
alla xb xc = rBT_Impl_rbt_all xb (impl_ofa xc);

ball :: forall a. (Ceq a, Ccompare a) => Set a -> (a -> Bool) -> Bool;
ball (RBT_set rbt) p =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "Ball RBT_set: ccompare = None" (\ _ -> ball (RBT_set rbt) p);
    Just _ -> alla (\ k _ -> p k) rbt;
  });
ball (DList_set dxs) p =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a) "Ball DList_set: ceq = None"
        (\ _ -> ball (DList_set dxs) p);
    Just _ -> dlist_all p dxs;
  });
ball (Set_Monad xs) p = all p xs;

find :: forall a. (a -> Bool) -> [a] -> Maybe a;
find uu [] = Nothing;
find p (x : xs) = (if p x then Just x else find p xs);

last :: forall a. [a] -> a;
last (x : xs) = (if null xs then x else last xs);

image ::
  forall a b.
    (Ceq a, Ccompare a, Ceq b, Ccompare b,
      Set_impl b) => (a -> b) -> Set a -> Set b;
image h (RBT_set rbt) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "image RBT_set: ccompare = None" (\ _ -> image h (RBT_set rbt));
    Just _ -> foldb (insert . h) rbt bot_set;
  });
image g (DList_set dxs) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "image DList_set: ceq = None" (\ _ -> image g (DList_set dxs));
    Just _ -> foldc (insert . g) dxs bot_set;
  });
image f (Complement (Complement b)) = image f b;
image f (Collect_set a) =
  (error :: forall a. String -> (() -> a) -> a) "image Collect_set"
    (\ _ -> image f (Collect_set a));
image f (Set_Monad xs) = Set_Monad (map f xs);

set_as_map ::
  forall a b.
    (Ceq a, Ccompare a, Eq a, Mapping_impl a, Ceq b, Ccompare b,
      Set_impl b) => Set (a, b) -> a -> Maybe (Set b);
set_as_map (DList_set xs) =
  (case (ceq_prod :: Maybe ((a, b) -> (a, b) -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "set_as_map RBT_set: ccompare = None"
        (\ _ -> set_as_map (DList_set xs));
    Just _ ->
      lookupa
        (foldc (\ (x, z) m -> (case lookupa m x of {
                                Nothing -> updateb x (insert z bot_set) m;
                                Just zs -> updateb x (insert z zs) m;
                              }))
          xs emptya);
  });
set_as_map (RBT_set t) =
  (case (ccompare_prod :: Maybe ((a, b) -> (a, b) -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "set_as_map RBT_set: ccompare = None" (\ _ -> set_as_map (RBT_set t));
    Just _ ->
      lookupa
        (foldb (\ (x, z) m -> (case lookupa m x of {
                                Nothing -> updateb x (insert z bot_set) m;
                                Just zs -> updateb x (insert z zs) m;
                              }))
          t emptya);
  });

h_from ::
  forall a b c.
    (Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ceq b, Ccompare b,
      Set_impl b, Ceq c, Ccompare c,
      Set_impl c) => Fsm a b c -> a -> Set (b, (c, a));
h_from m q = let {
               ma = set_as_map (transitions m);
             } in (case ma q of {
                    Nothing -> bot_set;
                    Just yqs -> yqs;
                  });

targeta :: forall a b c. a -> [(a, (b, (c, a)))] -> a;
targeta q [] = q;
targeta q (v : va) = snd (snd (snd (last (v : va))));

target :: forall a b c. a -> [(a, (b, (c, a)))] -> a;
target q p = targeta q p;

foldr :: forall a b. (a -> b -> b) -> [a] -> b -> b;
foldr f [] = id;
foldr f (x : xs) = f x . foldr f xs;

filtera :: forall a. (Ceq a, Ccompare a) => (a -> Bool) -> Set a -> Set a;
filtera p a = inf_set a (Collect_set p);

membera :: forall a. (Eq a) => [a] -> a -> Bool;
membera [] y = False;
membera (x : xs) y = x == y || membera xs y;

from_FSMa ::
  forall a b c.
    (Ceq a, Ccompare a) => Fsm_impl_ext a b c () -> a -> Fsm_impl_ext a b c ();
from_FSMa m q =
  (if member q (nodesa m)
    then Fsm_impl_ext q (nodesa m) (inputsa m) (outputsa m) (transitionsa m) ()
    else m);

from_FSM :: forall a b c. (Ceq a, Ccompare a) => Fsm a b c -> a -> Fsm a b c;
from_FSM xb xc = Fsm (from_FSMa (fsm_impl xb) xc);

butlast :: forall a. [a] -> [a];
butlast [] = [];
butlast (x : xs) = (if null xs then [] else x : butlast xs);

remdups :: forall a. (Eq a) => [a] -> [a];
remdups [] = [];
remdups (x : xs) = (if membera xs x then remdups xs else x : remdups xs);

remove1 :: forall a. (Eq a) => a -> [a] -> [a];
remove1 x [] = [];
remove1 x (y : xs) = (if x == y then xs else y : remove1 x xs);

pow_list :: forall a. [a] -> [[a]];
pow_list [] = [[]];
pow_list (x : xs) = let {
                      pxs = pow_list xs;
                    } in pxs ++ map (\ a -> x : a) pxs;

prefixes :: forall a. [a] -> [[a]];
prefixes [] = [[]];
prefixes (v : va) = prefixes (butlast (v : va)) ++ [v : va];

comp_fun_idem_apply :: forall b a. Comp_fun_idem b a -> b -> a -> a;
comp_fun_idem_apply (Abs_comp_fun_idem x) = x;

set_fold_cfi ::
  forall a b. (Ceq a, Ccompare a) => Comp_fun_idem a b -> b -> Set a -> b;
set_fold_cfi f b (RBT_set rbt) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "set_fold_cfi RBT_set: ccompare = None"
        (\ _ -> set_fold_cfi f b (RBT_set rbt));
    Just _ -> foldb (comp_fun_idem_apply f) rbt b;
  });
set_fold_cfi f b (DList_set dxs) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "set_fold_cfi DList_set: ceq = None"
        (\ _ -> set_fold_cfi f b (DList_set dxs));
    Just _ -> foldc (comp_fun_idem_apply f) dxs b;
  });
set_fold_cfi f b (Set_Monad xs) = fold (comp_fun_idem_apply f) xs b;
set_fold_cfi f b (Collect_set p) =
  (error :: forall a. String -> (() -> a) -> a)
    "set_fold_cfi not supported on Collect_set"
    (\ _ -> set_fold_cfi f b (Collect_set p));
set_fold_cfi f b (Complement a) =
  (error :: forall a. String -> (() -> a) -> a)
    "set_fold_cfi not supported on Complement"
    (\ _ -> set_fold_cfi f b (Complement a));

sup_cfi :: forall a. (Lattice a) => Comp_fun_idem a a;
sup_cfi = Abs_comp_fun_idem sup;

sup_seta ::
  forall a.
    (Finite_UNIV a, Cenum a, Ceq a, Cproper_interval a,
      Set_impl a) => Set (Set a) -> Set a;
sup_seta a =
  (if finite a then set_fold_cfi sup_cfi bot_set a
    else (error :: forall a. String -> (() -> a) -> a) "Sup: infinite"
           (\ _ -> sup_seta a));

acyclic_paths_up_to_lengtha ::
  forall a b c.
    (Ceq a, Ccompare a, Ceq b, Ccompare b, Ceq c,
      Ccompare c) => [(a, (b, (c, a)))] ->
                       a -> (a -> Set (b, (c, a))) ->
                              Set a -> Nat -> Set [(a, (b, (c, a)))];
acyclic_paths_up_to_lengtha prev q hF visitedNodes k =
  (if equal_nat k zero_nat
    then insert prev (set_empty (of_phantom set_impl_list))
    else let {
           tF = filtera (\ (_, (_, qa)) -> not (member qa visitedNodes)) (hF q);
         } in insert prev
                (sup_seta
                  (image
                    (\ (x, (y, qa)) ->
                      acyclic_paths_up_to_lengtha (prev ++ [(q, (x, (y, qa)))])
                        qa hF (insert qa visitedNodes) (minus_nat k one_nat))
                    tF)));

acyclic_paths_up_to_length ::
  forall a b c.
    (Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ceq b, Ccompare b,
      Set_impl b, Ceq c, Ccompare c,
      Set_impl c) => Fsm a b c -> a -> Nat -> Set [(a, (b, (c, a)))];
acyclic_paths_up_to_length m q k =
  (if member q (nodes m)
    then acyclic_paths_up_to_lengtha [] q
           (\ x -> (case set_as_map (transitions m) x of {
                     Nothing -> bot_set;
                     Just xs -> xs;
                   }))
           (insert q bot_set) k
    else set_empty (of_phantom set_impl_list));

lS_acyclic ::
  forall a b c.
    (Card_UNIV a, Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ceq b,
      Ccompare b, Set_impl b, Ceq c, Ccompare c,
      Set_impl c) => Fsm a b c -> a -> Set [(b, c)];
lS_acyclic m q =
  image (map (\ t -> (fst (snd t), fst (snd (snd t)))))
    (acyclic_paths_up_to_length m q (minus_nat (size m) one_nat));

observable ::
  forall a b c.
    (Ceq a, Ccompare a, Eq a, Ceq b, Ccompare b, Eq b, Ceq c, Ccompare c,
      Eq c) => Fsm a b c -> Bool;
observable m =
  ball (transitions m)
    (\ t1 ->
      ball (transitions m)
        (\ t2 ->
          (if fst t1 == fst t2 &&
                fst (snd t1) == fst (snd t2) &&
                  fst (snd (snd t1)) == fst (snd (snd t2))
            then snd (snd (snd t1)) == snd (snd (snd t2)) else True)));

removeAll :: forall a. (Eq a) => a -> [a] -> [a];
removeAll x [] = [];
removeAll x (y : xs) = (if x == y then removeAll x xs else y : removeAll x xs);

filter_nodesa ::
  forall a b c.
    (Ceq a, Ccompare a, Ceq b, Ccompare b, Ceq c,
      Ccompare c) => Fsm_impl_ext a b c () ->
                       (a -> Bool) -> Fsm_impl_ext a b c ();
filter_nodesa m p =
  (if p (initiala m)
    then Fsm_impl_ext (initiala m) (filtera p (nodesa m)) (inputsa m)
           (outputsa m)
           (filtera (\ t -> p (fst t) && p (snd (snd (snd t))))
             (transitionsa m))
           ()
    else m);

filter_nodes ::
  forall a b c.
    (Ceq a, Ccompare a, Ceq b, Ccompare b, Ceq c,
      Ccompare c) => Fsm a b c -> (a -> Bool) -> Fsm a b c;
filter_nodes xb xc = Fsm (filter_nodesa (fsm_impl xb) xc);

productc ::
  forall a b. (Ceq a, Ceq b) => Set_dlist a -> Set_dlist b -> Set_dlist (a, b);
productc dxs1 dxs2 =
  Abs_dlist (foldc (\ a -> foldc (\ c -> (\ b -> (a, c) : b)) dxs2) dxs1 []);

rbt_product ::
  forall a b c d e.
    (a -> b -> c -> d -> e) -> Rbt a b -> Rbt c d -> Rbt (a, c) e;
rbt_product f rbt1 rbt2 =
  rbtreeify
    (reverse
      (folda (\ a b -> folda (\ c d -> (\ e -> ((a, c), f a b c d) : e)) rbt2)
        rbt1 []));

productf ::
  forall a d b e c.
    (Ccompare a,
      Ccompare b) => (a -> d -> b -> e -> c) ->
                       Mapping_rbt a d ->
                         Mapping_rbt b e -> Mapping_rbt (a, b) c;
productf xc xd xe = Mapping_RBTa (rbt_product xc (impl_ofa xd) (impl_ofa xe));

productb ::
  forall a b.
    (Ccompare a,
      Ccompare b) => Mapping_rbt a () ->
                       Mapping_rbt b () -> Mapping_rbt (a, b) ();
productb rbt1 rbt2 = productf (\ _ _ _ _ -> ()) rbt1 rbt2;

producte ::
  forall a b.
    (Ceq a, Ccompare a, Set_impl a, Ceq b, Ccompare b,
      Set_impl b) => Set a -> Set b -> Set (a, b);
producte (RBT_set rbt1) (RBT_set rbt2) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "product RBT_set RBT_set: ccompare1 = None"
        (\ _ -> producte (RBT_set rbt1) (RBT_set rbt2));
    Just _ ->
      (case (ccompare :: Maybe (b -> b -> Ordera)) of {
        Nothing ->
          (error :: forall a. String -> (() -> a) -> a)
            "product RBT_set RBT_set: ccompare2 = None"
            (\ _ -> producte (RBT_set rbt1) (RBT_set rbt2));
        Just _ -> RBT_set (productb rbt1 rbt2);
      });
  });
producte a2 (RBT_set rbt2) =
  (case (ccompare :: Maybe (b -> b -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "product RBT_set: ccompare2 = None" (\ _ -> producte a2 (RBT_set rbt2));
    Just _ -> foldb (\ y -> sup_set (image (\ x -> (x, y)) a2)) rbt2 bot_set;
  });
producte (RBT_set rbt1) b2 =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "product RBT_set: ccompare1 = None" (\ _ -> producte (RBT_set rbt1) b2);
    Just _ -> foldb (\ x -> sup_set (image (\ a -> (x, a)) b2)) rbt1 bot_set;
  });
producte (DList_set dxs) (DList_set dys) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "product DList_set DList_set: ceq1 = None"
        (\ _ -> producte (DList_set dxs) (DList_set dys));
    Just _ ->
      (case (ceq :: Maybe (b -> b -> Bool)) of {
        Nothing ->
          (error :: forall a. String -> (() -> a) -> a)
            "product DList_set DList_set: ceq2 = None"
            (\ _ -> producte (DList_set dxs) (DList_set dys));
        Just _ -> DList_set (productc dxs dys);
      });
  });
producte a1 (DList_set dys) =
  (case (ceq :: Maybe (b -> b -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "product DList_set2: ceq = None" (\ _ -> producte a1 (DList_set dys));
    Just _ -> foldc (\ y -> sup_set (image (\ x -> (x, y)) a1)) dys bot_set;
  });
producte (DList_set dxs) b1 =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "product DList_set1: ceq = None" (\ _ -> producte (DList_set dxs) b1);
    Just _ -> foldc (\ x -> sup_set (image (\ a -> (x, a)) b1)) dxs bot_set;
  });
producte (Set_Monad xs) (Set_Monad ys) =
  Set_Monad (fold (\ x -> fold (\ y -> (\ a -> (x, y) : a)) ys) xs []);
producte a b = Collect_set (\ (x, y) -> member x a && member y b);

producta ::
  forall a b c d.
    (Finite_UNIV a, Cenum a, Ceq a, Cproper_interval a, Set_impl a,
      Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b, Eq b, Set_impl b,
      Finite_UNIV c, Cenum c, Ceq c, Cproper_interval c, Eq c, Set_impl c,
      Finite_UNIV d, Cenum d, Ceq d, Cproper_interval d,
      Set_impl d) => Fsm_impl_ext a b c () ->
                       Fsm_impl_ext d b c () -> Fsm_impl_ext (a, d) b c ();
producta a b =
  Fsm_impl_ext (initiala a, initiala b) (producte (nodesa a) (nodesa b))
    (sup_set (inputsa a) (inputsa b)) (sup_set (outputsa a) (outputsa b))
    (image
      (\ (aa, ba) ->
        (case aa of {
          (qA, (x, (y, qAa))) ->
            (\ (qB, (_, (_, qBa))) -> ((qA, qB), (x, (y, (qAa, qBa)))));
        })
          ba)
      (filtera
        (\ (aa, ba) ->
          (case aa of {
            (_, (x, (y, _))) -> (\ (_, (xa, (ya, _))) -> x == xa && y == ya);
          })
            ba)
        (sup_seta
          (image (\ tA -> image (\ aa -> (tA, aa)) (transitionsa b))
            (transitionsa a)))))
    ();

find_removea :: forall a. (a -> Bool) -> [a] -> [a] -> Maybe (a, [a]);
find_removea p [] uu = Nothing;
find_removea p (x : xs) prev =
  (if p x then Just (x, prev ++ xs) else find_removea p xs (prev ++ [x]));

find_remove :: forall a. (a -> Bool) -> [a] -> Maybe (a, [a]);
find_remove p xs = find_removea p xs [];

fsm_impl_from_lista ::
  forall a b c.
    (Ceq a, Ccompare a, Eq a, Set_impl a, Ceq b, Ccompare b, Eq b, Set_impl b,
      Ceq c, Ccompare c, Eq c,
      Set_impl c) => a -> [(a, (b, (c, a)))] -> Fsm_impl_ext a b c ();
fsm_impl_from_lista q [] =
  Fsm_impl_ext q (insert q bot_set) bot_set bot_set bot_set ();
fsm_impl_from_lista q (t : ts) =
  let {
    tsr = remdups (t : ts);
  } in Fsm_impl_ext (fst t)
         (set (remdups (map fst tsr ++ map (\ a -> snd (snd (snd a))) tsr)))
         (set (remdups (map (\ a -> fst (snd a)) tsr)))
         (set (remdups (map (\ a -> fst (snd (snd a))) tsr))) (set tsr) ();

fsm_impl_from_list ::
  forall a b c.
    (Ceq a, Ccompare a, Eq a, Set_impl a, Ceq b, Ccompare b, Eq b, Set_impl b,
      Ceq c, Ccompare c, Eq c,
      Set_impl c) => a -> [(a, (b, (c, a)))] -> Fsm_impl_ext a b c ();
fsm_impl_from_list q ts = fsm_impl_from_lista q ts;

fsm_from_list ::
  forall a b c.
    (Ceq a, Ccompare a, Eq a, Set_impl a, Ceq b, Ccompare b, Eq b, Set_impl b,
      Ceq c, Ccompare c, Eq c,
      Set_impl c) => a -> [(a, (b, (c, a)))] -> Fsm a b c;
fsm_from_list xb xc = Fsm (fsm_impl_from_list xb xc);

insort_key :: forall a b. (Linorder b) => (a -> b) -> a -> [a] -> [a];
insort_key f x [] = [x];
insort_key f x (y : ys) =
  (if less_eq (f x) (f y) then x : y : ys else y : insort_key f x ys);

sort_key :: forall a b. (Linorder b) => (a -> b) -> [a] -> [a];
sort_key f xs = foldr (insort_key f) xs [];

sorted_list_of_set ::
  forall a. (Ceq a, Ccompare a, Eq a, Linorder a) => Set a -> [a];
sorted_list_of_set (RBT_set rbt) =
  (case (ccompare :: Maybe (a -> a -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "sorted_list_of_set RBT_set: ccompare = None"
        (\ _ -> sorted_list_of_set (RBT_set rbt));
    Just _ -> sort_key (\ x -> x) (keysa rbt);
  });
sorted_list_of_set (DList_set dxs) =
  (case (ceq :: Maybe (a -> a -> Bool)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "sorted_list_of_set DList_set: ceq = None"
        (\ _ -> sorted_list_of_set (DList_set dxs));
    Just _ -> sort_key (\ x -> x) (list_of_dlist dxs);
  });
sorted_list_of_set (Set_Monad xs) = sort_key (\ x -> x) (remdups xs);

nodes_as_list ::
  forall a b c. (Ceq a, Ccompare a, Eq a, Linorder a) => Fsm a b c -> [a];
nodes_as_list m = sorted_list_of_set (nodes m);

prefix_pairs :: forall a. [a] -> [([a], [a])];
prefix_pairs [] = [];
prefix_pairs (v : va) =
  prefix_pairs (butlast (v : va)) ++
    map (\ ys -> (ys, v : va)) (butlast (prefixes (v : va)));

inputs_as_list ::
  forall a b c. (Ceq b, Ccompare b, Eq b, Linorder b) => Fsm a b c -> [b];
inputs_as_list m = sorted_list_of_set (inputs m);

find_remove_2a ::
  forall a b. (a -> b -> Bool) -> [a] -> [b] -> [a] -> Maybe (a, (b, [a]));
find_remove_2a p [] uu uv = Nothing;
find_remove_2a p (x : xs) ys prev =
  (case find (p x) ys of {
    Nothing -> find_remove_2a p xs ys (prev ++ [x]);
    Just y -> Just (x, (y, prev ++ xs));
  });

find_remove_2 ::
  forall a b. (a -> b -> Bool) -> [a] -> [b] -> Maybe (a, (b, [a]));
find_remove_2 p xs ys = find_remove_2a p xs ys [];

productd ::
  forall a c d b.
    (Finite_UNIV a, Cenum a, Ceq a, Cproper_interval a, Set_impl a,
      Finite_UNIV c, Cenum c, Ceq c, Cproper_interval c, Eq c, Set_impl c,
      Finite_UNIV d, Cenum d, Ceq d, Cproper_interval d, Eq d, Set_impl d,
      Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Set_impl b) => Fsm a c d -> Fsm b c d -> Fsm (a, b) c d;
productd xb xc = Fsm (producta (fsm_impl xb) (fsm_impl xc));

remove_subsets :: forall a. (Cenum a, Ceq a, Ccompare a) => [Set a] -> [Set a];
remove_subsets [] = [];
remove_subsets (x : xs) =
  (case find_remove (less_set x) xs of {
    Nothing -> x : remove_subsets (filter (\ y -> not (less_eq_set y x)) xs);
    Just (y, xsa) ->
      remove_subsets (y : filter (\ ya -> not (less_eq_set ya x)) xsa);
  });

filter_transitionsa ::
  forall a b c.
    (Ceq a, Ccompare a, Ceq b, Ccompare b, Ceq c,
      Ccompare c) => Fsm_impl_ext a b c () ->
                       ((a, (b, (c, a))) -> Bool) -> Fsm_impl_ext a b c ();
filter_transitionsa m p =
  Fsm_impl_ext (initiala m) (nodesa m) (inputsa m) (outputsa m)
    (filtera p (transitionsa m)) ();

filter_transitions ::
  forall a b c.
    (Ceq a, Ccompare a, Ceq b, Ccompare b, Ceq c,
      Ccompare c) => Fsm a b c -> ((a, (b, (c, a))) -> Bool) -> Fsm a b c;
filter_transitions xb xc = Fsm (filter_transitionsa (fsm_impl xb) xc);

emptyd :: forall a. Mp_trie a;
emptyd = MP_Trie [];

paths :: forall a. Mp_trie a -> [[a]];
paths (MP_Trie []) = [[]];
paths (MP_Trie (t : ts)) =
  concatMap (\ (x, ta) -> map (\ a -> x : a) (paths ta)) (t : ts);

select_inputs ::
  forall a b c.
    (Card_UNIV a, Ceq a, Cproper_interval a, Card_UNIV c, Ceq c,
      Cproper_interval c) => ((a, b) -> Set (c, a)) ->
                               a -> [b] -> [a] -> Set a -> [(a, b)] -> [(a, b)];
select_inputs f q0 inputList [] nodeSet m =
  (case find (\ x ->
               not (is_empty (f (q0, x))) &&
                 ball (f (q0, x)) (\ (_, q) -> member q nodeSet))
          inputList
    of {
    Nothing -> m;
    Just x -> m ++ [(q0, x)];
  });
select_inputs f q0 inputList (n : nL) nodeSet m =
  (case find (\ x ->
               not (is_empty (f (q0, x))) &&
                 ball (f (q0, x)) (\ (_, q) -> member q nodeSet))
          inputList
    of {
    Nothing ->
      (case find_remove_2
              (\ q x ->
                not (is_empty (f (q, x))) &&
                  ball (f (q, x)) (\ (_, qa) -> member qa nodeSet))
              (n : nL) inputList
        of {
        Nothing -> m;
        Just (q, (x, nodeList)) ->
          select_inputs f q0 inputList nodeList (insert q nodeSet)
            (m ++ [(q, x)]);
      });
    Just x -> m ++ [(q0, x)];
  });

d_states ::
  forall a b c.
    (Card_UNIV a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a, Linorder a,
      Set_impl a, Ceq b, Ccompare b, Eq b, Mapping_impl b, Linorder b,
      Card_UNIV c, Ceq c, Cproper_interval c,
      Set_impl c) => Fsm a b c -> a -> [(a, b)];
d_states m q =
  (if q == initial m then []
    else select_inputs (h m) (initial m) (inputs_as_list m)
           (removeAll q (removeAll (initial m) (nodes_as_list m)))
           (insert q bot_set) []);

completely_specified ::
  forall a b c.
    (Ceq a, Ccompare a, Eq a, Ceq b, Ccompare b, Eq b, Ceq c,
      Ccompare c) => Fsm a b c -> Bool;
completely_specified m =
  ball (nodes m)
    (\ q ->
      ball (inputs m)
        (\ x -> bex (transitions m) (\ t -> fst t == q && fst (snd t) == x)));

update_assoc_list_with_default ::
  forall a b. (Eq a) => a -> (b -> b) -> b -> [(a, b)] -> [(a, b)];
update_assoc_list_with_default k f d [] = [(k, f d)];
update_assoc_list_with_default k f d ((x, y) : xys) =
  (if k == x then (x, f y) : xys
    else (x, y) : update_assoc_list_with_default k f d xys);

insertc :: forall a. (Eq a) => [a] -> Mp_trie a -> Mp_trie a;
insertc [] t = t;
insertc (x : xs) (MP_Trie ts) =
  MP_Trie (update_assoc_list_with_default x (insertc xs) emptyd ts);

initial_singleton ::
  forall a b c.
    (Ceq a, Ccompare a, Set_impl a, Ceq b, Ccompare b, Set_impl b, Ceq c,
      Ccompare c, Set_impl c) => Fsm_impl_ext a b c () -> Fsm_impl_ext a b c ();
initial_singleton m =
  Fsm_impl_ext (initiala m) (insert (initiala m) bot_set) (inputsa m)
    (outputsa m) bot_set ();

from_list :: forall a. (Eq a) => [[a]] -> Mp_trie a;
from_list seqs = foldr insertc seqs emptyd;

cartesian_product_list :: forall a b. [a] -> [b] -> [(a, b)];
cartesian_product_list xs ys = concatMap (\ x -> map (\ a -> (x, a)) ys) xs;

remove_proper_prefixes ::
  forall a. (Ceq a, Ccompare a, Eq a) => Set [a] -> Set [a];
remove_proper_prefixes (RBT_set t) =
  (case (ccompare_list :: Maybe ([a] -> [a] -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "remove_proper_prefixes RBT_set: ccompare = None"
        (\ _ -> remove_proper_prefixes (RBT_set t));
    Just _ ->
      (if is_emptya t then set_empty (of_phantom set_impl_list)
        else set (paths (from_list (keysa t))));
  });

shifted_transitions ::
  forall a b c d.
    (Ceq a, Ccompare a, Set_impl a, Ceq b, Ccompare b, Set_impl b, Ceq c,
      Ccompare c, Set_impl c, Ceq d, Ccompare d,
      Set_impl d) => Set ((a, a), (b, (c, (a, a)))) ->
                       Set (Sum (a, a) d, (b, (c, Sum (a, a) d)));
shifted_transitions ts =
  image (\ t ->
          (Inl (fst t),
            (fst (snd t), (fst (snd (snd t)), Inl (snd (snd (snd t)))))))
    ts;

less_eq_nat :: Nat -> Nat -> Bool;
less_eq_nat m n = integer_of_nat m <= integer_of_nat n;

minus_set :: forall a. (Ceq a, Ccompare a) => Set a -> Set a -> Set a;
minus_set a b = inf_set a (uminus_set b);

distinguishing_transitions ::
  forall a b c.
    (Finite_UNIV a, Cenum a, Ceq a, Cproper_interval a, Set_impl a,
      Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b, Set_impl b,
      Finite_UNIV c, Cenum c, Ceq c, Cproper_interval c,
      Set_impl c) => ((a, b) -> Set c) ->
                       a -> a -> Set (a, a) ->
                                   Set b ->
                                     Set (Sum (a, a) a, (b, (c, Sum (a, a) a)));
distinguishing_transitions f q1 q2 nodeSet inputSet =
  sup_seta
    (image
      (\ (a, b) ->
        (case a of {
          (q1a, q2a) ->
            (\ x ->
              sup_set
                (image (\ y -> (Inl (q1a, q2a), (x, (y, Inr q1))))
                  (minus_set (f (q1a, x)) (f (q2a, x))))
                (image (\ y -> (Inl (q1a, q2a), (x, (y, Inr q2))))
                  (minus_set (f (q2a, x)) (f (q1a, x)))));
        })
          b)
      (producte nodeSet inputSet));

canonical_separator ::
  forall a b c.
    (Finite_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b, Eq b,
      Mapping_impl b, Set_impl b, Finite_UNIV c, Cenum c, Ceq c,
      Cproper_interval c,
      Set_impl c) => Fsm_impl_ext a b c () ->
                       Fsm_impl_ext (a, a) b c () ->
                         a -> a -> Fsm_impl_ext (Sum (a, a) a) b c ();
canonical_separator m p q1 q2 =
  (if initiala p == (q1, q2)
    then let {
           f = set_as_map_image (transitionsa m)
                 (\ (q, (x, (y, _))) -> ((q, x), y));
           fa = (\ qx -> (case f qx of {
                           Nothing -> bot_set;
                           Just yqs -> yqs;
                         }));
           shifted_transitionsa = shifted_transitions (transitionsa p);
           distinguishing_transitions_lr =
             distinguishing_transitions fa q1 q2 (nodesa p) (inputsa p);
           ts = sup_set shifted_transitionsa distinguishing_transitions_lr;
         } in Fsm_impl_ext (Inl (q1, q2))
                (sup_set (image Inl (nodesa p))
                  (insert (Inr q1) (insert (Inr q2) bot_set)))
                (sup_set (inputsa m) (inputsa p))
                (sup_set (outputsa m) (outputsa p)) ts ()
    else Fsm_impl_ext (Inl (q1, q2)) (insert (Inl (q1, q2)) bot_set) bot_set
           bot_set bot_set ());

initial_preamble ::
  forall a b c.
    (Ceq a, Ccompare a, Set_impl a, Ceq b, Ccompare b, Set_impl b, Ceq c,
      Ccompare c, Set_impl c) => Fsm a b c -> Fsm a b c;
initial_preamble xa = Fsm (initial_singleton (fsm_impl xa));

canonical_separatorb ::
  forall a b c.
    (Finite_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b, Eq b,
      Mapping_impl b, Set_impl b, Finite_UNIV c, Cenum c, Ceq c,
      Cproper_interval c,
      Set_impl c) => Fsm a b c ->
                       Fsm (a, a) b c -> a -> a -> Fsm (Sum (a, a) a) b c;
canonical_separatorb xc xe xf xg =
  Fsm (canonical_separator (fsm_impl xc) (fsm_impl xe) xf xg);

canonical_separatora ::
  forall a b c.
    (Finite_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b, Eq b,
      Mapping_impl b, Set_impl b, Finite_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c,
      Set_impl c) => Fsm a b c -> a -> a -> Fsm (Sum (a, a) a) b c;
canonical_separatora m q1 q2 =
  canonical_separatorb m (productd (from_FSM m q1) (from_FSM m q2)) q1 q2;

extend_until_conflict ::
  forall a. (Ceq a, Ccompare a) => Set (a, a) -> [a] -> [a] -> Nat -> [a];
extend_until_conflict non_confl_set candidates xs k =
  (if equal_nat k zero_nat then xs
    else (case dropWhile
                 (\ x ->
                   not (is_none
                         (find (\ y -> not (member (x, y) non_confl_set)) xs)))
                 candidates
           of {
           [] -> xs;
           c : cs ->
             extend_until_conflict non_confl_set cs (c : xs)
               (minus_nat k one_nat);
         }));

acyclic_language_intersection ::
  forall a b c d.
    (Finite_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b, Eq b,
      Set_impl b, Finite_UNIV c, Cenum c, Ceq c, Cproper_interval c, Eq c,
      Set_impl c, Card_UNIV d, Cenum d, Ceq d, Cproper_interval d, Eq d,
      Mapping_impl d, Set_impl d) => Fsm a b c -> Fsm d b c -> Set [(b, c)];
acyclic_language_intersection m a =
  let {
    p = productd m a;
  } in image (map (\ t -> (fst (snd t), fst (snd (snd t)))))
         (acyclic_paths_up_to_length p (initial p)
           (minus_nat (size a) one_nat));

test_suite_to_io_maximal ::
  forall a b c d.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Linorder b, Set_impl b, Finite_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Set_impl c, Card_UNIV d, Cenum d, Ceq d,
      Cproper_interval d, Eq d, Mapping_impl d, Linorder d,
      Set_impl d) => Fsm a b c -> Test_suite a b c d -> Set [(b, c)];
test_suite_to_io_maximal m (Test_Suite prs tps rd_targets atcs) =
  remove_proper_prefixes
    (sup_seta
      (image
        (\ (q, p) ->
          sup_set (lS_acyclic p (initial p))
            (sup_seta
              (image
                (\ ioP ->
                  sup_seta
                    (image
                      (\ pt ->
                        insert
                          (ioP ++
                            map (\ t -> (fst (snd t), fst (snd (snd t)))) pt)
                          (sup_seta
                            (image
                              (\ qa ->
                                sup_seta
                                  (image
                                    (\ (a, (_, _)) ->
                                      image
(\ io_atc -> ioP ++ map (\ t -> (fst (snd t), fst (snd (snd t)))) pt ++ io_atc)
(remove_proper_prefixes
  (acyclic_language_intersection (from_FSM m (target q pt)) a)))
                                    (atcs (target q pt, qa))))
                              (rd_targets (q, pt)))))
                      (tps q)))
                (remove_proper_prefixes (lS_acyclic p (initial p))))))
        prs));

prefix_pair_tests ::
  forall a b c.
    (Finite_UNIV a, Ceq a, Cproper_interval a, Eq a, Set_impl a, Ceq b,
      Ccompare b, Ceq c,
      Ccompare c) => a -> Set ([(a, (b, (c, a)))], (Set a, Set a)) ->
                            Set (a, ([(a, (b, (c, a)))], a));
prefix_pair_tests q (RBT_set t) =
  (case (ccompare_prod ::
          Maybe (([(a, (b, (c, a)))], (Set a, Set a)) ->
                  ([(a, (b, (c, a)))], (Set a, Set a)) -> Ordera))
    of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "prefix_pair_tests RBT_set: ccompare = None"
        (\ _ -> prefix_pair_tests q (RBT_set t));
    Just _ ->
      set (concatMap
            (\ (p, (rd, _)) ->
              concat
                (map_filter
                  (\ x ->
                    (if (case x of {
                          (p1, p2) ->
                            not (target q p1 == target q p2) &&
                              member (target q p1) rd &&
                                member (target q p2) rd;
                        })
                      then Just (case x of {
                                  (p1, p2) ->
                                    [(q, (p1, target q p2)),
                                      (q, (p2, target q p1))];
                                })
                      else Nothing))
                  (prefix_pairs p)))
            (keysa t));
  });

dual_set_as_map_image ::
  forall a b c d e f.
    (Ccompare a, Ccompare b, Ccompare c, Eq c, Mapping_impl c, Ceq d,
      Ccompare d, Set_impl d, Ccompare e, Eq e, Mapping_impl e, Ceq f,
      Ccompare f,
      Set_impl f) => Set (a, b) ->
                       ((a, b) -> (c, d)) ->
                         ((a, b) -> (e, f)) ->
                           (c -> Maybe (Set d), e -> Maybe (Set f));
dual_set_as_map_image (RBT_set t) f1 f2 =
  (case (ccompare_prod :: Maybe ((a, b) -> (a, b) -> Ordera)) of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "dual_set_as_map_image RBT_set: ccompare = None"
        (\ _ -> dual_set_as_map_image (RBT_set t) f1 f2);
    Just _ ->
      let {
        mm = foldb (\ kv (m1, m2) ->
                     ((case f1 kv of {
                        (x, z) -> (case lookupa m1 x of {
                                    Nothing -> updateb x (insert z bot_set) m1;
                                    Just zs -> updateb x (insert z zs) m1;
                                  });
                      }),
                       (case f2 kv of {
                         (x, z) ->
                           (case lookupa m2 x of {
                             Nothing -> updateb x (insert z bot_set) m2;
                             Just zs -> updateb x (insert z zs) m2;
                           });
                       })))
               t (emptya, emptya);
      } in (lookupa (fst mm), lookupa (snd mm));
  });

paths_up_to_length_or_condition_with_witnessa ::
  forall a b c d.
    (Ceq a, Ccompare a, Ceq b, Ccompare b, Ceq c, Ccompare c, Finite_UNIV d,
      Cenum d, Ceq d, Cproper_interval d,
      Set_impl d) => (a -> Set (b, (c, a))) ->
                       ([(a, (b, (c, a)))] -> Maybe d) ->
                         [(a, (b, (c, a)))] ->
                           Nat -> a -> Set ([(a, (b, (c, a)))], d);
paths_up_to_length_or_condition_with_witnessa f p prev k q =
  (if equal_nat k zero_nat then (case p prev of {
                                  Nothing -> bot_set;
                                  Just w -> insert (prev, w) bot_set;
                                })
    else (case p prev of {
           Nothing ->
             sup_seta
               (image
                 (\ (x, (y, qa)) ->
                   paths_up_to_length_or_condition_with_witnessa f p
                     (prev ++ [(q, (x, (y, qa)))]) (minus_nat k one_nat) qa)
                 (f q));
           Just w -> insert (prev, w) bot_set;
         }));

paths_up_to_length_or_condition_with_witness ::
  forall a b c d.
    (Ceq a, Ccompare a, Eq a, Mapping_impl a, Set_impl a, Ceq b, Ccompare b,
      Set_impl b, Ceq c, Ccompare c, Set_impl c, Finite_UNIV d, Cenum d, Ceq d,
      Cproper_interval d,
      Set_impl d) => Fsm a b c ->
                       ([(a, (b, (c, a)))] -> Maybe d) ->
                         Nat -> a -> Set ([(a, (b, (c, a)))], d);
paths_up_to_length_or_condition_with_witness m p k q =
  (if member q (nodes m)
    then paths_up_to_length_or_condition_with_witnessa (h_from m) p [] k q
    else bot_set);

m_traversal_paths_with_witness_up_to_length ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Set_impl a, Ceq b, Ccompare b, Set_impl b, Ceq c, Ccompare c,
      Set_impl c) => Fsm a b c ->
                       a -> [(Set a, Set a)] ->
                              Nat ->
                                Nat -> Set ([(a, (b, (c, a)))], (Set a, Set a));
m_traversal_paths_with_witness_up_to_length ma q d m k =
  paths_up_to_length_or_condition_with_witness ma
    (\ p ->
      find (\ da ->
             less_eq_nat (suc (minus_nat m (card (snd da))))
               (size_list
                 (filter (\ t -> member (snd (snd (snd t))) (fst da)) p)))
        d)
    k q;

m_traversal_paths_with_witness ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Set_impl a, Ceq b, Ccompare b, Set_impl b, Ceq c, Ccompare c,
      Set_impl c) => Fsm a b c ->
                       a -> [(Set a, Set a)] ->
                              Nat -> Set ([(a, (b, (c, a)))], (Set a, Set a));
m_traversal_paths_with_witness ma q d m =
  m_traversal_paths_with_witness_up_to_length ma q d m
    (suc (times_nat (size ma) m));

preamble_prefix_tests ::
  forall a b c.
    (Finite_UNIV a, Ceq a, Cproper_interval a, Eq a, Set_impl a, Ceq b,
      Ccompare b, Ceq c,
      Ccompare c) => a -> Set ([(a, (b, (c, a)))], (Set a, Set a)) ->
                            Set a -> Set (a, ([(a, (b, (c, a)))], a));
preamble_prefix_tests q (RBT_set t1) (RBT_set t2) =
  (case (ccompare_prod ::
          Maybe (([(a, (b, (c, a)))], (Set a, Set a)) ->
                  ([(a, (b, (c, a)))], (Set a, Set a)) -> Ordera))
    of {
    Nothing ->
      (error :: forall a. String -> (() -> a) -> a)
        "prefix_pair_tests RBT_set: ccompare = None"
        (\ _ -> preamble_prefix_tests q (RBT_set t1) (RBT_set t2));
    Just _ ->
      (case (ccompare :: Maybe (a -> a -> Ordera)) of {
        Nothing ->
          (error :: forall a. String -> (() -> a) -> a)
            "prefix_pair_tests RBT_set: ccompare = None"
            (\ _ -> preamble_prefix_tests q (RBT_set t1) (RBT_set t2));
        Just _ ->
          set (concatMap
                (\ (p, (rd, _)) ->
                  concat
                    (map_filter
                      (\ x ->
                        (if (case x of {
                              (p1, q2) ->
                                not (target q p1 == q2) &&
                                  member (target q p1) rd && member q2 rd;
                            })
                          then Just (case x of {
                                      (p1, q2) ->
[(q, (p1, q2)), (q2, ([], target q p1))];
                                    })
                          else Nothing))
                      (cartesian_product_list (prefixes p) (keysa t2))))
                (keysa t1));
      });
  });

preamble_pair_tests ::
  forall a b c.
    (Finite_UNIV a, Cenum a, Ceq a, Cproper_interval a, Set_impl a, Ceq b,
      Ccompare b, Ceq c,
      Ccompare c) => Set (Set a) ->
                       Set (a, a) -> Set (a, ([(a, (b, (c, a)))], a));
preamble_pair_tests drss rds =
  sup_seta
    (image
      (\ drs ->
        image (\ (q1, q2) -> (q1, ([], q2))) (inf_set (producte drs drs) rds))
      drss);

calculate_test_paths ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Set_impl a, Ceq b, Ccompare b, Eq b, Set_impl b, Ceq c, Ccompare c, Eq c,
      Set_impl c) => Fsm a b c ->
                       Nat ->
                         Set a ->
                           Set (a, a) ->
                             [(Set a, Set a)] ->
                               (a -> Set [(a, (b, (c, a)))],
                                 (a, [(a, (b, (c, a)))]) -> Set a);
calculate_test_paths ma m d_reachable_nodes r_distinguishable_pairs
  repetition_sets =
  let {
    paths_with_witnesses =
      image (\ q -> (q, m_traversal_paths_with_witness ma q repetition_sets m))
        d_reachable_nodes;
    get_paths = (\ x -> (case set_as_map paths_with_witnesses x of {
                          Nothing -> set_empty (of_phantom set_impl_set);
                          Just xs -> xs;
                        }));
    prefixPairTests =
      sup_seta
        (image (\ q -> sup_seta (image (prefix_pair_tests q) (get_paths q)))
          d_reachable_nodes);
    preamblePrefixTests =
      sup_seta
        (image
          (\ q ->
            sup_seta
              (image
                (\ mrsps -> preamble_prefix_tests q mrsps d_reachable_nodes)
                (get_paths q)))
          d_reachable_nodes);
    preamblePairTests =
      preamble_pair_tests
        (sup_seta
          (image (\ (_, a) -> image (\ (_, (_, dr)) -> dr) a)
            paths_with_witnesses))
        r_distinguishable_pairs;
    tests =
      sup_set (sup_set prefixPairTests preamblePrefixTests) preamblePairTests;
    tps = (\ x ->
            (case set_as_map_image paths_with_witnesses
                    (\ (q, p) -> (q, image fst p)) x
              of {
              Nothing -> sup_seta (set_empty (of_phantom set_impl_set));
              Just a -> sup_seta a;
            }));
    dual_maps =
      dual_set_as_map_image tests (\ (q, (p, _)) -> (q, p))
        (\ (q, a) -> (case a of {
                       (p, aa) -> ((q, p), aa);
                     }));
    tpsa = (\ x -> (case fst dual_maps x of {
                     Nothing -> set_empty (of_phantom set_impl_list);
                     Just xs -> xs;
                   }));
    tpsb = (\ q -> sup_set (tps q) (tpsa q));
    a = (\ x -> (case snd dual_maps x of {
                  Nothing -> bot_set;
                  Just xs -> xs;
                }));
  } in (tpsb, a);

combine_test_suite ::
  forall a b c d.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Set_impl a, Cenum b, Ceq b, Ccompare b, Eq b, Set_impl b, Cenum c, Ceq c,
      Ccompare c, Eq c, Set_impl c, Cenum d, Ceq d, Ccompare d, Eq d,
      Set_impl d) => Fsm a b c ->
                       Nat ->
                         Set (a, Fsm a b c) ->
                           Set ((a, a), (Fsm d b c, (d, d))) ->
                             [(Set a, Set a)] -> Test_suite a b c d;
combine_test_suite ma m nodes_with_preambles pairs_with_separators
  repetition_sets =
  let {
    drs = image fst nodes_with_preambles;
    rds = image fst pairs_with_separators;
    tps_and_targets = calculate_test_paths ma m drs rds repetition_sets;
    a = (\ x -> (case set_as_map pairs_with_separators x of {
                  Nothing -> bot_set;
                  Just xs -> xs;
                }));
  } in Test_Suite nodes_with_preambles (fst tps_and_targets)
         (snd tps_and_targets) a;

state_separator_from_input_choices ::
  forall a b c.
    (Ceq a, Ccompare a, Set_impl a, Ceq b, Ccompare b, Set_impl b, Ceq c,
      Ccompare c) => Fsm a b c ->
                       Fsm (Sum (a, a) a) b c ->
                         a -> a -> [(Sum (a, a) a, b)] ->
                                     Fsm (Sum (a, a) a) b c;
state_separator_from_input_choices m cSep q1 q2 cs =
  let {
    css = set cs;
    cssQ =
      sup_set (set (map fst cs)) (insert (Inr q1) (insert (Inr q2) bot_set));
    s0 = filter_nodes cSep (\ q -> member q cssQ);
    s1 = filter_transitions s0 (\ t -> member (fst t, fst (snd t)) css);
  } in s1;

state_separator_from_s_states ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c,
      Set_impl c) => Fsm a b c -> a -> a -> Maybe (Fsm (Sum (a, a) a) b c);
state_separator_from_s_states m q1 q2 =
  let {
    c = canonical_separatora m q1 q2;
    cs = select_inputs (h c) (initial c) (inputs_as_list c)
           (remove1 (Inl (q1, q2))
             (remove1 (Inr q1) (remove1 (Inr q2) (nodes_as_list c))))
           (insert (Inr q1) (insert (Inr q2) bot_set)) [];
  } in (if equal_nat (size_list cs) zero_nat then Nothing
         else (if equal_sum (fst (last cs)) (Inl (q1, q2))
                then Just (state_separator_from_input_choices m c q1 q2 cs)
                else Nothing));

r_distinguishable_state_pairs_with_separators ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c,
      Set_impl c) => Fsm a b c -> Set ((a, a), Fsm (Sum (a, a) a) b c);
r_distinguishable_state_pairs_with_separators m =
  set (concat
        (map_filter
          (\ x ->
            (if (case x of {
                  (_, a) -> not (is_none a);
                })
              then Just (case x of {
                          (a, b) ->
                            (case a of {
                              (q1, q2) ->
                                (\ aa ->
                                  [((q1, q2), the aa), ((q2, q1), the aa)]);
                            })
                              b;
                        })
              else Nothing))
          (map_filter
            (\ x ->
              (if (case x of {
                    (a, b) -> less a b;
                  })
                then Just (case x of {
                            (q1, q2) ->
                              ((q1, q2), state_separator_from_s_states m q1 q2);
                          })
                else Nothing))
            (cartesian_product_list (nodes_as_list m) (nodes_as_list m)))));

pairwise_r_distinguishable_state_sets_from_separators_list ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Set_impl c) => Fsm a b c -> [Set a];
pairwise_r_distinguishable_state_sets_from_separators_list m =
  let {
    rds = image fst (r_distinguishable_state_pairs_with_separators m);
  } in filter
         (\ s ->
           ball s
             (\ q1 ->
               ball s
                 (\ q2 ->
                   (if not (q1 == q2) then member (q1, q2) rds else True))))
         (map set (pow_list (nodes_as_list m)));

maximal_pairwise_r_distinguishable_state_sets_from_separators_list ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Set_impl c) => Fsm a b c -> [Set a];
maximal_pairwise_r_distinguishable_state_sets_from_separators_list m =
  remove_subsets (pairwise_r_distinguishable_state_sets_from_separators_list m);

calculate_state_preamble_from_input_choices ::
  forall a b c.
    (Card_UNIV a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a, Linorder a,
      Set_impl a, Ceq b, Ccompare b, Eq b, Mapping_impl b, Linorder b,
      Set_impl b, Card_UNIV c, Ceq c, Cproper_interval c,
      Set_impl c) => Fsm a b c -> a -> Maybe (Fsm a b c);
calculate_state_preamble_from_input_choices m q =
  (if q == initial m then Just (initial_preamble m)
    else let {
           ds = d_states m q;
           dss = set ds;
         } in (case ds of {
                [] -> Nothing;
                _ : _ ->
                  (if fst (last ds) == initial m
                    then Just (filter_transitions m
                                (\ t -> member (fst t, fst (snd t)) dss))
                    else Nothing);
              }));

d_reachable_states_with_preambles ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Cenum b, Ceq b, Ccompare b, Eq b, Mapping_impl b,
      Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c, Cproper_interval c,
      Set_impl c) => Fsm a b c -> Set (a, Fsm a b c);
d_reachable_states_with_preambles m =
  image (\ qp -> (fst qp, the (snd qp)))
    (filtera (\ qp -> not (is_none (snd qp)))
      (image (\ q -> (q, calculate_state_preamble_from_input_choices m q))
        (nodes m)));

maximal_repetition_sets_from_separators_list_naive ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Set_impl c) => Fsm a b c -> [(Set a, Set a)];
maximal_repetition_sets_from_separators_list_naive m =
  let {
    dr = image fst (d_reachable_states_with_preambles m);
  } in map (\ s -> (s, inf_set s dr))
         (maximal_pairwise_r_distinguishable_state_sets_from_separators_list m);

calculate_test_suite_for_repetition_sets ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c,
      Set_impl c) => Fsm a b c ->
                       Nat ->
                         [(Set a, Set a)] -> Test_suite a b c (Sum (a, a) a);
calculate_test_suite_for_repetition_sets ma m repetition_sets =
  let {
    nodes_with_preambles = d_reachable_states_with_preambles ma;
    pairs_with_separators =
      image (\ (a, b) ->
              (case a of {
                (q1, q2) -> (\ aa -> ((q1, q2), (aa, (Inr q1, Inr q2))));
              })
                b)
        (r_distinguishable_state_pairs_with_separators ma);
  } in combine_test_suite ma m nodes_with_preambles pairs_with_separators
         repetition_sets;

calculate_test_suite_naive ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c,
      Set_impl c) => Fsm a b c -> Nat -> Test_suite a b c (Sum (a, a) a);
calculate_test_suite_naive ma m =
  calculate_test_suite_for_repetition_sets ma m
    (maximal_repetition_sets_from_separators_list_naive ma);

greedy_pairwise_r_distinguishable_state_sets_from_separators ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Set_impl c) => Fsm a b c -> [Set a];
greedy_pairwise_r_distinguishable_state_sets_from_separators m =
  let {
    pwrds = image fst (r_distinguishable_state_pairs_with_separators m);
    k = size m;
    nL = nodes_as_list m;
  } in map (\ q -> set (extend_until_conflict pwrds (remove1 q nL) [q] k)) nL;

maximal_repetition_sets_from_separators_list_greedy ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c, Set_impl c) => Fsm a b c -> [(Set a, Set a)];
maximal_repetition_sets_from_separators_list_greedy m =
  let {
    dr = image fst (d_reachable_states_with_preambles m);
  } in remdups
         (map (\ s -> (s, inf_set s dr))
           (greedy_pairwise_r_distinguishable_state_sets_from_separators m));

calculate_test_suite_greedy ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Finite_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c,
      Set_impl c) => Fsm a b c -> Nat -> Test_suite a b c (Sum (a, a) a);
calculate_test_suite_greedy ma m =
  calculate_test_suite_for_repetition_sets ma m
    (maximal_repetition_sets_from_separators_list_greedy ma);

calculate_test_suite_naive_as_io_sequences_with_assumption_check ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c,
      Set_impl c) => Fsm a b c -> Nat -> Sum String (Set [(b, c)]);
calculate_test_suite_naive_as_io_sequences_with_assumption_check ma m =
  (if not (is_empty (inputs ma))
    then (if observable ma
           then (if completely_specified ma
                  then Inr (test_suite_to_io_maximal ma
                             (calculate_test_suite_naive ma m))
                  else Inl "specification is not completely specified")
           else Inl "specification is not observable")
    else Inl "specification has no inputs");

generate_test_suite_naive ::
  Fsm Integer Integer Integer -> Integer -> Sum String [[(Integer, Integer)]];
generate_test_suite_naive ma m =
  (case calculate_test_suite_naive_as_io_sequences_with_assumption_check ma
          (nat_of_integer m)
    of {
    Inl a -> Inl a;
    Inr ts -> Inr (sorted_list_of_set ts);
  });

calculate_test_suite_greedy_as_io_sequences_with_assumption_check ::
  forall a b c.
    (Card_UNIV a, Cenum a, Ceq a, Cproper_interval a, Eq a, Mapping_impl a,
      Linorder a, Set_impl a, Card_UNIV b, Cenum b, Ceq b, Cproper_interval b,
      Eq b, Mapping_impl b, Linorder b, Set_impl b, Card_UNIV c, Cenum c, Ceq c,
      Cproper_interval c, Eq c,
      Set_impl c) => Fsm a b c -> Nat -> Sum String (Set [(b, c)]);
calculate_test_suite_greedy_as_io_sequences_with_assumption_check ma m =
  (if not (is_empty (inputs ma))
    then (if observable ma
           then (if completely_specified ma
                  then Inr (test_suite_to_io_maximal ma
                             (calculate_test_suite_greedy ma m))
                  else Inl "specification is not completely specified")
           else Inl "specification is not observable")
    else Inl "specification has no inputs");

generate_test_suite_greedy ::
  Fsm Integer Integer Integer -> Integer -> Sum String [[(Integer, Integer)]];
generate_test_suite_greedy ma m =
  (case calculate_test_suite_greedy_as_io_sequences_with_assumption_check ma
          (nat_of_integer m)
    of {
    Inl a -> Inl a;
    Inr ts -> Inr (sorted_list_of_set ts);
  });

}
