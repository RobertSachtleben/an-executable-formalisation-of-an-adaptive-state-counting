module Main where

import Options.Applicative
import System.Environment
import System.Exit
import AdaptiveTestSuiteGenerator
import Data.Char
import System.CPUTime
import System.FilePath
import System.IO
import System.Directory
import Control.DeepSeq
import Control.Monad
import System.Timeout



--------------------------------------------------------------------------------
-- reading FSMs from .fsm files 
--------------------------------------------------------------------------------


readTransition :: String -> Either String (Integer, (Integer, (Integer, Integer))) 
readTransition line = 
    if length ts /= 4
        then Left $ "Not exactly 4 parameters for transition: " ++ line
        else if all (all isDigit) ts
            then Right (q1,(x,(y,q2)))
            else Left $ "Contains parameter that is not a positive integer: " ++ line     
    where  
        ts = words line
        q1 = read $ ts !! 0
        x  = read $ ts !! 1
        y  = read $ ts !! 2
        q2 = read $ ts !! 3 


readTransitions :: String -> Either String [(Integer, (Integer, (Integer, Integer)))]
readTransitions ts = Prelude.foldr f (Right []) (zip [1::Integer ..] $ lines ts)
    where
        f (n,line) (Right ts) = case readTransition line of 
                                Right t -> Right $ t:ts
                                Left err -> Left $ "ERROR (line " ++ (show n) ++ "): " ++ err
        f _ err = err
        
readFSM :: String -> Either String (Fsm Integer Integer Integer)
readFSM fsmStr = fmap (fsm_from_list 0) $ readTransitions fsmStr




--------------------------------------------------------------------------------
-- generating test suites for .fsm files
--------------------------------------------------------------------------------

data Opts = Opts { path                    :: String 
                 , additionalStates        :: Integer
                 , testEntireDirectory     :: Bool
                 , doNotStoreTestSuites    :: Bool
                 , collectTimingsAndCounts :: Bool
                 , timeoutInMilliseconds   :: Int }

getOpts :: Parser Opts
getOpts = Opts 
            <$> strOption
                ( short 'p'
                    <> long "path"
                    <> metavar "PATH"
                    <> help "path to .fsm file or directory" )
            <*> option auto 
                ( short 'a'
                    <> long "additional_states"
                    <> metavar "N"
                    <> help "upper bound on the number of states that the SUT may have in addition to the number of states in the specification" 
                    <> showDefault
                    <> value (0::Integer) )                   
            <*> switch
                ( short 'd'
                    <> long "test_entire_directory"
                    <> help "whether to test a single given .fsm file or all .fsm files in a given directory" )        
            <*> switch
                ( short 'n'
                    <> long "do_not_store_test_suites"
                    <> help "whether to store the generated test suites to a file" )        
            <*> switch
                ( short 'c'
                    <> long "collect_timings_and_counts"
                    <> help "if set, a .csv file is generated that collects the number of tests in the test suite generated for each entered .fsm file and the time it took to calculate these" )
            <*> option auto 
                ( short 't'
                    <> long "timeout"
                    <> metavar "T"
                    <> help "timeout for each individual test suite generation in milliseconds; can be set to a negative value to disable" 
                    <> showDefault
                    <> value ((15*60*1000)::Int) ) -- default: 15 minutes            

main :: IO ()
main = performGeneration =<< execParser opts
  where
    opts = info (getOpts <**> helper)
      ( fullDesc
        <> progDesc (  "Generates a test suite for a .fsm file represented as a list of input-output sequences.\n"     
                    ++ "If the -d flag is not set, then the path passed by -p must be a .fsm file and the test suite generated is a single file of the same name with extension \".test_suite\".\n"
                    ++ "If the -d flag is set, then the path passed by -p must be a directory and for each .fsm file in this directory a .test_suite file will be generated.\n"
                    ++ "If the -c flag is set, then a .csv file containing details on the size and execution time for each tested .fsm file is stored as \"timings_and_counts.log\"."
                    ++ "If the -d flag is set additionally, then \"timings_and_counts.log\" is stored in the directory given by the path, otherwise it is stored in the current directory.\n"
                    ++ "If the -n flag is set, then test suites are still calculated but not stored.\n"
                    ++ "Any individual test suite calculation is aborted if its calculation is not finished within the time set by -t. If -t specifies a negative value, then no timeout is used.")
        <> header "Test Suite Generator - a generator for a test suite sufficient to check the reduction conformance relation on (possibly non-deterministic) FSMs" )



showTestSequence :: [(Integer,Integer)] -> String
showTestSequence [] = ""
showTestSequence ((x,y):[]) = "(" ++ (show x) ++ "/" ++ (show y) ++ ")"
showTestSequence ((x,y):(x',y'):xys) = "(" ++ (show x) ++ "/" ++ (show y) ++ ")" ++ "." ++ (showTestSequence $ (x',y'):xys)


testFSM' :: Integer -> Bool -> FilePath -> IO (Either String (Integer, Integer))
testFSM' additionalStates doNotStoreTestSuites fsmFilePath = do 
    fsmFile <- readFile fsmFilePath
    case readFSM fsmFile of
        Left err  -> return $ Left err
        Right fsm -> do 
            s1 <- getCPUTime
            let res = generate_test_suite_greedy fsm ((additionalStates +) . integer_of_nat . size $ fsm)
            case res of 
                (Inr ts) -> do
                    -- force evaluation of ts here
                    s2 <- ts `deepseq` getCPUTime
                    when (not doNotStoreTestSuites) $ do 
                        let testSuiteFilePath = replaceExtension fsmFilePath ".test_suite"
                        withFile testSuiteFilePath WriteMode $ \file -> do 
                            mapM_ ((hPutStrLn file) . showTestSequence) ts
                    return $ Right ((s2 - s1) `div` 1000000000, toInteger $ length ts) -- convert from ps to ms
                (Inl err) -> do
                    putStrLn $ "Error handling " ++ fsmFilePath ++ ": " ++ err
                    -- do not write into any file
                    return $ Right (-1, -1) 

testFSM :: Opts -> FilePath -> IO ()
testFSM opts fsmFilePath = do
    let tout = (1000 * timeoutInMilliseconds opts)
    result <- timeout tout $ testFSM' (additionalStates opts) (doNotStoreTestSuites opts) fsmFilePath
    let result' = case result of 
                    Just x -> x
                    Nothing -> Left "Timeout"
    when (collectTimingsAndCounts opts) $ do
        appendFile timingFilePath $ fsmFilePath ++ "," ++ (case result' of
            Left err    -> "Error: " ++ err
            Right (t,n) -> (show $ additionalStates opts) ++ "," ++ (show t) ++ "," ++ (show n)) ++ "\n"
    return ()

timingFilePath :: String
timingFilePath = "timings_and_counts.log"

timingFileHeader :: String
timingFileHeader = "FSM file,additional states,time required for calculation,size of testsuite\n"

performGeneration :: Opts -> IO ()
performGeneration opts = if testEntireDirectory opts 
    then do
        isDir <- doesDirectoryExist (path opts) 
        if isDir 
            then do
                setCurrentDirectory (path opts)                 
                files <- listDirectory "." 
                let fsms = reverse $ filter (\file -> takeExtension file == ".fsm") files
                when (collectTimingsAndCounts opts) $ do
                    writeFile timingFilePath timingFileHeader
                mapM_ (testFSM opts) fsms                
                return ()

            else error $ "Error: Given path " ++ (path opts)  ++ " is not a directory but -d flag is set."
    else do
        isFile <- doesFileExist (path opts) 
        if isFile && takeExtension (path opts) == ".fsm"
            then do 
                testFSM opts (path opts)
                return ()                

            else error $ "Error: Given path " ++ (path opts)  ++ " is not a .fsm file but -d flag is not set."


