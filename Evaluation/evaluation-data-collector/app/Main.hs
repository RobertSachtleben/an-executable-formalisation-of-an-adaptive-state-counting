{-# LANGUAGE OverloadedStrings #-}

module Main where

import Control.Applicative
import qualified Data.ByteString.Lazy as BL
import Data.Csv
import qualified Data.Vector as V
import Data.Char
import qualified Data.List.Extra as DLE
import qualified Data.Map.Strict as Map
import System.Environment
import System.IO

data CsvEntry = CsvEntry
    { fsm'              :: !String
    , additionalStates' :: !Int
    , timeInMillis'     :: !Int
    , testSuiteSize'    :: !Int
    } deriving Show

data FsmConfig = FsmConfig 
    { numberOfInputs   :: Int
    , numberOfOutputs  :: Int
    , degreeOfNonDet   :: Int
    , additionalStates :: Int
    } deriving (Show, Eq, Ord)

data DataPoint = DataPoint
    { numberOfStates        :: Int
    , fsmConfig             :: FsmConfig
    , timeInMillis          :: Int
    , testSuiteSize         :: Int
    , exactDegreeOfNonDet   :: Int
    } deriving Show    

instance FromNamedRecord CsvEntry where
    parseNamedRecord r = CsvEntry <$> r .: "FSM file" <*> r .: "additional states" <*> r .: "time required for calculation" <*> r .: "size of testsuite"


dataPointFromCsvEntry :: CsvEntry -> Either String DataPoint
dataPointFromCsvEntry csvEntry = case parseFsmName of
    Left err -> Left err
    Right (n,i,o,d,xd) -> Right $ DataPoint n
                                            (FsmConfig i
                                                       o
                                                       d
                                                       (additionalStates' csvEntry))
                                            (timeInMillis' csvEntry)
                                            (testSuiteSize' csvEntry)
                                            xd
    where
        numberComponents = filter (/= []) $ DLE.split (not . isDigit) $ fsm' csvEntry
        parseFsmName = if length numberComponents == 5
            then Right (read $ numberComponents !! 0
                       ,read $ numberComponents !! 1
                       ,read $ numberComponents !! 2
                       ,read $ numberComponents !! 3
                       ,-1)
            else if length numberComponents == 6
                then Right (read $ numberComponents !! 0
                           ,read $ numberComponents !! 1
                           ,read $ numberComponents !! 2
                           ,read $ numberComponents !! 3
                           ,read $ numberComponents !! 5)           
                else Left $ "Invalid filename format: " ++ (fsm' csvEntry)
            



readCsv :: FilePath -> IO (Either String (Map.Map FsmConfig [DataPoint]))
readCsv file = do
    csvData <- BL.readFile file
    return $ case decodeByName csvData of
        Left err -> Left err
        Right (_, v) -> case V.mapM dataPointFromCsvEntry v of
            Left err -> Left err
            Right dps -> Right $ V.foldr (\ dp m -> Map.insertWith (++) (fsmConfig dp) [dp] m)
                                         Map.empty 
                                         dps


average :: [Int] -> Int
average xs = sum xs `div` length xs

median :: [Int] -> Int
median xs = xs !! (length xs `div` 2)

{-
    create sorted data tuples of the following form from a list of datapoints:
    < fsm size
    , average test suite size
    , median -//-
    , minimum -//-
    , maximum -//-
    , average execution time
    , median -//-
    , minimum -//-
    , maximum -//-
    , minimum degree of nondeterminism 
    , maximum degree of nondeterminism
    >
-}
collectResultFileData :: [DataPoint] -> [(Int,Int,Int,Int,Int,Float,Float,Float,Float,Int,Int)]
collectResultFileData dps = map dataForSize sizes
    where
        sizes = DLE.sort . DLE.nub $ map numberOfStates dps
        dataForSize x = let sizePoints = filter ((== x) . numberOfStates) dps 
                            ptsBySize = map testSuiteSize sizePoints
                            ptsByTime = map timeInMillis sizePoints
                            ptsByDNoD = map exactDegreeOfNonDet sizePoints
                            timeToSecs = (\t -> (fromIntegral t) / (fromIntegral 1000))
                        in  ( x
                            , average ptsBySize
                            , median  ptsBySize
                            , minimum ptsBySize
                            , maximum ptsBySize
                            , timeToSecs $ average ptsByTime
                            , timeToSecs $ median  ptsByTime
                            , timeToSecs $ minimum ptsByTime
                            , timeToSecs $ maximum ptsByTime
                            , minimum ptsByDNoD
                            , maximum ptsByDNoD) -- inefficient
                                    
                            
resultFilePath :: FsmConfig -> FilePath
resultFilePath fsmConfig = "i" ++ (show $ numberOfInputs fsmConfig) ++ "o" ++ (show $ numberOfOutputs fsmConfig) ++ "d" ++ (show $ degreeOfNonDet fsmConfig) ++ "a" ++ (show $ additionalStates fsmConfig) ++ ".eval"

resultFileHeader :: String
resultFileHeader = DLE.intercalate "    " [ "FsmSize"
                                          , "AvgTestSuiteSize"
                                          , "MedianTestSuiteSize"
                                          , "MinTestSuiteSize"
                                          , "MaxTestSuiteSize"
                                          , "AvgExecTime"
                                          , "MedianExecTime"
                                          , "MinExecTime"
                                          , "MaxExecTime"
                                          , "MinDegreeOfNonDet"
                                          , "MaxDegreeOfNonDet"]   

showAsList11 :: (Show a, Show b, Show c, Show d, Show e, Show f, Show g, Show h, Show i, Show j, Show k) => (a,b,c,d,e,f,g,h,i,j,k) -> [String]
showAsList11 (a,b,c,d,e,f,g,h,i,j,k) = [show a, show b, show c, show d, show e, show f, show g, show h, show i, show j, show k]

createResultFiles :: Map.Map FsmConfig [DataPoint] -> IO ()
createResultFiles m = mapM_ printResultFile $ Map.assocs m
    where
        printResultFile (fsmConfig, dps) = do
            withFile (resultFilePath fsmConfig) WriteMode $ \file -> do 
                (hPutStrLn file) resultFileHeader
                mapM_ ((hPutStrLn file) . (DLE.intercalate "    ") . showAsList11) $ collectResultFileData dps


collect_results :: FilePath -> IO()
collect_results filePath = do
    csvRes <- readCsv filePath
    case csvRes of
        Left err -> print $ "Error: " ++ err
        Right resMap -> createResultFiles resMap   

main :: IO ()
main = do
    args <- getArgs
    case args of
        [] -> error "missing file path"
        _  -> collect_results $ head args

         
            
            
