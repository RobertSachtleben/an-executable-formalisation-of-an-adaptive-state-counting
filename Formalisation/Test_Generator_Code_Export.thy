section \<open>Code Export\<close>

text \<open>This theory exports the two test suite generation algorithms given in @{text "Test_Suite_Calculation"}.\<close>

theory Test_Generator_Code_Export
  imports Test_Suite_Calculation_Refined
          "HOL-Library.Code_Target_Nat"
begin


subsection \<open>Generating the Test Suite as a List of Input-Output Sequences\<close>

derive linorder list

(* the call to sorted_list_of_set does not produce any significant overhead as the RBT-set is 
   already sorted *)
definition generate_test_suite_naive :: "(integer,integer,integer) fsm \<Rightarrow> integer \<Rightarrow> String.literal + (integer\<times>integer) list list" where
  "generate_test_suite_naive M m = (case (calculate_test_suite_naive_as_io_sequences_with_assumption_check M (nat_of_integer m)) of
    Inl err \<Rightarrow> Inl err |
    Inr ts \<Rightarrow> Inr (sorted_list_of_set ts))"

definition generate_test_suite_greedy :: "(integer,integer,integer) fsm \<Rightarrow> integer \<Rightarrow> String.literal + (integer\<times>integer) list list" where
  "generate_test_suite_greedy M m = (case (calculate_test_suite_greedy_as_io_sequences_with_assumption_check M (nat_of_integer m)) of
    Inl err \<Rightarrow> Inl err |
    Inr ts \<Rightarrow> Inr (sorted_list_of_set ts))"



export_code Inl
            fsm_from_list 
            size 
            integer_of_nat 
            generate_test_suite_naive
            generate_test_suite_greedy
in Haskell module_name AdaptiveTestSuiteGenerator


subsection \<open>Fault Detection Capabilities of the Test Harness\<close>

text \<open>The naive test harness using the code generated above (see https://bitbucket.org/RobertSachtleben/an-executable-formalisation-of-an-adaptive-state-counting)
      applies a test suite to a system under test (SUT) by repeatedly applying each IO-sequence 
      (test case) in the test suite input by input to the SUT until either the test case has been
      fully applied or the first output is observed that does not correspond to the outputs in the 
      IO-sequence and then checks whether the observed IO-sequence (consisting of a prefix of the 
      test case possibly followed by an IO-pair consisting of the next input in the test case and an
      output that is not the next output in the test case) is prefix of some test case in the test
      suite. If such a prefix exists, then the application passes, else it fails and the overall
      application is aborted, reporting a failure.

      The following lemma shows that the SUT (whose behaviour corresponds to an FSM @{text "M'"}) 
      conforms to the specification (here FSM @{text "M"}) if and only if the above application 
      procedure does not fail. As the following lemma uses quantification over all possible 
      responses of the SUT to each test case, a further testability hypothesis is required to 
      transfer this result to the actual test application process, which by necessity can only
      perform a finite number of applications: we assume that some value @{text "k"} exists such 
      that by applying each test case @{text "k"} times, all responses of the SUT to it can be 
      observed.\<close> 

lemma test_harness_soundness :
  fixes M :: "(integer,integer,integer) fsm"
  assumes "observable M'"
  and     "inputs M' = inputs M"
  and     "completely_specified M'"
  and     "size M' \<le> nat_of_integer m"
  and     "generate_test_suite_greedy M m = Inr ts"
shows  "(L M' \<subseteq> L M) \<longleftrightarrow> (list_all  (\<lambda> io . \<not> (\<exists> ioPre x y y' ioSuf . io = ioPre@[(x,y)]@ioSuf \<and> ioPre@[(x,y')] \<in> L M' \<and> \<not>(\<exists> ioSuf' . ioPre@[(x,y')]@ioSuf' \<in> set ts))) ts)"
proof -

  obtain tss where "calculate_test_suite_greedy_as_io_sequences_with_assumption_check M (nat_of_integer m) = Inr tss"
    using assms(5) unfolding generate_test_suite_greedy_def 
    by (metis Inr_Inl_False old.sum.exhaust old.sum.simps(5))


  have "inputs M \<noteq> {}"
  and  "observable M" 
  and  "completely_specified M"
    using \<open>calculate_test_suite_greedy_as_io_sequences_with_assumption_check M (nat_of_integer m) = Inr tss\<close>
    unfolding calculate_test_suite_greedy_as_io_sequences_with_assumption_check_def 
    by (meson Inl_Inr_False)+ 
  then have "tss = (test_suite_to_io_maximal M (calculate_test_suite_greedy M (nat_of_integer m)))"
    using \<open>calculate_test_suite_greedy_as_io_sequences_with_assumption_check M (nat_of_integer m) = Inr tss\<close>
    unfolding calculate_test_suite_greedy_as_io_sequences_with_assumption_check_def
    by (metis sum.inject(2)) 

  have  "\<And>q. q \<in> FSM.states M \<Longrightarrow> \<exists>d\<in>set (maximal_repetition_sets_from_separators_list_greedy M). q \<in> fst d"
    unfolding maximal_repetition_sets_from_separators_list_greedy_def Let_def
    using greedy_pairwise_r_distinguishable_state_sets_from_separators_cover[of _ M]
    by simp 
  moreover have "\<And>d. d \<in> set (maximal_repetition_sets_from_separators_list_greedy M) \<Longrightarrow> fst d \<subseteq> states M \<and> (snd d = fst d \<inter> fst ` d_reachable_states_with_preambles M)" 
            and "\<And> d q1 q2. d \<in> set (maximal_repetition_sets_from_separators_list_greedy M) \<Longrightarrow> q1\<in>fst d \<Longrightarrow> q2\<in>fst d \<Longrightarrow> q1 \<noteq> q2 \<Longrightarrow> (q1, q2) \<in> fst ` r_distinguishable_state_pairs_with_separators M"
  proof 
    fix d assume "d \<in> set (maximal_repetition_sets_from_separators_list_greedy M)"
    then have "fst d \<in> set (greedy_pairwise_r_distinguishable_state_sets_from_separators M)"
         and  "(snd d = fst d \<inter> fst ` d_reachable_states_with_preambles M)"
      unfolding maximal_repetition_sets_from_separators_list_greedy_def Let_def by force+

    then have "fst d \<in> pairwise_r_distinguishable_state_sets_from_separators M"
      using greedy_pairwise_r_distinguishable_state_sets_from_separators_soundness by blast
    then show "fst d \<subseteq> states M" and "(snd d = fst d \<inter> fst ` d_reachable_states_with_preambles M)" 
          and "\<And> q1 q2 . q1\<in>fst d \<Longrightarrow> q2\<in>fst d \<Longrightarrow> q1 \<noteq> q2 \<Longrightarrow> (q1, q2) \<in> fst ` r_distinguishable_state_pairs_with_separators M"
      using \<open>(snd d = fst d \<inter> fst ` d_reachable_states_with_preambles M)\<close>
      unfolding pairwise_r_distinguishable_state_sets_from_separators_def
      by force+ 
  qed
  ultimately have "implies_completeness (calculate_test_suite_greedy M (nat_of_integer m)) M (nat_of_integer m)"
             and  "is_finite_test_suite (calculate_test_suite_greedy M (nat_of_integer m))"
    using calculate_test_suite_for_repetition_sets_sufficient_and_finite[OF \<open>observable M\<close> \<open>completely_specified M\<close> \<open>inputs M \<noteq> {}\<close>]
    unfolding calculate_test_suite_greedy_def
    by simp+
    
  then have "finite tss" 
    using test_suite_to_io_maximal_finite[OF _ _ \<open>observable M\<close>] 
    unfolding \<open>tss = (test_suite_to_io_maximal M (calculate_test_suite_greedy M (nat_of_integer m)))\<close>
    by blast 

  have "set ts = test_suite_to_io_maximal M (calculate_test_suite_greedy M (nat_of_integer m))"
  and  "ts = sorted_list_of_set tss"
    using sorted_list_of_set(1)[OF \<open>finite tss\<close>]
    using assms(5)
    unfolding \<open>tss = (test_suite_to_io_maximal M (calculate_test_suite_greedy M (nat_of_integer m)))\<close>
              \<open>calculate_test_suite_greedy_as_io_sequences_with_assumption_check M (nat_of_integer m) = Inr tss\<close>
              generate_test_suite_greedy_def
    by simp+

  then have "(L M' \<subseteq> L M) = pass_io_set_maximal M' (set ts)"
    using calculate_test_suite_greedy_as_io_sequences_with_assumption_check_completeness[OF assms(1,2,3,4)]
          \<open>calculate_test_suite_greedy_as_io_sequences_with_assumption_check M (nat_of_integer m) = Inr tss\<close> 
          \<open>tss = test_suite_to_io_maximal M (calculate_test_suite_greedy M (nat_of_integer m))\<close>
    by simp

  moreover have "pass_io_set_maximal M' (set ts) 
                  = (list_all (\<lambda> io . \<not> (\<exists> ioPre x y y' ioSuf . io = ioPre@[(x,y)]@ioSuf \<and> ioPre@[(x,y')] \<in> L M' \<and> \<not>(\<exists> ioSuf' . ioPre@[(x,y')]@ioSuf' \<in> set ts))) ts)"
  proof -
    have "\<And> P . list_all P (sorted_list_of_set tss) = (\<forall> x \<in> tss . P x)"
      by (simp add: \<open>finite tss\<close> list_all_iff)
    then have scheme: "\<And> P . list_all P ts = (\<forall> x \<in> (set ts) . P x)"
      unfolding \<open>ts = sorted_list_of_set tss\<close> sorted_list_of_set(1)[OF \<open>finite tss\<close>] 
      by simp
      
    show ?thesis
      using scheme[of "(\<lambda> io . \<not> (\<exists> ioPre x y y' ioSuf . io = ioPre@[(x,y)]@ioSuf \<and> ioPre@[(x,y')] \<in> L M' \<and> \<not>(\<exists> ioSuf' . ioPre@[(x,y')]@ioSuf' \<in> set ts)))"]
      unfolding pass_io_set_maximal_def 
      by fastforce
  qed

  ultimately show ?thesis
    by simp
qed

end