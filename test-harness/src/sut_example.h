#ifndef SUT_EXAMPLE_H
#define SUT_EXAMPLE_H

extern int example_apply(int input);

extern void example_reset();

#endif /* SUT_EXAMPLE_H */