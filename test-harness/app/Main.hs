{-# LANGUAGE ForeignFunctionInterface #-}

module Main where
    
import qualified Data.List.Extra as DLE
import qualified Text.Read as TR
import Data.Maybe
import Options.Applicative
import Data.List

foreign import ccall "sut_init" sut_init :: IO ()
foreign import ccall "sut_reset" sut_reset :: IO ()
foreign import ccall "sut" sut :: Int -> IO Int

shoIOSequence :: [(Int,Int)] -> String
shoIOSequence [] = ""
shoIOSequence ((x,y):[]) = "(" ++ (show x) ++ "/" ++ (show y) ++ ")"
shoIOSequence ((x,y):(x',y'):xys) = "(" ++ (show x) ++ "/" ++ (show y) ++ ")" ++ "." ++ (shoIOSequence $ (x',y'):xys)

readIOPair :: String -> Either String (Int,Int)
readIOPair x@('(':rem) = if length rem' == 2 && last rem'' == ')' && isJust input && isJust output
                            then Right (fromJust input, fromJust output) 
                            else Left $ "not and IO pair: " ++ x  
    where 
        rem' = DLE.split (=='/') rem    
        input = TR.readMaybe (rem' !! 0) :: Maybe Int
        rem'' = rem' !! 1
        output = TR.readMaybe (init rem'') :: Maybe Int
readIOPair x = Left $ "not and IO pair: " ++ x    

readTestCase :: String -> Either String [(Int,Int)]
readTestCase str = case mapM readIOPair $ DLE.split (== '.') str of
  Left err -> Left $ "Error parsing test case " ++ str ++ " : " ++ err
  Right tc -> Right tc

readTestSuite :: FilePath -> IO (Either String [[(Int,Int)]])
readTestSuite file = do 
    content <- readFile file
    return $ mapM readTestCase $ lines content


runTestCaseUntilDeviation :: [(Int,Int)] -> IO [(Int,Int)]
runTestCaseUntilDeviation [] = return []
runTestCaseUntilDeviation ((x,y):ts) = do
  y' <- sut x
  if y == y'
    then do
      remainder <- runTestCaseUntilDeviation ts
      return $ (x,y):remainder
    else
      return [(x,y')] 

{-
  a test case (i.e.: a list of IO-pairs) is applied as follows:
  1.) the SUT is reset 
  2.) the input of the head of the test case is applied to the SUT
      - if the observed output is equal to the output in the head of the test case,
        then the next input in the test case is applied, repeating this process
        until either the test case has been fully processed or an output is observed
        that deviates from the corresponding output in the test case
      - if the observed output does not equal the corresponding output in the test case,
        then test case execution stops
  3.) the observed response of the SUT to the test case is checked against the test suite:      
      - if the observed response is a prefix of some test case in the test suite, then
        the application reports PASS, else FAIL
-}
applyTestCase :: [[(Int,Int)]] -> String -> [(Int,Int)] -> IO Bool
applyTestCase testSuite outputFile testCase = do
  sut_reset
  appendFile outputFile $ "Applying              : " ++ (shoIOSequence testCase) ++ "\n"
  observedBehaviour <- runTestCaseUntilDeviation testCase
  appendFile outputFile $ "    observed reaction : " ++ (shoIOSequence observedBehaviour) ++ "\n"
  if testCase == observedBehaviour 
    then do
      appendFile outputFile "    verdict           : PASS\n"
      return True
    else if any (isPrefixOf observedBehaviour) testSuite
      then do
        appendFile outputFile "    verdict           : PASS (observed reaction is prefix of another test case in the test suite)\n"
        return True
      else do 
        appendFile outputFile "    verdict           : FAIL\n"
        return False

repeatedlyApplyTestCase :: [[(Int,Int)]] -> String -> Int -> [(Int,Int)] -> IO Bool
repeatedlyApplyTestCase testSuite outputFile k testCase = do
  results <- mapM (\_ -> applyTestCase testSuite outputFile testCase) [1..k]
  return $ all id results


data Opts = Opts { testSuiteFilePath       :: String 
                 , resultFilePath          :: String
                 , repetitions             :: Int }

getOpts :: Parser Opts
getOpts = Opts 
            <$> strArgument
                ( metavar "TESTSUITE_PATH"
                  <> help "path to a test suite" )
            <*> strOption
                ( short 'o'
                    <> long "output"
                    <> metavar "PATH"
                    <> help "file into which the results are written (will be cleared if it already exists)" 
                    <> showDefault
                    <> value "results.txt" )                   
            <*> option auto 
                ( short 'r'
                    <> long "repetitions"
                    <> metavar "N"
                    <> help "Number of times each line of the test suite is applied to the SUT to try to observe all reactions" 
                    <> showDefault
                    <> value (100::Int) )

main :: IO ()
main = testSUT =<< execParser opts
  where
    opts = info (getOpts <**> helper)
      ( fullDesc
        <> progDesc (  "Applies a test suite generated using test-suite-generator to an SUT (establishing an interface via sut_wrapper.c) and checks whether the SUT passes the test suite. "
                       ++ "An SUT passes a test suite if for every IO sequence (x1/y1)...(xn/yn) in the test suite the SUT does not exhibit some response (x1,y1)...(xi/yi') with i <= n such that yi' is the first output that deviates from the corresponding output in (x1/y1)...(xn/yn) and such that (x1,y1)...(xi/yi') is not prefix of any test case in the test suite.")
        <> header "Test Harness" )


testSUT :: Opts -> IO () 
testSUT opts = do
    testSuite' <- readTestSuite $ testSuiteFilePath opts 
    case testSuite' of
      Left err -> error $ "Error parsing test suite: " ++ err
      Right testSuite -> do
        sut_init
        writeFile (resultFilePath opts) ""
        results <- mapM (repeatedlyApplyTestCase testSuite (resultFilePath opts) (repetitions opts)) testSuite
        let verdict = if all id results then "PASS" else "FAIL"
        appendFile (resultFilePath opts) $ "\n\n verdict: " ++ verdict


